function out = integrateContext(in,contextTF,shape,type,padOpt)
%integrateContext   Integrate context across spectro-temporal neighborhood.
%
%USAGE
%   out = integrateContext(in)
%   out = integrateContext(in,contextTF,shape,type,padOpt)
%
%INPUT ARGUMENTS
%          in : input signal [nChannels x nFrames]
%   contextTF : time-frequency context (must be odd)
%               (default, contextTF = [5 5])
%       shape : string specifying shape of integration window
%               'plus'        : plus-shaped window
%               'rect'        : rectangular window
%               'plus_causal' : plus-shaped, causal window
%               'rect_causal' : rectangular, causal window
%        type : function handle specifying type of integration
%               (default, type = @mean)
%      padOpt : string specifying method to pad input data
%               'circular'  : pad with circular repetition of elements
%               'replicate' : repeat border elements 
%               'symmetric' : pad array with mirror reflections of itself
%               (default, padOpt = 'replicate')    
% 
%OUTPUT ARGUMENTS
%         out : output signal [nChannels x nFrames]
%
%   integrateContext(...) plots the result in a new figure.
% 
%REFERENCES
%   [1] T. May and T. Dau, "Environment-aware ideal binary mask estimation
%       using monaural cues," IEEE Workshop on Applications of Signal
%       Processing to Audio and Acoustics (WASPAA), 2013.
% 
%   [2] T. May and T. Gerkmann, "Generalization of supervised learning for
%       binary mask estimation", International Workshop on Acoustic Signal
%       Enhancement (IWAENC), pp.154-158, 2014. 

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/08/02
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 5
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(contextTF); contextTF = [5 5];       end
if nargin < 3 || isempty(shape);     shape     = 'plus';      end
if nargin < 4 || isempty(type);      type      = @mean;       end
if nargin < 5 || isempty(padOpt);    padOpt    = 'replicate'; end

% Check if context contains odd numbers only
if any(~(contextTF-fix(contextTF./2)*2))
    error('All elements in context must be odd-numbered.')
end


%% CREATE SPECTRO-TEMPORAL INTEGRATION WINDOW
% 
% 
% Allocate matrix for spectro-temporal filter
strel = false(contextTF(2),contextTF(1));

% Select window shape used for spectro-temporal integration
switch(lower(shape))
    case 'rect'
        strel(:) = true;
    case 'rect_causal'
        strel(:,(1:(contextTF(1)+1)/2)) = true;
    case 'plus'
        strel((contextTF(2)+1)/2,:) = true;
        strel(:,(contextTF(1)+1)/2) = true;
    case 'plus_causal'
        strel((contextTF(2)+1)/2,1:(contextTF(1)+1)/2) = true;
        strel(:,(contextTF(1)+1)/2) = true;
    otherwise
        error('Spectro-temporal integration shape is not supported!')
end

            
%% PERFORM SPECTRO-TEMPORAL INTEGRATION
% 
% 
% Integrate input across neighborhood function
out = sffilt(type,in,strel,padOpt);


%% VISUALIZE RESULTS
% 
% 
if nargout == 0
   figure;
   
   subplot(211)
   imagesc(in);
   xlabel('Time frames')
   ylabel('Frequency channels')
   title('Input')
   colorbar;
   axis xy
   
   subplot(212)
   imagesc(out);
   xlabel('Time frames')
   ylabel('Frequency channels')
   title('Output')
   colorbar;
   axis xy   
end
