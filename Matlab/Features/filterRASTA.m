function out = filterRASTA(in,stepSec,tauSec,dim)
%filterRASTA   Relative spectral (RASTA) filtering. 
%   The RASTA filter emphasizes speech-specific modulations by suppressing 
%   temporal fluctuations outside the speech-relevant frequency range.
%
%USAGE
%       out = filterRASTA(in,stepSec)
%       out = filterRASTA(in,stepSec,tauSec,dim)
%   
%INPUT ARGUMENTS
%        in : data matrix arranged as [nFrames x nChannels]
%   stepSec : step size between consecutive frames in seconds
%    tauSec : auto-regressive integration constant in seconds
%             (default, tauSec = 160E-3)
%       dim : dimension along which the input data should be filtered
% 
%OUTPUT ARGUMENTS
%       out : filtered output sequence [nFrames x featureDim] 
% 
%REFERENCES
%   [1] H. Hermansky and N. Morgan, "RASTA processing of speech," IEEE
%       Trans. Speech, Audio Process. 2, pp.578�589, 1994.

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/07/17
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(tauSec); tauSec = 160E-3;          end
if nargin < 4 || isempty(dim);    dim    = findNonSDim(in); end

% Determine dimensionality of x
xdim = size(in);

% Check dimensionality
if numel(xdim) > 2
    error('Input must be two-dimensional')
end
    

%% DESIGN RASTA FILTER
% 
% 
% Numerator and denominator coefficients
numer = -2:2;
numer = -numer ./ sum(numer.*numer);
denom = [1 -exp(-(1/((1/stepSec) * tauSec)))];


%% PAD INPUT DATA
% 
% 
% Filter order
order = max(numel(numer),numel(denom))-1;

% Determine filter delay
delay = order/2;

% Pad data with zeros
if dim == 1
    xx = [zeros(floor(delay),xdim(2)); in; zeros(ceil(delay),xdim(2))];
else
    xx = [zeros(xdim(1),floor(delay)) in zeros(xdim(1),ceil(delay))];
end


%% PERFORM RASTA FILTERING
% 
% 
% RASTA filtering 
out = filter(numer, denom, xx, [], dim);

% Trim edges
if dim == 1
    out = out(order + (1:xdim(dim)),:);
else
    out = out(:,order + (1:xdim(dim)));
end


function dim = findNonSDim(input)
% Helper function to find the first non-singleton dimension
dim = find(size(input)~=1,1);
if isempty(dim), dim = 1; end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************