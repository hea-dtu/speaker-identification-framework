function delta = calcDeltaHTK(input,w,dim,bNormalize)
%calcDeltaHTK   Compute time derivatives using regression.
%   The regression is computed over a range of -w:w samples across the
%   first non-singleton dimension. Since the resulting delta coefficients
%   rely on past and future values, the start and end points of the input
%   are replicated [1].  
% 
%USAGE
%        delta = calcDeltaHTK(input)
%        delta = calcDeltaHTK(input,w,dim,bNormalize)
%
%INPUT ARGUMENTS
%        input : input signal [nSamples x nChannels]
%            w : size of regression window, which must be an integer. The
%                number of elements in w determine the order of the delta
%                coefficients. If w is a scalar, first order derivatives
%                are computed. If w contains two values, first and second
%                order variants are returned. (default, w = [ 2 ])
%          dim : dimension along which the delta coefficients are computed
%   bNormalize : normalize delta coefficients according to the size of w
%                (default, bNormalize = true)
% 
%OUTPUT ARGUMENTS
%        delta : delta coefficients [nSamples * numel(w) x nChannels] or
%                                   [nSamples x nChannels * numel(w)] 
% 
%REFERENCES
%   [1] S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G.
%       Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland,
%       "The HTK Book (for HTK Version 3.4.1)", Engineering Department,
%       Cambridge University, 2006.   


%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/02/24
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(w);          w          = 2;                  end
if nargin < 3 || isempty(dim);        dim        = findNonSDim(input); end
if nargin < 4 || isempty(bNormalize); bNormalize = true;               end

% Check if w is an integer
if any(w ~= round(w)) || any(w < 1) || any(~isfinite(w))
   error('The value of w must be a finite integer larger than zero.') 
end

% Order of derivatives
order = length(w);

% Determine input size
nDim = size(input);

if numel(nDim) > 2
    error('Input must be two-dimensional')
else
    % Index specifying dimension of cepstral coefficients
    idxCeps = setdiff(1:2,dim);
    
    % Dimension of delta coefficients
    nDimDelta = nDim; nDimDelta(idxCeps) = nDim(idxCeps) * order;
    
    % Allocate memory    
    delta = zeros(nDimDelta);
end


%% COMPUTE DELTA FEATURES
% 
% 
if dim == 1
    % ==================
    % Work along columns
    % ==================
    %
    %
    % Loop over the derivative order
    for jj = 1 : order
        
        % Indices of delta coefficients
        idxD = (1:nDim(idxCeps)) + (jj-1)*nDim(idxCeps);
        
        % Loop over the regression window size
        for ii = 1 : w(jj)
            
            % Pad data
            data_P_T = [input((1+ii):end,:); repmat(input(end,:),[ii 1])];
            data_M_T = [repmat(input(1,:),[ii 1]); input(1:end-ii,:)    ];
            
            % Differentiate
            delta(:,idxD) = delta(:,idxD) + ii * (data_P_T - data_M_T);
        end
        
        % Normalize
        if bNormalize
            delta(:,idxD) = delta(:,idxD) ./ (2 * sum((1:w(jj)).^2));
        end
        
        % Replace input with jj-th delta coefficients
        input = delta(:,idxD);
    end
else
    % ==================
    % Work along rows
    % ==================
    %     
    % 
    % Loop over the derivative order
    for jj = 1 : order
        
        % Indices of delta coefficients
        idxD = (1:nDim(idxCeps)) + (jj-1)*nDim(idxCeps);
        
        % Loop over the regression window size
        for ii = 1 : w(jj)
            
            % Pad data
            data_P_T = [input(:,(1+ii):end)  repmat(input(:,end),[1 ii])];
            data_M_T = [repmat(input(:,1),[1 ii])  input(:,1:end-ii)    ];
            
            % Differentiate
            delta(idxD,:) = delta(idxD,:) + ii * (data_P_T - data_M_T);
        end
        
        % Normalize
        if bNormalize
            delta(idxD,:) = delta(idxD,:) ./ (2 * sum((1:w(jj)).^2));
        end
        
        % Replace input with jj-th delta coefficients
        input = delta(idxD,:);
    end
end


function dim = findNonSDim(input)
% Helper function to find the first non-singleton dimension
dim = find(size(input)~=1,1);
if isempty(dim), dim = 1; end