function out = applyWeights_FFT(spec,wts,domain)
%applyWeights_FFT   Apply real-valued FFT weights to a spectrum.
%   
%USAGE
%      out = applyWeights_FFT(spec,wts,domain)
% 
%INPUT ARGUMENTS
%     spec : matrix containing the complex, one-sided spectrum, e.g.
%            obtained via spectrogram.m [(mx-mn) x nFrames] 
%      wts : sparse matrix containing the weights [nFilter x (mx-mn)]
%   domain : specify filtering domain and output domain
%            'a_a' - amplitude domain filtering, output amplitude
%            'a_p' - amplitude domain filtering, output power
%            'p_a' - power domain filtering, output amplitude
%            'p_p' - power domain filtering, output power
%            'c_a' - complex domain filtering, output amplitude
%            'c_p' - complex domain filtering, output power
% 
%OUTPUT ARGUMENTS
%	   out : filtered spectrum [nFilter x nFrames]
% 
%   See also createFB_Triang and calcFBE.

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/06/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Determine input size
[nS,kS] = size(spec); %#ok
[nW,kW] = size(wts);  %#ok

% Check dimensionality
if kW ~= nS
    error('Number of FFT weights must be equal to the number of FFT bins.')
end


%% PERFORM FILTERING
% 
% 
% Select domain
switch lower(domain)
    case 'a_a'
        % Amplitude domain filtering, output amplitude
        out = wts * abs(spec);
    case 'a_p'
        % Amplitude domain filtering, output power
        out = (wts * abs(spec)).^2;
    case 'p_a'
        % Power domain filtering, output amplitude
        out = sqrt(wts * (spec .* conj(spec)));
    case 'p_p'
        % Power domain filtering, output power
        out = wts * (spec .* conj(spec));
    case 'c_a'
        % Complex domain filtering, output amplitude
        out = abs(wts * spec);
    case 'c_p'
        % Complex domain filtering, output power
        out = wts * spec;
        out = out .* conj(out);
    otherwise
        error('Unknown domain')
end
 