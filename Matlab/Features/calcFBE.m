function [FBE,cfHz] = calcFBE(input,fsHz,blockSec,stepSec,fRangeHz,nFilter,bRemoveDC,bPreEmph)
%calcFBE   Compute filterbank energy (FBE) features
%   
%USAGE
%   [FBE,cfHz] = calcFBE(input,fsHz)
%   [FBE,cfHz] = calcFBE(input,fsHz,blockSec,stepSec,fRangeHz,nFilter,bRemoveDC,bPreEmph)
% 
%INPUT ARGUMENTS
%       input : input signal [nSamples x 1]
%        fsHz : sampling frequency in Hertz
%    blockSec : block size in seconds (default, blockSec = 20E-3)
%     stepSec : step size in seconds  (default, stepSec = 10E-3)
%    fRangeHz : lower and upper frequency limit in Hertz 
%               (default, fRangeHz = [80 fsHz/2])
%     nFilter : number of triangular filters (default, nFilter = 20)
%   bRemoveDC : remove DC signal offset prior to FBE feature extraction
%               (default, bRemoveDC = false)
%    bPreEmph : apply first order whitening filter 
%               (default, bPreEmphasis = false)
% 
%OUTPUT ARGUMENTS
%         FBE : filterbank energy (FBE) features [nFilter x nFrames]
%        cfHz : centre frequencies of triangular filters [1 x nFilter]
% 
%   calcFBE(...) plots the FBE features in a new figure.
% 
%   See also calcMFCCs.
% 
%REFERENCES
%   [1] S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G.
%       Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland,
%       "The HTK Book (for HTK Version 3.4.1)", Engineering Department,
%       Cambridge University, 2006. 

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/06/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 8
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(blockSec);  blockSec  = 20E-3;       end
if nargin < 4 || isempty(stepSec);   stepSec   = 10E-3;       end
if nargin < 5 || isempty(fRangeHz);  fRangeHz  = [80 fsHz/2]; end
if nargin < 6 || isempty(nFilter);   nFilter   = 20;          end
if nargin < 7 || isempty(bRemoveDC); bRemoveDC = false;       end
if nargin < 8 || isempty(bPreEmph);  bPreEmph  = false;       end


%% INITIALIZE PARAMETERS
% 
% 
% Pre-emphasize coefficient
preCoeff = 0.97; 

% Compute framing parameters
blockSize = 2 * round(blockSec * fsHz / 2);
stepSize  = round(stepSec * fsHz);
overlap   = blockSize - stepSize;
nfft      = pow2(nextpow2(blockSize));
win       = hamming(blockSize);

% Determine size of input signal
[nSamples,nChannels] = size(input);

% Check if input is monaural
if nChannels ~= 1
    error('Monaural input required.')
end


%% ZERO-PADDING
% 
% 
% Number of frames
nFrames = ceil((nSamples-overlap)/stepSize); 

% Compute number of required zeros
nZeros = ((nFrames * stepSize) + overlap) - nSamples;

% Pad input signal with zeros
input = [input; zeros(nZeros,1)];


%% PRE-PROCESSING
% 
%
% Remove DC offset
if bRemoveDC
    input = input - mean(input);
end
   
% Apply first order whitening filter
if bPreEmph
    input = filter([1 -preCoeff], 1, input);
end


%% AUDITORY SPECTROGRAM
% 
% 
% Compute spectrogram
[spec,fHz] = spectrogram(input,win,overlap,nfft,fsHz);

% Implement triangular filter weights
[wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,false,false);

% Apply triangular filters
FBE = applyWeights_FFT(spec(mn:mx,:),wts,'p_a');


%% PLOT RESULTS
% 
% 
if nargout == 0
    
    timeSec   = (1:numel(input))/fsHz;
    framesSec = (0:nFrames-1) * stepSec + (blockSec/2);
    
    figure;
    ax(1) = subplot(311);
    plot(timeSec,input);
    title('Time domain')
    
    ax(2) = subplot(312);
    imagesc(framesSec,fHz,power(1/3,abs(spec)));axis xy
    title('Spectrogram')
    
    ax(3) = subplot(313);
    imagesc(framesSec,cfHz,power(1/3,FBE));axis xy
    title('FBE features')
        
    xlabel('Time (s)')
    
    linkaxes(ax,'x');
    xlim([framesSec(1) framesSec(end)]);
end
