function delta = calcDeltaDiff(x,type,dim)
%calcDeltaDiff   Compute time derivatives using first order difference.
% 
%USAGE
%   delta = calcDeltaDiff(x)
%   delta = calcDeltaDiff(x,type,dim)
%
%INPUT ARGUMENTS
%       x : input signal [nSamples x nChannels]
%    type : string specifying differentiation method
%             'first' : first-order difference 
%           'central' : central difference 
%              'lyon' : Lyon's differentiator
%     dim : dimension along which the delta coefficients are computed
% 
%OUTPUT ARGUMENTS
%   delta : delta coefficients [nSamples x nChannels]


%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/02/23
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(type); type = 'first';        end
if nargin < 3 || isempty(dim);  dim  = findNonSDim(x); end

% Determine dimensionality of x
xdim = size(x);

% Check dimensionality
if numel(xdim) > 2
    error('Input must be two-dimensional')
end


%% CREATE IMPULSE RESPONSE
% 
% 
% Select type
switch lower(type)
    case 'first'
        % First difference high-pass characteristic
        hDiff = [1 -1];
        delay = 0;
    case 'central'
        % Central difference which does not amplify high frequencies. It's
        % linear operation ends around fHz = 0.17 * fsHz [2]. 
        hDiff = [1 0 -1] / 2;
        delay = 0;
    case 'lyon'
        % Lyon's differentiator [2], it's magnitude response stays linear
        % up to 0.17 * fsHz.
        hDiff = [-1/16 0 1 0 -1 0 1/16] * 0.6;
        delay = 3;
    otherwise
        error('Method is not supported.')
end


%% PAD INPUT DATA
% 
% 
% Determine filter order
hOrder = length(hDiff) - 1;

% Pad data by repeating first and last columns
if dim == 1
    xx = [repmat(x(1,:),hOrder,1); x; repmat(x(end,:),hOrder,1)];
else
    xx = [repmat(x(:,1),hOrder,1) x repmat(x(:,end),hOrder,1)];
end


%% COMPUTE DELTA FEATURES
% 
% 
% Differentiate
delta = filter(hDiff,1,xx,[],dim);

% Trim edges
if dim == 1
    delta = delta(hOrder + delay + (1:xdim(dim)),:);
else
    delta = delta(:,hOrder + delay + (1:xdim(dim)));
end


function dim = findNonSDim(input)
% Helper function to find the first non-singleton dimension
dim = find(size(input)~=1,1);
if isempty(dim), dim = 1; end