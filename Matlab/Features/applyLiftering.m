function [out,w] = applyLiftering(input,L)
%applyLiftering   Perform liftering of mel-frequency cepstral coefficients.
%   Higher order MFCC coefficients tend to be smaller in magnitude. Thus,
%   liftering aims at boosting the magnitude of higher order cepstral
%   coefficients, such that all coefficients have a similar magnitude.
%   
%USAGE
%   [out,w] = applyLiftering(input)
%   [out,w] = applyLiftering(input,L)
%
%INPUT ARGUMENTS
%     input : cepstral coefficients [nCoefficients x nFrames]
%         L : liftering parameter which controls the amount of gain applied
%             to the higher order cepstral coefficients. If L is chosen too
%             low, the resulting weights will not increase monotonically. 
%             (default, L = 22) according to [1].
% 
%OUTPUT ARGUMENTS
%       out : liftered cepstral coefficients [nCoefficients x nFrames]
%         w : weights used for liftering [1 x nCoefficients]
% 
%REFERENCES
%   [1] S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G.
%       Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland,
%       "The HTK Book (for HTK Version 3.4.1)", Engineering Department,
%       Cambridge University, 2006.   


%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/02/24
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 2
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(L); L = 22; end

% Determine number of cepstral coefficients N
N = size(input,1);


%% COMPUTE AND APPLY LIFTERING WEIGHTS
% 
% 
% Lifering parameter
w = 1 + 0.5 * L * sin(pi * (0:N-1) / L);

% Check weights
if any(w==0)
    warning('Some liftering weights are zero, consider to increase L.')
end

% Check if weights are positive (monotonic seems to strict)
if any(w < 0)
    warning('Liftering weights are negative, consider to increase L.')
end

% Perform liftering
out = diag(w) * input;