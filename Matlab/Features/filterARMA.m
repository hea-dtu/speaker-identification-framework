function out = filterARMA(in,M,dim)
%filterARMA   Auto-Regression Moving Average (ARMA) filtering. 
%
%USAGE
%   OUT = filterARMA(IN,M)
%   OUT = filterARMA(IN,M,DIM)
%   
%INPUT ARGUMENTS
%    IN : input matrix assumed to be a arranged as [nFrames x featureDim].
%         The ARMA filtering is applied over all frames of each feature 
%         dimension in order to smooth out spikes in the feature sequence.
%     M : ARMA filter order (default, M = 2) 
%   DIM : dimension along which the ARMA filtering should be applied
%         (default, DIM = 1)
% 
%OUTPUT ARGUMENTS
%   OUT : smoothed output sequence [nFrames x featureDim] 
% 
%REFERENCES
%   [1] C.-P. Chen, J. Bilmes and K. Kirchhoff, "Frontend Post-Processing 
%       and Backend Model Enhancement on the Aurora 2.0/3.0 Databases", 
%       pp. 241-244, Proceedings of ICSLP 2002.
%
%   [2] C.-P. Chen, J. Bilmes and K. Kirchhoff, "Low-Resource Noise-Robust 
%       Feature Post-Processing on Aurora 2.0", Proceedings of ICSLP, 
%       pp. 2445-2448, 2002.
%
%   [3] C.-P. Chen and J. Bilmes, "MVA Processing of Speech Features", IEEE
%       Trans. Speech Audio Process., vol. 15, no. 1, pp. 257-270, 2007. 

%   Developed with Matlab 7.4.0.287 (R2007a). Please send bug reports to:
%   
%   Author  :   Tobias May    tobias.may@philips.com
%               � 2008 by Philips Research
%   History :   
%   v.0.1   2007/04/08
%   v.0.1   2015/10/04 added ability to select filtering dimension
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 2
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(dim); dim = 1; end
if nargin < 2 || isempty(M);   M   = 2; end

% Check if M is an integer value
if rem(M,1)~=0
    warning(['MATLAB:',mfilename],['ARMA filter order M must be an',...
             ' integer value! M is rounded to the next integer.']);
    % Round M to the next integer value
    M = round(M);
end

% Check for proper dimensionality
if numel(size(in)) > 2
    error('Two-dimensional input required.')
end


%% PERFORM ARMA FILTERING 
% 
% 
% ARMA filtering
if M > 0
    % ARMA filtering (MEX processing)
    out = filterArmaMEX(in,M,dim);
else
    % No processing...
    out = in;
end

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************