#include <math.h>
#include <cmath>
#include "mex.h"

/* Input Arguments */
#define   INPUT                prhs[0]  // input matrix
#define   ARMAORDER            prhs[1]  // ARMA filter order
#define   DIM                  prhs[2]  // Dimension

/* Output Arguments */
#define   OUTPUT        	   plhs[0]  // output matrix

/*  Set resolution of floating point numbers */
const double EPS = pow(2.00,-52.00);

void usage()
{
	mexPrintf(" filterARMA Auto-Regression Moving Average (ARMA) filtering.\n"); 
	mexPrintf("\n"); 
	mexPrintf(" USAGE\n"); 
	mexPrintf("\t OUT = filterARMA(IN,M)\n"); 
    mexPrintf("\t OUT = filterARMA(IN,M,DIM)\n"); 
	mexPrintf("\n"); 
	mexPrintf(" INPUT ARGUMENTS\n");
	mexPrintf("\t  IN : Input matrix assumed to be a arranged as [featureDim x nFrames].\n");
	mexPrintf("\t       The ARMA filtering is applied over all frames of each feature \n");
	mexPrintf("\t       dimension in order to smooth out spikes in the feature sequence.\n");
	mexPrintf("\t   M : ARMA filter order (default, M = 2) \n");
    mexPrintf("\t DIM : dimension along which the ARMA filtering should be applied (default, DIM = 2) \n");
	mexPrintf("\n");
	mexPrintf(" OUTPUT ARGUMENTS\n");
	mexPrintf("\t OUT : smoothed output sequence [featureDim x nFrames] \n");
	mexPrintf("\n");
	mexPrintf(" REFERENCES\n");
	mexPrintf("\t[1]  C.-P. Chen, J. Bilmes and K. Kirchhoff, \"Frontend Post-Processing \n");
	mexPrintf("\t     and Backend Model Enhancement on the Aurora 2.0/3.0 Databases\", \n");
	mexPrintf("\t     pp. 241-244, Proceedings of ICSLP 2002.\n");
	mexPrintf("\n");
	mexPrintf("\t[2]  C.-P. Chen, J. Bilmes and K. Kirchhoff, \"Low-Resource Noise-Robust\n");
	mexPrintf("\t     Feature Post-Processing on Aurora 2.0\", pp. 2445-2448, Proceedings \n");
	mexPrintf("\t     of ICSLP 2002.\n");
	mexPrintf("\n");
	mexPrintf(" Implementation by Tobias May, 2008-2015, Philips Research Eindhoven \n");
	mexPrintf("\n"); 
}    


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *input, *output, tmp1, tmp2, norm;
	int    ii, jj, kk, nSamples, nChannels, orderARMA, dim;
 	int    exit = false;
	
	// Check for proper number of arguments
  	if ((nrhs < 1) || (nlhs > 1)){
		usage();
    	exit = true;   
  	} 

 	if(!exit){
  		// Check the dimensions of the feature space
	  	nChannels = (int)mxGetM(INPUT);
  		nSamples  = (int)mxGetN(INPUT);

		// Assign pointer to the feature space and convert to valarray
		input = mxGetPr(INPUT);

	    // Create a matrix for the return argument 
		OUTPUT = mxCreateDoubleMatrix(nChannels, nSamples, mxREAL);
  		output = mxGetPr(OUTPUT);

		// Get ARMA filter order
		orderARMA = (int)mxGetScalar(ARMAORDER);

        // Get dimension
    	if (nrhs < 3){
            dim = 2;
        }
        else{
            dim = (int)mxGetScalar(DIM);
        }
        
		// Normalization constant
        norm = 2.00 * orderARMA + 1.00;
        
        if (dim == 1){
            
            // Loop over the number of samples
            for (ii = 0; ii < nSamples; ii++){
                
                // Loop over the number of channels
                for (jj = 0; jj < nChannels; jj++){
                                        
                    if (jj < orderARMA || jj >= (nChannels - orderARMA)){
                        // Copy input to output matrix
                        output[jj+ii*nChannels] = input[jj+ii*nChannels];
                    }
                    else{
                        // Reset filter states
                        tmp1 = 0.0;
                        tmp2 = 0.0;
                        
                        // ARMA filtering stage 1
                        for (kk = 1; kk <= orderARMA; kk++){
                            tmp1 += input[jj-kk+(ii*nChannels)];
                        }
                        
                        // ARMA filtering stage 2
                        for (kk = 0; kk <= orderARMA; kk++){
                            tmp2 += input[jj+kk+(ii*nChannels)];
                        }
                        output[jj+ii*nChannels] = (tmp1 + tmp2)/norm;
                    }
                }
            }
        }
        else{
            
            // Loop over the number of channels
            for (jj = 0; jj < nChannels; jj++){
                
                // Loop over the number of samples
                for (ii = 0; ii < nSamples; ii++){
                                        
                    if (ii < orderARMA || ii >= (nSamples - orderARMA)){
                        // Copy input to output matrix
                        output[jj+ii*nChannels] = input[jj+ii*nChannels];
                    }
                    else{
                        // Reset filter states
                        tmp1 = 0.0;
                        tmp2 = 0.0;
                        
                        // ARMA filtering stage 1
                        for (kk = 1; kk <= orderARMA; kk++){
                            tmp1 += input[jj-kk+(ii*nChannels)];
                        }
                        
                        // ARMA filtering stage 2
                        for (kk = 0; kk <= orderARMA; kk++){
                            tmp2 += input[jj+kk+(ii*nChannels)];
                        }
                        output[jj+ii*nChannels] = (tmp1 + tmp2)/norm;
                    }
                }
            }           
        }
	}
}
				
			