function [out,dctM] = applyDCT(input,N,type,bRemove1st)
%applyDCT   Apply discrete cosine transform (DCT) to input matrix.
%   
%USAGE
%   [out,dctM] = applyDCT(input)
%   [out,dctM] = applyDCT(input,N,type,bRemove1st)
% 
%INPUT ARGUMENTS
%        input : input matrix, e.g. FBE features [nFilter x nFrames], the
%                DCT is applied across columns
%            N : number of DCT coefficients (default, N = 13)
%         type : select DCT type, either 2 or 3 (default, type = 3)
%   bRemove1st : remove the first DCT coefficient, which reflects the DC
%                componens and, therefore, is strongly affected by noise
%                (default, bRemove1st = false)
% 
%OUTPUT ARGUMENTS
%	       out : DCT coefficients [N x nFrames]
% 
%   See also calcMFCCs.

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/06/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(N);          N          = 13;    end
if nargin < 3 || isempty(type);       type       = 3;     end
if nargin < 4 || isempty(bRemove1st); bRemove1st = false; end

% Determine input size
dim = size(input,1);

% Increase N by one and remove the DC component aferwards
if bRemove1st
    N = N + 1;
end

% Check is sufficient data is available
if N > dim
    error('Number of DCT coefficient is larger than the number of columns.')
end

% Select DCT type
switch type
    case 2
        % DCT type II
        bUnitary = true;
    case 3
        % DCT type III
        bUnitary = false;
    otherwise
        error('Requested DCT type is not supported.')
end


%% CREATE AND APPLY DCT MATRIX
% 
% 
% Allocate memory for DCT matrix
dctM = zeros(N,dim);

% Loop over the number of DCT coefficients
for ii = 1:N
    dctM(ii,:) = sqrt(2/dim) * cos((ii-1)*(1:2:(2*dim-1))/(2*dim)*pi);
end

% DCT type II
if bUnitary
    % Unitary DCT
    dctM(1,:) = dctM(1,:)/sqrt(2);
end

% Apply DCT matrix
out = dctM * input;

% Remove DC coefficient
if bRemove1st
    out  = out(2:end,:);
    dctM = dctM(2:end,:);
end