function [mfcc,FBE,cfHz] = calcMFCCs(input,fsHz,blockSec,stepSec,fRangeHz,nFilter,bRemoveDC,bPreEmph,nCoeff,comp,bRemove1st)
%calcMFCCs   Compute Mel frequency cepstral coefficient (MFCC) features
% 
% [mfcc,FBE] = calcMFCCs(input,fsHz)
% [mfcc,FBE] = calcMFCCs(input,fsHz,blockSec,stepSec,fRangeHz,nFilter,...
%                        bRemoveDC,bPreEmph,nCoeff,comp,bRemove1st)
% 
%INPUT ARGUMENTS
%        input : input signal [nSamples x 1]
%         fsHz : sampling frequency in Hertz
%     blockSec : block size in seconds (default, blockSec = 25E-3)
%      stepSec : step size in seconds  (default, stepSec = 10E-3)
%     fRangeHz : lower and upper frequency limit in Hertz 
%                (default, fRangeHz = [300 3700])
%      nFilter : number of triangular filters (default, nFilter = 20)
%    bRemoveDC : remove DC signal offset prior to FBE feature extraction
%                (default, bRemoveDC = true)
%     bPreEmph : apply first order whitening filter 
%                (default, bPreEmphasis = true)
%       nCoeff : number of cepstral coefficients
%                (default, nCoeff = 13)
%         comp : string specifying type of compression, see compress.m for
%                a list of supported methods, e.g. 'log' or 'cuberoot' 
%                (default,comp = 'cuberoot')
%   bRemove1st : remove the first MFC coefficient, which reflects the DC
%                componens and, therefore, is strongly affected by noise
%                (default, bRemove1st = true)
% 
%OUTPUT ARGUMENTS
%         mfcc : mel frequency cepstral coefficients [nCoeff x nFrames]
%          FBE : filterbank energy (FBE) features   [nFilter x nFrames]
% 
%   calcMFCCs(...) plots the MFCC features in a new figure.
% 
%   See also calcFBE.
% 
%REFERENCES
%   [1] S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G.
%       Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland,
%       "The HTK Book (for HTK Version 3.4.1)", Engineering Department,
%       Cambridge University, 2006.   

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/06/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 11
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3  || isempty(blockSec);   blockSec    = 25E-3;      end
if nargin < 4  || isempty(stepSec);    stepSec     = 10E-3;      end
if nargin < 5  || isempty(fRangeHz);   fRangeHz    = [300 3700]; end
if nargin < 6  || isempty(nFilter);    nFilter     = 20;         end
if nargin < 7  || isempty(bRemoveDC);  bRemoveDC   = true;       end
if nargin < 8  || isempty(bPreEmph);   bPreEmph    = true;       end
if nargin < 9  || isempty(nCoeff);     nCoeff      = 13;         end
if nargin < 10 || isempty(comp);       comp        = 'cuberoot'; end
if nargin < 11 || isempty(bRemove1st); bRemove1st  = true;       end


% ========================================
% MFCC-related parameters according to [1]
% ========================================
% 
typeDCT  = 3;    % Type of DCT 
L        = 22;   % Liftering parameter

% Determine size of input signal
nChannels = size(input,2);

% Check if input is monaural
if nChannels ~= 1
    error('Monaural input required.')
end

% Check if a sufficient number of bands is available
if nFilter < (nCoeff + bRemove1st)
    error(['Number of filter must be at least equal to the number of ',...
        'requested MFC coefficients + bRemoveDC. Increase the number ',...
        'of filters.'])
end


%% CALCULATE FBE FEATURES
% 
% 
% Get magnitude of FBE features 
[FBE,cfHz] = calcFBE(input,fsHz,blockSec,stepSec,fRangeHz,nFilter,...
    bRemoveDC,bPreEmph);


%% COMPRESS FBE FEATURES
% 
% 
% Apply compression
FBE = compress(FBE,comp);


%% TRANSFORM COMPRESSED FBE FEATURES TO CEPSTRAL FEATURES
% 
% 
% Apply DCT
mfcc = applyDCT(FBE,nCoeff,typeDCT,bRemove1st);

% Perform liftering, which aims at equalizing the feature range of the
% resulting MFC coefficients
mfcc = applyLiftering(mfcc,L);


%% PLOT RESULTS
% 
% 
if nargout == 0
    
    nFrames   = size(FBE,2);
    timeSec   = (1:numel(input))/fsHz;
    framesSec = (0:nFrames-1) * stepSec + (blockSec/2);
    
    figure;
    ax(1) = subplot(311);
    plot(timeSec,input);
    title('Time domain')
    
    ax(2) = subplot(312);
    imagesc(framesSec,cfHz,FBE);axis xy
    title('FBE features')
    
    ax(3) = subplot(313);
    imagesc(framesSec,1:nCoeff,mfcc);axis xy
    title('MFCC features')
        
    xlabel('Time (s)')
    
    linkaxes(ax,'x');
    xlim([framesSec(1) framesSec(end)]);
end
