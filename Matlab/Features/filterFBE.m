function out = filterFBE(fbEnergy,dim,method)
%filterFBE   Decorrelate filterbank energy (FBE) features
%   
%USAGE
%        out = filterFBE(fbEnergy)
%        out = filterFBE(fbEnergy,dim,method)
% 
%INPUT ARGUMENTS
%   fbEnergy : filterbank energy features [nFrames x nChannels]
%        dim : dimension across which the FBE should be filtered 
%              (default, dim = 2)
%       type : string specifying differentiation method 
%                'first' : first-order difference 
%              'central' : central difference (default)
%                 'lyon' : Lyon's differentiator
% 
%OUTPUT ARGUMENTS
%        out : decorrelated filterbank energy features [nFrames x nChannels]
% 
%   filterFBE(...) plots the decorrelated FBE features in a new figure.
% 
%REFERENCES
%   [1] C. Nadeu, D. Macho and J. Hernando, "Time and frequency filtering
%       of filter-bank energies for robust HMM speech recognition", Speech
%       Communication, vol. 34(1-2), pp.93-114, 2001.   
% 
%   [2] R. Lyons, "A Differentiator with a Difference", in Streamlining
%       Digital Signal Processing: A Tricks of the Trade Guidebook (ed R.
%       G. Lyons), chapter 19, pp. 199-202, John Wiley & Sons, Inc.,
%       Hoboken, NJ, USA, 2007.    

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/18
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(dim);    dim    = 2;         end
if nargin < 3 || isempty(method); method = 'central'; end

% Determine dimensionality of x
xdim = size(fbEnergy);

% Check dimensionality
if numel(xdim) > 2
    error('Input must be two-dimensional')
end


%% CREATE IMPULSE RESPONSE
% 
% 
% Select type
switch lower(method)
    case 'first'
        % First difference high-pass characteristic
        hDiff = [1 -1];
    case 'central'
        % Central difference which does not amplify high frequencies. It's
        % linear operation ends around fHz = 0.08 * fsHz [2]. 
        hDiff = [1 0 -1];
    case 'lyon'
        % Lyon's differentiator [2], it's magnitude response stays linear
        % up to 0.17 * fsHz.
        hDiff = [-1/16 0 1 0 -1 0 1/16];
    otherwise
        error('Method is not supported.')
end


%% PAD INPUT DATA
% 
% 
% Filter order
order = length(hDiff)-1;

% Determine filter delay
delay = order/2;

% Pad data with zeros
if dim == 1
    xx = [zeros(floor(delay),xdim(2)); fbEnergy; zeros(ceil(delay),xdim(2))];
else
    xx = [zeros(xdim(1),floor(delay)) fbEnergy zeros(xdim(1),ceil(delay))];
end


%% DECORRELATE FILTERBANK ENERGY FEATURES
% 
% 
% Allocate memory
out = zeros(xdim);

if dim == 2
    % Loop over number of channels
    for ii = 1 : xdim(dim)
        idx = (1:order+1) + ii-1;
        out(:,ii) = sum(bsxfun(@times,xx(:,idx),hDiff),dim);
    end
else
    % Loop over number of channels
    for ii = 1 : xdim(dim)
        idx = (1:order+1) + ii-1;
        out(ii,:) = sum(bsxfun(@times,xx(idx,:),hDiff'),dim);
    end
end

            
%% SHOW FEATURES BEFORE AND AFTER DECORRELATION
% 
% 
% Plot results
if nargout == 0
    figure;
    subplot(211)
    if dim == 1
        imagesc(fbEnergy)
        xlabel('Number of frequency channels')
        ylabel('Number of frames')
    else
        imagesc(fbEnergy')
        xlabel('Number of frames')
        ylabel('Number of frequency channels')
    end
    axis xy;
    colorbar;
    
    title('FBE');
    
    subplot(212)
    if dim == 1
        imagesc(out)
        xlabel('Number of frequency channels')
        ylabel('Number of frames')
    else
        imagesc(out')
        xlabel('Number of frames')
        ylabel('Number of frequency channels')
    end
    axis xy;
    colorbar;
       
    title('Decorrelated FBE');
end