function [wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,bDown2DC,bUp2Nyquist)
%createFB_Triang   Create FFT weights for MEL-spaced triangular filterbank
%   
%USAGE
%   [wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft)
%   [wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,bDown2DC,bUp2Nyquist)
% 
%INPUT ARGUMENTS
%          fsHz : sampling frequency in Hertz
%          nfft : number of FFT bins
%       nFilter : number of triangular filters (default, nFilter = 25)
%      fRangeHz : lower and upper frequency limit in Hertz 
%                 (default, fRangeHz = [50 fsHz/2])
%      bDown2DC : lowest filter remains at 1 down to DC frequency component             
%                 (default, bDown2DC = false)
%   bUp2Nyquist : highest filter remains at 1 up to nyquist frequency
%                 (default, bUp2Nyquist = false)
% 
%OUTPUT ARGUMENTS
%    wts : sparse matrix containing the weights [nFilter x (mx-mn)]
%	  mn : the lowest FFT bin with a non-zero filter weight
%	  mx : the highest FFT bin with a non-zero filter weight
%   cfHz : centre frequencies of triangular filters [1 x nFilter]
% 
%   createFB_Triang(...) plots the frequency response of the filterbank in
%   a new figure. 
% 
%   See also applyWeights_FFT, calcFBE and calcMFCCs.
% 
%ACKNOWLEDGEMENT
%   The core functionality is based on "melbankm.m", a function from the 
%   VOICEBOX Matlab toolbox.
% 
%REFERENCES
%   [1] S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G.
%       Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland,
%       "The HTK Book (for HTK Version 3.4.1)", Engineering Department,
%       Cambridge University, 2006. 

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/06/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 6
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(nFilter);     nFilter     = 25;          end
if nargin < 4 || isempty(fRangeHz);    fRangeHz    = [50 fsHz/2]; end
if nargin < 5 || isempty(bDown2DC);    bDown2DC    = false;       end
if nargin < 6 || isempty(bUp2Nyquist); bUp2Nyquist = false;       end


%% IMPLEMENT FILTERBANK
% 
% 
% Express fLow and fHigh as a fraction in relation to fs
fLow  = fRangeHz(1) / fsHz;
fHigh = fRangeHz(2) / fsHz;

% Implement FFT-based filterbank with MEL-spaced triangular filters
[wts,cfMEL,mn,mx] = melbankm(nFilter,nfft,fsHz,fLow,fHigh,'st');

% Convert MEL to Hertz
cfHz = mel2frq(cfMEL);

% Number of FFT bins (single-sided spectrum)
nFreqs = fix(1+nfft/2);

% Use DC, lowest filter remains at 1 down to 0 frequency
if bDown2DC
    wts = [zeros(nFilter,mn-1) full(wts)];
    wts(1,1:argmax(wts(1,:))) = 1;
    wts = sparse(wts);
    mn  = 1;
end

% Highest filter remains at 1 up to nyquist frequency
if bUp2Nyquist
    wts = [full(wts) zeros(nFilter,nFreqs-mx)];
    wts(end,argmax(wts(end,:)):end) = 1;
    wts = sparse(wts);
    mx  = nFreqs;
end


%% PLOT FILTERBANK
% 
% 
% Plot filter transfer function
if nargout == 0
    % Frequency axis
    fHz = (0:(nfft/2))'/(nfft/2)/2 * fsHz;
    
    fontSize  = 12;
    lineWidth = 2;
    strCol = {[0 0 0 ] [0.45 0.45 0.45]};
    figure; hold on;
    for ii = 1:nFilter
        plot(fHz(mn:mx),20*log10(wts(ii,:) + eps),'linewidth',lineWidth,...
            'color',strCol{1+mod(ii-1,2)});
    end
    set(gca,'xscale','log')
    title('Triangular filterbank')
    xlabel('Frequency (Hz)','FontSize',fontSize)
    ylabel('Filter attenuation (dB)','FontSize',fontSize)
    xlim([fHz(1) fHz(end)])
    ylim([-40 5])
end

