function [out,STATS] = scaleDataAdaptive(in,STATS,stepSec,tauSec)
%scaleDataAdaptive   Adaptive scaling according to normalization parameters.
%
%USAGE
%      OUT = scaleDataAdaptive(IN,STATS,STEPSEC,TAUSEC)
%   
%INPUT ARGUMENTS
%        IN : data matrix arranged as [nSamples x nChannels]
%     STATS : structure array created by normalizeData.m with the following
%             fields 
%             .method - normalization method
%             .dim    - dimension 
%             .data   - normalization factors 
%   STEPSEC : stepsize between two adjacent samples in seconds
%    TAUSEC : length of normalization window in seconds across which the
%             statistics are estimated adaptively (default, TAUSEC = 2)
% 
%OUTPUT ARGUMENTS
%      OUT : scaled data [nSamples x nChannels]
%    STATS : structure array containing the adaptive states
% 
%   See also scaleData and normalizeData.
% 
%NOTE
%   When training a classifier with features, this function is typically
%   used in combination with the function normalizeData: First, the
%   feature matrix is normalized during training. Then, the normalization
%   statistics measured during training are applied during testing to
%   scale the features accordingly. In addition, the speed of adaptaion can
%   be controlled by a time constant.
%  
%   % Create 2D feature matrix
%   featureSpace = randn(10,250);
%
%   % Split data into training and testing set
%   fTrain = featureSpace(:,1:100);
%   fTest  = featureSpace(:,101:end);
% 
%   % Normalize feature matrix during training 
%   [fTrainNorm,STATS] = normalizeData(fTrain,'meanvar',2);
%
%   % Normalize feature matrix during testing. Starting from the initial
%   % normalization statistics measured during training, the statistics are
%   % recursively updated using a time constant of 500E-3, assuming the
%   % feature values were computed with a stepsize of 10E-3.   
%   fTestNorm = scaleDataAdaptive(fTest,STATS,10E-3,500E-3);


%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/22
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS 
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 4 || isempty(tauSec); tauSec = 2; end

% Check dimensionality of input data
if numel(size(in)) > 2
    error('Input must be two-dimensional')
end

% Prevent division overflow
epsilon = 1e-10;


%% PERFORM NORMALIZATION
% 
%
% Check if supplied normalization factor is a struct
if ~isstruct(STATS) || ...
        ~isfield(STATS,'dim') || ...
        ~isfield(STATS,'method') || ...
        ~isfield(STATS,'data')
    error('STATS must be a struct created by normalizeData.m')
end
        
% Select normalization method
switch lower(STATS.method)
    case 'mean'
        
        % Check dimension of supplied "stats"
        if all( size(in,setdiff(1:2,STATS.dim)) ~= numel(STATS.data) )
            error('Dimension mismatch between input and normalization factors')
        end
        
        % Determine size of input data
        [nSamples,nChannels] = size(in);

        % Allocate memory for the output 
        out = zeros(nSamples,nChannels);
        
        % Look for existing adaptive filter states
        if isfield(STATS,'states')
            % Use existing states
            y = STATS.states;
        else
            % Initialize state of mean estimation
            y = STATS.data;
        end
        
        % Compute filter coefficients
        aLP = exp(-stepSec/tauSec);
        bLP = 1 - aLP;
                
        if STATS.dim == 1
            % Loop over number of samples
            for ii = 1 : nSamples
                x = in(ii,:);
                % Estimate mean
                y = aLP * y + bLP * x;
                % Detrend
                x = x - y;
                % Store normalized output
                out(ii,:) = x;
            end
        else
            % Loop over number of samples
            for ii = 1 : nChannels
                x = in(:,ii);
                % Estimate mean
                y = aLP * y + bLP * x;
                % Detrend
                x = x - y;
                % Store normalized output
                out(:,ii) = x;
            end
        end
        
        % Store states
        STATS.states = y;
                
    case 'meanvar'
        
        % Check dimension of supplied "stats"
        if all( size(in,setdiff(1:2,STATS.dim)) ~= numel(STATS.data)/2 )
            error('Dimension mismatch between input and normalization factors')
        end
             
        % Determine size of input data
        [nSamples,nChannels] = size(in);

        % Allocate memory for the output 
        out = zeros(nSamples,nChannels);
        
        % Look for existing adaptive filter states
        if isfield(STATS,'states')
            if STATS.dim == 1
                % Use existing states
                y = STATS.states(1,:);
                z = STATS.states(2,:);
            else
                % Use existing states
                y = STATS.states(:,1);
                z = STATS.states(:,2);                
            end
        else
            if STATS.dim == 1
                % Initialize state of mean estimation
                y = STATS.data(1,:);
                % Initialize state of variance estimation
                z = STATS.data(2,:);
            else
                % Initialize state of mean estimation
                y = STATS.data(:,1);
                % Initialize state of variance estimation
                z = STATS.data(:,2);                
            end
        end
        
        % Compute filter coefficients
        aLP = exp(-stepSec/tauSec);
        bLP = 1 - aLP;
        
        if STATS.dim == 1
            % Loop over number of samples
            for ii = 1 : nSamples
                x = in(ii,:);
                % Estimate mean
                y = aLP * y + bLP * x;
                % Detrend
                x = x - y;
                % Estimate variance
                z = aLP * z + bLP * x .* x;
                % Normalize variance
                x = x ./ (sqrt(z) + epsilon);
                
                % Store normalized output
                out(ii,:) = x;
            end
            % Store states
            STATS.states = cat(1,y,z);
        else
            % Loop over number of samples
            for ii = 1 : nChannels
                x = in(:,ii);
                % Estimate mean
                y = aLP * y + bLP * x;
                % Detrend
                x = x - y;
                % Estimate variance
                z = aLP * z + bLP * x .* x;
                % Normalize variance
                x = x ./ (sqrt(z) + epsilon);
                
                % Store normalized output
                out(:,ii) = x;
            end
            % Store states
            STATS.states = cat(2,y,z);
        end
            
    otherwise
        error('Normalization ''%s'' is not supported',STATS.method);
end 


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************