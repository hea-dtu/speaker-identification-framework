%SIDDemo   Perform large-scale speaker identification experiments


%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/07/02
%   ***********************************************************************

clear
close all
clc

addpath SID


%% SID-SPECIFIC PARAMETERS 
% 
% 
% Save simulations to a mat file
bSaveSimulation = true;

% Visualize simulation results
bPlotSimulation = true;

% Activate parallel computing
bParfor = true;

% Experimental file(s)
strEXP = {'EXP_Feature_Normalization'      ...
          'EXP_Missing_Data_Techniques'    ...
          'EXP_LC_Direct_Masking'          ...
          'EXP_LC_Bounded_Marginalization' ...
          'EXP_Soft_Mask_Missing_Data'      };
      
      
%% RUN SID EXPERIMENT
% 
% 
% Batch processing
R = SIDBatch(strEXP,bParfor,bSaveSimulation,bPlotSimulation);
