function bBinary = isBinary(data)
%isBinary   Check if input is binary.
%
%USAGE 
%   isBinary = isBinary(data)
%
%INPUT ARGUMENTS
%   data : input data
%
%OUTPUT ARGUMENTS
%   OUT : results [nFrames x 1]
%
%NOTE
%   This is a template.
% 
%REFERENCES
%   [1] P. Boersma, "Accurate Short-Term Analysis of the Fundamental
%       Frequency and the Harmonics-to-Noise Ratio of Sampled Sounds", IFA
%       Proceedings 17, 1993.
%
%   See also ...

%   Developed with Matlab 7.9.0.529 (R2009b). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2009 
%              TUe Eindhoven and Philips Research  
%              t.may@tue.nl      tobias.may@philips.com
%
%   History :
%   v.0.1   2009/11/23
%   ***********************************************************************


% Check if all entries are either 0 or 1
bBinary = all(data(:) == 0 | data(:) == 1);


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************