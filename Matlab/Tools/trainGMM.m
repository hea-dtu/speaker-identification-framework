function C = trainGMM(data,labels,method,K,nIter,thresEM,cv,reg)
%trainGMM   Train GMM


%   Developed with Matlab 7.5.0.342 (R2007b). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2009-2010 
%              University of Oldenburg and TU/e Eindhoven 
%              tobias.may@uni-oldenburg.de   t.may@tue.nl
%
%   History :
%   v.0.1   2009/11/23
%   ***********************************************************************



%% ***********************  CHECK INPUT ARGUMENTS  ************************
% 
% 
% Set default values
if nargin < 3 || isempty(method);  method  = 'netlab'; end
if nargin < 4 || isempty(K);       K       = 4;        end
if nargin < 5 || isempty(nIter);   nIter   = 100;      end
if nargin < 6 || isempty(thresEM); thresEM = 1e-4;     end
if nargin < 7 || isempty(cv);      cv      = 'full';   end
if nargin < 8 || isempty(reg);     reg     = 1E-9;     end

% Select method
switch lower(method)
    case 'netlab'
        C = trainGMM_Netlab(data,labels,K,nIter,thresEM,cv,reg);
    case 'matlab'
        C = trainGMM_Matlab(data,labels,K(1),nIter,thresEM,cv,reg);
    otherwise
        error('Method is not supported.')
end

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************