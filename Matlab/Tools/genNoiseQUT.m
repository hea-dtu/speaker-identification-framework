function noise = genNoiseQUT(noiseType,fsRef,nSamples,bRand,bTrain)
%genNoiseQUT   Create noise signals from the QUT database.
%   
%USAGE
%       noise = genNoiseQUT(noiseType,fs,nSamples,bRand,bTrain)
%
%INPUT ARGUMENTS
%   noiseType : string specifying noise type
%               'qut_cafe'       = outdooor cafe environment
%               'qut_foodcourt'  = indoor shopping centre food-court
%               'qut_city'       = roadside inner-city inter-section
%               'qut_kg'         = roadside outer-city inter-section
%               'qut_carwindown' = car driving with windows down
%               'qut_carwinup'   = car driving with windows up
%               'qut_pool'       = enclosed indoor pool
%               'qut_carpark'    = partially-enclosed carpark
%          fs : sampling frequency in Hz
%    nSamples : length of noise signal in samples
%       bRand : add random offset to realize different (random) reali-
%               zations of one noise type (default, bRandOffset = true)
%      bTrain : derive noise signals from training or testing database
%               (default, bTrain = true)
%
%OUTPUT ARGUMENTS
%       noise : noise signal [nSamples x 1]
% 
%NOTE
%   The user-specific path to the audio signals has to be specified in
%   the function getRoot.m. The corresponding noise files can be found at:
%   https://qut.edu.au/research/saivt
% 
%REFERENCES
%   [1] D. Dean, S. Sridharan, R. Vogt and M. Mason, "The QUT-NOISE-TIMIT
%       Corpus for the Evaluation of Voice Activity Detection Algorithms", 
%       Proc. Interspeech, 2010. 

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/04/29
%   v.0.2   2014/07/04 introduced user-specific root directories
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 5
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default parameter
if nargin < 4 || isempty(bRand);  bRand  = true; end
if nargin < 5 || isempty(bTrain); bTrain = true; end


%% SELECT NOISE TYPE
% 
% 
% Select noise type
switch lower(noiseType)
    case 'qut_cafe'
        fname = 'CAFE.wav';
    case 'qut_foodcourt'
        fname = 'FOODCOURT.wav';
    case 'qut_carwindown'
        fname = 'CAR_WINDOWN.wav';
    case 'qut_carwinup'
        fname = 'CAR_WINUP.wav';
    case 'qut_carpark'
        fname = 'REVERB_CARPARK.wav';
    case 'qut_pool'
        fname = 'REVERB_POOL.wav';
    case 'qut_city'
        fname = 'STREET_CITY.wav';
    case 'qut_kg'
        fname = 'STREET_KG.wav';
    otherwise
        error('Noise type "%s" is not recognized!',noiseType);
end

% Root directory of noise signals
if bTrain
    root = [getRoot('noise'),filesep,'QUT',filesep,'1_TRAIN',filesep];
else
    root = [getRoot('noise'),filesep,'QUT',filesep,'2_TEST',filesep];
end

% Check if root directory does exist
if ~isdir(root)
    error(['Root directory for the noise "%s" does not exist. Please ',...
        'check the list of user-specific root directories in the ',...
        'function getRoot.m.'],noiseType);
end


%% COMPUTE SIGNAL RANGE
% 
% 
% Read wave file
info = audioinfo([root,fname]);
dim  = [info.TotalSamples info.NumChannels];
fs   = info.SampleRate;

% Ensure that we have enough samples in case we need to down-sample
fsRatio = ceil(fs / fsRef);

% Check if signal range is sufficient
if dim(1) < ceil(nSamples * fsRatio)
    bReplicate = true;
    nRep       = ceil(ceil(nSamples * fsRatio)/dim(1));
else
    bReplicate = false;
    nRep       = 1;
end

% Select random realization of the noise recordings
if bRand
    % Create random offset
    offset = rnsubset(1,nRep*dim(1) - ceil(nSamples * fsRatio));
else
    offset = 0;
end


%% READ AUDIO SIGNAL
% 
% 
% Read audio data
if bReplicate
    noise = audioread([root,fname]); 
    noise = circshift(repmat(noise,[nRep 1]),-offset);
else
    noise = audioread([root,fname],[1 ceil(nSamples * fsRatio)]+offset);
end


%% RESAMPLE & TRIM SIGNAL
% 
%
% Resampling
if ~isequal(fs,fsRef)
    noise = resample(noise,fsRef,fs);
end
        
% Trim edges
noise = noise(1:nSamples,1);
        

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************