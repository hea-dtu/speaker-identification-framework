function fSpace = loadMATFile(namefSpace,cacheID)
%loadMATFile   Load mat file into persistent cache

if nargin < 2 || isempty(cacheID); cacheID = 'training'; end

persistent PER_name PER_CACHE

if isnumeric(cacheID)
    cacheID = ['num',num2str(cacheID)];
end

% Check if cacheID exists
if isfield(PER_name,cacheID) && isequal(namefSpace,PER_name.(cacheID))
    % Read from cache
    fSpace = PER_CACHE.(cacheID);
else
    if isfield(PER_name,cacheID)
        warning('CacheID exists and will be overwritten, press any key to continue!')
        pause;
        
        % Free memory
        PER_CACHE.(cacheID) = [];
    end

    % Load *.mat file
    fSpace = load(namefSpace);
    tmp    = fieldnames(fSpace);
    fSpace = fSpace.(tmp{:});
end

% Cache data
PER_name.(cacheID)  = namefSpace;
PER_CACHE.(cacheID) = fSpace;