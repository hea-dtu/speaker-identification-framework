function noise = genNoise(noiseType,fsHz,nSamples,bRand,bTrain)
%genNoise   Create noise signals from various databases.
%   
%USAGE
%       noise = genNoise(noiseType,fsHz,nSamples)
%       noise = genNoise(noiseType,fsHz,nSamples,bRand,bTrain)
%
%INPUT ARGUMENTS
%   noiseType : string defining noise databse & noise type
% 
%   ICRA        Noise signals from the ICRA database
%               'icra_01' = male speech-shaped noise, normal
%               'icra_02' = male speech-shaped noise, raised
%               'icra_03' = male speech-shaped noise, loud
%               'icra_04' = female speech-shaped & modulated noise, normal
%               'icra_05' = male speech-shaped & modulated noise, normal
%               'icra_06' = speech-shaped & modulated noise, 2-sp
%               'icra_07' = speech-shaped & modulated noise, 6-sp, normal
%               'icra_08' = speech-shaped & modulated noise, 6-sp, raised
%               'icra_09' = speech-shaped & modulated noise, 6-sp, loud 
% 
%   NOISEX      Noise signals from the NOISEX database
%               'noisex_white' 
%               'noisex_pink'
%               'noisex_hfchannel' 
%               'noisex_babble' 
%               'noisex_factory1'
%               'noisex_factory2'
%               'noisex_jet1'
%               'noisex_jet2'
%               'noisex_destroyerengine' 
%               'noisex_destroyeroperation'
%               'noisex_cockpit'
%               'noisex_militaryvehicle' 
%               'noisex_tank' 
%               'noisex_machinegun' 
%               'noisex_car'
% 
%   SAM noise   sinusoidal amplitude-modulated Gaussian noise
%               'sam_<freq>_<depth>'
%                <freq>  = modulation frequency in Hertz
%                <depth> = modulation depth (default, depth = 0.5)
% 
%                'sam_8_1' produces 8-Hz modulated white noise with a depth
%                 of 1 
%                  
%   PSAM noise  sinusoidal amplitude-modulated pink noise
%               'psam_<freq>_<depth>'
%                <freq>  = modulation frequency in Hertz
%                <depth> = modulation depth (default, depth = 0.5)
% 
%                'psam_8_1' produces 8-Hz modulated pink noise with a depth
%                of 1 
% 
%        fsHz : sampling frequency in Hz
%    nSamples : length of noise signal in samples
%       bRand : add random offset to create different (random) reali-
%               zations (default, bRandOffset = true)
%      bTrain : derive noise signals from training or testing database
%               (default, bTrain = true)
%
%OUTPUT ARGUMENTS
%       noise : noise signal [nSamples x 1]
% 
%   See also genNoiseSAM, genNoisePSAM, genNoiseICRA and genNoiseNOISEX.

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/04/11
%   ***********************************************************************



%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 5
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default parameter
if nargin < 5 || isempty(bTrain); bTrain = true; end
if nargin < 4 || isempty(bRand);  bRand  = true; end


%% GENERATE NOISE SIGNAL
% 
% 
% Detect first underscore
idxUscore = strfind(noiseType,'_');

% Detect noise database
database = upper(noiseType(1:idxUscore(1)-1));

% Generate noise file 
if exist(['genNoise',database],'file')
    noise = feval(['genNoise',database],noiseType,fsHz,nSamples,bRand,bTrain);
else
    error('The noise generation for the database ''%s'' is not supported.',database);
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************