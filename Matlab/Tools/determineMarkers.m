function [strMarker,strCol] = determineMarkers(strMethods)
%determineMarkers   Determine markers and line colors based on SID presets.

% List of possible markers
refMarker = {'s' 'o' '^' 'v' '<' '>' 'p' 'h' 'd' '+' '*' 'x' '.' 'none'};
nMethods  = numel(strMethods);
strMarker = cell(nMethods,1);
strCol    = zeros(nMethods,3);

% Only retain first part of the preset name until the first blank space
idxBlank = strfind(strMethods,' ');
groupPresets = zeros(nMethods,1);
iter = 1;
for ii = 1 : nMethods
    if isempty(idxBlank{ii})
        strMethod = strMethods{ii};
    else
        strMethod = strMethods{ii}(1:idxBlank{ii}(1)-1);
    end
    if ii > 1
        if ~isequal(pastMethod,strMethod)
            iter = iter + 1;
            pastMethod = strMethod;
        end
    else
        pastMethod = strMethod;
    end
    groupPresets(ii) = iter;
end

for ii = 1 : groupPresets(end)
    idxGroup = find(groupPresets==ii);
    strCol(idxGroup,:) = colormapVoicebox(numel(idxGroup));
    for jj = 1:numel(idxGroup)
        strMarker{idxGroup(jj)} = refMarker{ii};
    end
end