function data = compress(data,method)
%compress   Compress input signal.
%   
%USAGE
%     data = compress(data,method)
%
%INPUT ARGUMENTS
%     data : N-dimensional data
%   method : string specifying type of compression
%            'log'        - natural logarithm
%            'log_md'     - natural logarithm with an offset of 1, such
%                           that 0s do not produce INFs.
%            'log10'      - base 10 logarithm
%            'loga'       - scale-able logarithmic compression 
%            'cuberoot'   - cuberoot compression ^(1/3) 
%            '20log10_80' - Log-compression with a dynamic range of 80 dB 
%            'nonlinear'  - non-linear hair cell compression [1]
%            '1over15'    - power law compression with ^(1/15) [1]
%            'inst'       - instantaneous compression with ^0.4 [2]
% 
%OUTPUT ARGUMENTS
%     data : compressed N-dimensional data
% 
%REFERENCES 
%   [1] N. Moritz, J. Anem�ller and B. Kollmeier. "Amplitude Modulation
%       Filters as Feature Sets for Robust ASR: Constant Absolute or
%       Relative Bandwidth?", Interspeech, 2012.    
% 
%   [2] S. D. Ewert and T. Dau. "Characterizing frequency selectivity for
%       envelope fluctuations", JASA, vol. 108(3), pp. 1181-1196, 2000.
%       

%   Developed with Matlab 8.2.0.701 (R2013b). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/08/11
%   v.0.2   2014/04/02 added non-linear compression
%   ***********************************************************************
   
    
%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 2
    help(mfilename);
    error('Wrong number of input arguments!')
end


%% APPLY COMPRESSION
%
%
% Select method
switch(lower(method))
    case 'log'
        data = log(data);
        
    case 'log_md'
        % Add one, such that a lower bound of zero can be used in the
        % context of missing data classification
        data = log(data + 1);
        
    case 'log10'
        data = log10(data);
        
    case 'inst'
        % Instantaneous compression with an exponent of 0.4
        data = sign(data) .* abs(data).^0.4;
        
    case 'loga'
        a = 3;
        data = log10(1 + a * data) ./ log10(1 + a);
        
    case 'cuberoot'
        data = power(data,1/3);
    
    case '20log10_80'
        % Limit dynamic range to 80 dB
        dynamicRange = 80;
        
        floorMag = 10.^(-dynamicRange/20);
        
        data = 20 * log10(data + floorMag);
            
    case 'nonlinear'
        % Non-linear operation to mimic outer hair cell compression [1]
        data = (power(data,0.4) + log(data) + 1)/2;
		
    case '1over15'
        % Non-linear operation to mimic outer hair cell compression [1]
        data = power(data,1/15);		
        
    otherwise
        error('Type of compression is not supported!')
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************