function noise = genNoiseICRA(noiseType,fsHzRef,nSamples,bRand,bTrain)
%genNoiseICRA   Create noise signals from the ICRA database.
%   
%USAGE
%       noise = genNoiseICRA(noiseType,fsHz,nSamples,bRand,bTrain)
%
%INPUT ARGUMENTS
%   noiseType : string specifying noise type
%               'icra_01' = male speech-shaped noise, normal
%               'icra_02' = male speech-shaped noise, raised
%               'icra_03' = male speech-shaped noise, loud
%               'icra_04' = female speech-shaped & modulated noise, normal
%               'icra_05' = male speech-shaped & modulated noise, normal
%               'icra_06' = speech-shaped & modulated noise, 2-sp
%               'icra_07' = speech-shaped & modulated noise, 6-sp, normal
%               'icra_08' = speech-shaped & modulated noise, 6-sp, raised
%               'icra_09' = speech-shaped & modulated noise, 6-sp, loud 
%        fsHz : sampling frequency in Hz
%    nSamples : length of noise signal in samples
%       bRand : add random offset to create different (random) reali-
%               zations (default, bRandOffset = true)
%      bTrain : derive noise signals from training or testing database
%               (default, bTrain = true)
%
%OUTPUT ARGUMENTS
%       noise : noise signal [nSamples x 1]
% 
%NOTE
%   The user-specific path to the audio signals has to be specified in
%   the function getRoot.m. In general, noise signals are expected to be
%   separated into two folders ("1_Train" and "2_Test"), containing the
%   signals used for training and testing, respectively.  
% 
%REFERENCES
%   [1] W. A. Dreschler, H. Verschuure, C. Ludvigsen and S. Westermann,
%       "ICRA Noises: Artificial Noise Signals with Speech-like Spectral
%       and Temporal Properties for Hearing Instrument Assessment",
%       International Journal of Audiology, 40(3), pp.148-157, 2001. 

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/04/29
%   v.0.2   2014/07/04 introduced user-specific root directories
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 5
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default parameter
if nargin < 4 || isempty(bRand);  bRand  = true; end
if nargin < 5 || isempty(bTrain); bTrain = true; end


%% SELECT NOISE TYPE
% 
% 
% Select noise type
switch lower(noiseType)
    case 'icra_01'
        fname = 'ICRA_01.wav';
    case 'icra_02'
        fname = 'ICRA_02.wav';
    case 'icra_03'
        fname = 'ICRA_03.wav';
    case 'icra_04'
        fname = 'ICRA_04.wav';
    case 'icra_05'
        fname = 'ICRA_05.wav';
    case 'icra_06'
        fname = 'ICRA_06.wav';
    case 'icra_07'
        fname = 'ICRA_07.wav';
    case 'icra_08'
        fname = 'ICRA_08.wav';
    case 'icra_09'
        fname = 'ICRA_09.wav';        
    otherwise
        error('Noise type "%s" is not recognized!',noiseType);
end

% Root directory of noise signals
if bTrain
    root = [getRoot('noise'),filesep,'ICRA',filesep,'1_Train',filesep];
else
    root = [getRoot('noise'),filesep,'ICRA',filesep,'2_Test',filesep];
end

% Check if root directory does exist
if ~isdir(root)
    error(['Root directory for the noise "%s" does not exist. Please ',...
        'check the list of user-specific root directories in the ',...
        'function getRoot.m.'],noiseType);
end


%% COMPUTE SIGNAL RANGE
% 
% 
% Determine dimensions of wave file
[dim,fsHz] = readAudio([root,fname],'info');

% Ensure that we have enough samples in case we need to down-sample
fsRatio = ceil(fsHz / fsHzRef);

% Check if signal range is sufficient
if dim(1) < ceil(nSamples * fsRatio)
    bReplicate = true;
    nRep       = ceil(ceil(nSamples * fsRatio)/dim(1));
else
    bReplicate = false;
    nRep       = 1;
end

% Select random realization of the noise recordings
if bRand
    % Create random offset
    offset = rnsubset(1,nRep*dim(1) - ceil(nSamples * fsRatio));
else
    offset = 0;
end


%% READ AUDIO SIGNAL
% 
% 
% Read audio data
if bReplicate
    noise = readAudio([root,fname]); 
    noise = circshift(repmat(noise,[nRep 1]),-offset);
else
    noise = readAudio([root,fname],[],[1 ceil(nSamples * fsRatio)]+offset);
end


%% RESAMPLE & TRIM SIGNAL
% 
%
% Resampling
if ~isequal(fsHz,fsHzRef)
    noise = resample(noise,fsHzRef,fsHz);
end
        
% Trim edges
noise = noise(1:nSamples,1);
        

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************