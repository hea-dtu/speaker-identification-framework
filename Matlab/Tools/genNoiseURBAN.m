function noise = genNoiseURBAN(noiseType,fsRef,nSamples,bRand,bTrain)
%genNoiseURBAN   Create noise signals from the URBAN SOUND database.
%   
%USAGE
%       noise = genNoiseURBAN(noiseType,fs,nSamples,bRand,bTrain)
%
%INPUT ARGUMENTS
%   noiseType : string defining noise type
%               'urban_streetmusic' 
%               'urban_engine' 
%               'urban_children' 
%               'urban_airconditioner' 
%          fs : sampling frequency in Hz
%    nSamples : length of noise signal in samples
%       bRand : add random offset to realize different (random) reali-
%               zations of one noise type (default, bRandOffset = true)
%      bTrain : derive noise signals from training or testing database
%               (default, bTrain = true)
%
%OUTPUT ARGUMENTS
%       noise : noise signal [nSamples x 1]
% 
%   The user-specific path to the audio signals has to be specified in
%   the function getRoot.m. The corresponding noise files can be found at:
%   https://serv.cusp.nyu.edu/projects/urbansounddataset/
% 
%REFERENCES
%   [1] J. Salamon, C. Jacoby and J. P. Bello, "A Dataset and Taxonomy for
%       Urban Sound Research", Proceedings of the ACM International
%       Conference on Multimedia, pp. 1041-1044, 2014.   


%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/07/18
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 5
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default parameter
if nargin < 4 || isempty(bRand);  bRand  = true; end
if nargin < 5 || isempty(bTrain); bTrain = true; end


%% SELECT NOISE TYPE
% 
% 
% Select noise type
switch lower(noiseType)
    case 'urban_streetmusic'
        files = {...
            '74458' '115243' '115415' '136558' '137156' '147491'    ...
            '155127' '168906' '171478' '172519' '185374' '203654'   ...
            };
    case 'urban_engine'
        files = {...
            '15544' '17578' '17853' '23219' '39854' '39856' '50668' ...
            '50668' '62566' '94632' '102857' '103249' '103258'      ...
            '128160' '144068' '154758' '156634' '166101' '176787'   ...
            '200786' '201652' '201988'                              ...
            };
    case 'urban_children'
        files = {...
            '54067' '71173' '88569' '155219' '155263' '158593'      ...
            '160009' '160010' '164194' '165529' '170564' '176714'   ...
            '192269' '204408'                                       ...
            };
    case 'urban_airconditioner'
        files = {...
            '30204' '57320' '62461' '63724' '73524' '74507' '83502' ...
            '101729' '121285' '121286' '134717' '146690' '146709'   ...
            '146714' '146845' '147926' '151977' '162103' '165454'   ...
            '166942' '167464' '178686' '189023' '202516' '204240'   ...
            };
    otherwise
        error(['Noise type ''',noiseType,''' is not recognized!']);
end

% Root directory of noise signals
if bTrain
    root = [getRoot('noise'),filesep,'URBAN',filesep,'1_TRAIN',filesep];
else
    root = [getRoot('noise'),filesep,'URBAN',filesep,'2_TEST',filesep];
end

% Complete root directory
root = [root,noiseType(strfind(noiseType,'_')+1:end),filesep];

% Check if root directory does exist
if ~isdir(root)
    error(['Root directory for the noise "%s" does not exist. Please ',...
        'check the list of user-specific root directories in the ',...
        'function getRoot.m.'],noiseType);
end

% Randomly select noise file 
fname = [files{randperm(numel(files),1)},'.wav'];


%% COMPUTE SIGNAL RANGE
% 
% 
% Read wave file
info = audioinfo([root,fname]);
dim  = [info.TotalSamples info.NumChannels];
fs   = info.SampleRate;

% Ensure that we have enough samples in case we need to down-sample
fsRatio = ceil(fs / fsRef);

% Check if signal range is sufficient
if dim(1) < ceil(nSamples * fsRatio)
    bReplicate = true;
    nRep       = ceil(ceil(nSamples * fsRatio)/dim(1));
else
    bReplicate = false;
    nRep       = 1;
end

% Select random realization of the noise recordings
if bRand
    % Create random offset
    offset = rnsubset(1,nRep*dim(1) - ceil(nSamples * fsRatio));
else
    offset = 0;
end


%% READ AUDIO SIGNAL
% 
% 
% Read audio data
if bReplicate
    noise = audioread([root,fname]); 
    noise = circshift(repmat(noise,[nRep 1]),-offset);
else
    noise = audioread([root,fname],[1 ceil(nSamples * fsRatio)]+offset);
end


%% RESAMPLE & TRIM SIGNAL
% 
%
% Resampling
if ~isequal(fs,fsRef)
    noise = resample(noise,fsRef,fs);
end
        
% Trim edges
noise = noise(1:nSamples,1);
        

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************