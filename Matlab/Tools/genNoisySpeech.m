function [mix,target,noise] = genNoisySpeech(fName,fsHz,noiseType,snrdB,bTrain)
%genNoisySpeech   Create noisy speech mixture.
%   
%USAGE
%   [mix,target,noise] = genNoisySpeech(fName,fsHz,noiseType,snrdB,bTrain)
%
%INPUT ARGUMENTS
%       fName : filename of target signal
%        fsHz : reference sampling frequency in Hz
%   noiseType : type of background noise (see genNoise.m for more details)
%       snrdB : signal-to-noise ratio in dB
%      bTrain : select noise signals from training or testing database
% 
%OUTPUT ARGUMENTS
%         mix : noisy mixture [nSamples x 1]
%      target : target signal [nSamples x 1]
%       noise : noise signal  [nSamples x 1]
% 
%   genNoisySpeech(...) plots the three signals in a new figure.
% 
%   See also controlSNR.

%   Developed with Matlab 8.4.0.150421 (R2014b). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/07/02
%   ***********************************************************************
   
    
%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin ~= 5
    help(mfilename);
    error('Wrong number of input arguments!')
end


%% READ TARGET SIGNAL
%
%
% Load audio file
[target,fsHz] = readAudio(fName,fsHz);

% Check dimensions
if min(size(target)) > 1
    error('Single channel audio files required!')
end

% Determine dimensionality
nSamples = numel(target);


%% CREATE NOISE SIGNAL
%
%
% Generate noise
noise = genNoise(noiseType,fsHz,nSamples,true,bTrain);
    
    
%% CONTROL THE SIGNAL-TO-NOISE RATIO
%
%
% Adjust noise level to obtain predefined SNR
[mix,target,noise] = controlSNR(target,noise,snrdB);


%% SHOW NOISY MIXTURE
%
%
if nargout == 0
    
    figure;hold on;
    hM = plot(mix);
    hN = plot(noise);
    hS = plot(target);
    grid on;xlim([1 numel(mix)]);
    xlabel('Number of samples')
    ylabel('Amplitude')
    set(hM,'color',[0 0 0])
    set(hS,'color',[0 0.5 0])
    set(hN,'color',[0.5 0.5 0.5])
    legend({'mix' 'noise' 'target'})
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************