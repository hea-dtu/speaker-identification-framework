function user = getUser(varargin)
%getUser   Identify the user's computer name.
% 
%USAGE
%   user = getUser
%
%OUTPUT ARGUMENTS
%   user : string containing the user name

%   Developed with Matlab 7.4.0.287 (R2007a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2009 
%              TUe Eindhoven and Philips Research  
%              t.may@tue.nl      tobias.may@philips.com
% 
%   History :  
%   v.0.1   2009/05/11
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin > 0
    help(mfilename);
    error('Wrong number of input arguments!')
end


%% IDENTIFY NAME OF COMPUTER  
% 
% 
% Determine user name
if ispc
    user = getenv('computername');
else
    user = getenv('HOSTNAME');
end
    
% Delete blank characters from user name
user = strtrim(user);

end