function bInt = isInt(x)
%isInt Determines if input consists of integer values
%
%USAGE 
%    bInt = isInt(data)
%
%INPUT ARGUMENTS
%    data : input data
%
%OUTPUT ARGUMENTS
%    bInt : binary decision for each element

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2009/11/23
%   ***********************************************************************

% Check for proper input arguments
if nargin ~= 1
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Check if array contains integer values
bInt = mod(x,1) == 0;
