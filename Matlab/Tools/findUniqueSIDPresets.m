function [idxUnique,idxCopy] = findUniqueSIDPresets(P)
%findUniqueSIDPresets   Find unique SID presets

nPresets = numel(P);

idx2Process = 1:nPresets;
idxCopy = zeros(nPresets,1);

nIter = 0;
idxUnique = [];

% Loop until all SID presets have been processed
while ~isempty(idx2Process)
    
    % Counter
    nIter = nIter + 1;
    
    % Delete presets with identical settings
    bDelete = cat(1,true,false(numel(idx2Process)-1,1));
    
    % Loop over remaining SID presets
    for ii = 2:numel(idx2Process)
        if ...
                isequal(P(idx2Process(1)).PRE        , P(idx2Process(ii)).PRE)        && ...
                isequal(P(idx2Process(1)).FEATURE    , P(idx2Process(ii)).FEATURE)    && ...
                isequal(P(idx2Process(1)).POST       , P(idx2Process(ii)).POST)       && ...
                isequal(P(idx2Process(1)).CLASSIFIER , P(idx2Process(ii)).CLASSIFIER)
            
            bDelete(ii) = true;
        end
    end
    
    % Save unique preset index
    idxUnique = cat(2,idxUnique,idx2Process(1));
    
    % Save iteration index
    idxCopy(idx2Process(bDelete)) = nIter;
    
    % Delete identical presets
    idx2Process(bDelete) = [];
end
