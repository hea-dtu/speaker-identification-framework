function y = isOdd(x)
%isOdd   Checks if input is odd-numbered.
%
%USAGE 
%   bOdd = isOdd(data)
%
%INPUT ARGUMENTS
%   data : input data
%
%OUTPUT ARGUMENTS
%   bOdd : binary decision for each element

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2007/07/22
%   ***********************************************************************

% Check for proper input arguments
if nargin ~= 1
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Check if input consists of integer values
if all(isInt(x))
    % Check if numbers are odd-numbered
    y = x ~= (round(x./2).*2);
else
    error('Array must contain integer values only.')
end
    

