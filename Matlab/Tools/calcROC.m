function [hitfa,tpr,fpr,fnr,tnr,acc,bacc] = calcROC(predicted,reference,dim)
%calcROC   Compute a Receiver Operating Characteristic (ROC) curve.
%
%USAGE
%   [hitfa,tpr,fpr,fnr,tnr,acc,bacc] = calcROC(predict,target)
%   [hitfa,tpr,fpr,fnr,tnr,acc,bacc] = calcROC(predict,target,dim)
%
%INPUT ARGUMENTS
%   predicted : input pattern, e.g. the estimated binary mask of size
%               [nChannels x nFrames]
%   reference : reference pattern, e.g. the ideal binary mask of size
%               [nChannels x nFrames]
%         dim : dimension along which the ROC statistics should be
%               averaged, if dim is not specified or empty, a scalar value
%               will be computed for each ROC statistics
%               (default, dim = [])
%               
% 
%OUTPUT ARGUMENTS
%       hitfa : hit-false alarm rate                   [1 x 1 | 1 x nFrames | nChannels x 1]
%         tpr : true positive (hit) rate               [1 x 1 | 1 x nFrames | nChannels x 1]
%         fpr : false positive (false alarm) rate      [1 x 1 | 1 x nFrames | nChannels x 1]
%         fnr : false negative (miss) rate             [1 x 1 | 1 x nFrames | nChannels x 1]
%         tnr : true negative (correct rejection) rate [1 x 1 | 1 x nFrames | nChannels x 1]
%         acc : accuracy                               [1 x 1 | 1 x nFrames | nChannels x 1]
%        bacc : balanced accuracy                      [1 x 1 | 1 x nFrames | nChannels x 1]
%
%REFERENCES
%   [1] T. Fawcett, "An introduction to ROC analysis," Pattern Recognition
%       Letters, vol. 27(8), pp. 861-874, 2006. 

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/07/22
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(dim); dim = []; end;

% Check if both pattern have the same dimensionality
if size(predicted) ~= size(reference)
    error('Predicted and reference pattern must be of equal size!')
end

% Check dimensionality
if numel(size(predicted)) > 2
    error('Predicted and reference pattern must be two-dimensional')
end

% Check if input is logical
if isBinary(predicted) && isBinary(reference)
    % Map [0, 1] to [-1, 1]
    predicted = predicted * 2 - 1;
    reference = reference * 2 - 1;
end


%% ROC STATISTICS
% 
% 
% TP: Correctly classified target-dominated units
TP = ( (predicted ==  1) + (reference ==  1) ) == 2;
FN = ( (predicted == -1) + (reference ==  1) ) == 2;

% FP: Erroneously classified interference-dominated units
FP = ( (predicted ==  1) + (reference == -1) ) == 2;
TN = ( (predicted == -1) + (reference == -1) ) == 2;

% Average statistics
if ~isempty(dim)
    TP = sum(TP, dim);
    FN = sum(FN, dim);
    FP = sum(FP, dim);
    TN = sum(TN, dim);
else
    TP = sum(TP(:));
    FN = sum(FN(:));
    FP = sum(FP(:));
    TN = sum(TN(:));
end

POS = TP  + FN;  % Total positives 
NEG = FP  + TN;  % Total negatives
N   = POS + NEG; % Total items

tpr = TP./POS;
fpr = FP./NEG;   % False positive rate (type I error) 
fnr = FN./POS;   % False negative rate (type II error)
tnr = TN./NEG; 

% Compute hit-false alarm rate, accuracy and balanced accuracy
hitfa = tpr - fpr; 
acc   = (TP + TN) ./ N; 
bacc  = 0.5 * ((TP / POS) + (TN / NEG));
