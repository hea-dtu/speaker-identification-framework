function rootDir = getRoot(select)
%getRoot   Return user-specific root directory
%   Different directories can be specified for speech, noise, BRIR and
%   feature space files.  
%   
%USAGE
%      rootDir = getRoot(select)
%
%INPUT ARGUMENTS
%       select : string specifying file type 
%                'speech' - speech files
%                'noise'  - noise files
%                'brir'   - binaural room impulse responses
%                'fspace' - feature space
%
%OUTPUT ARGUMENTS
%      rootDir : user-specific root diretory


%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2014/07/04
%   v.0.2   2014/07/11 added root directory for feature space
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS 
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 1
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Check if select is empty
if isempty(select)
    error('Select either ''speech'', ''noise'', ''brir'' or ''fspace''.')
end


%% SELECT USER-SPECIFIC ROOT DIRECTORIES
% 
% 
% Select user-dependent root directory for the audio files
if ispc
    switch(upper(getUser))
        case 'ELEK-D0254'
            root.speech = 'M:\PhD\Matlab\Databases\Audio';
            root.noise  = 'M:\PhD\Matlab\Databases\Audio';
            root.brir   = 'M:\PhD\Matlab\Databases\BRIRs\SURREY';
        case 'ELEK-D0170'
            root.speech = 'M:\Research\Audio';
            root.noise  = 'M:\Research\Audio';
            root.brir   = 'M:\Research\BRIRs\SURREY';
            root.fspace = 'M:\Research\Matlab\Projects';
        case 'DEMHOBELSEINPC'
            root.speech = 'E:\Hobel\Research\Audio';
            root.noise  = 'E:\Hobel\Research\Audio';
            root.brir   = 'E:\Hobel\Dropbox\AMSLearning\BRIR';
            root.fspace = 'E:\Hobel\Research\Matlab\Projects';
        otherwise
            error(['Root directories are not specified for ''%s'' ! Edit ',...
                'the root entries in ''%s.m''.'],upper(getUser),mfilename)
    end
else
    homeDir     = getenv('HOME');
    root.speech = [homeDir,filesep,'Documents',filesep,'MATLAB',filesep,'Audio'];
    root.noise  = [homeDir,filesep,'Documents',filesep,'MATLAB',filesep,'Audio'];
    root.brir   = [homeDir,filesep,'Documents',filesep,'MATLAB',filesep,'Audio'];
    root.fspace = [homeDir,filesep,'Documents',filesep,'MATLAB',filesep,'Projects'];
end


%% GET USER-SPECIFIC ROOT DIRECTORIES
% 
%
% Select output
switch(lower(select))
    case {'speech' 'noise' 'brir' 'fspace'}
        rootDir = root.(lower(select));
    otherwise
        error('Requested root directory ''%s'' is not supported!',select)
end
