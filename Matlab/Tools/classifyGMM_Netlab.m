function prob = classifyGMM_Netlab(mix,featSpace,prior,mask,lowerBound,bScale,erfSwitch)


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 7
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Number of classes
nClasses = length(mix);

if nargin < 3 || isempty(prior); prior = ones(nClasses,1)/nClasses; end
if nargin < 4 || isempty(mask); 
   bMissingData = false; 
else
   bMissingData = true;
end 
if nargin < 5 || isempty(lowerBound); lowerBound = 0;    end
if nargin < 6 || isempty(bScale);     bScale     = true; end
if nargin < 7 || isempty(erfSwitch);  erfSwitch  = 2;    end


%% PERFORM CLASSIFICATION
% 
% 
% Allocate memory
prob = zeros(size(featSpace,1),nClasses);

if bMissingData
    % Loop over number of classes
    for jj = 1 : nClasses
        % Missing data recognition 
        prob(:,jj) = prior(jj) * marginalize(featSpace,mix(jj),mask,...
            lowerBound,bScale,erfSwitch);
    end
else
    % Loop over number of classes
    for jj = 1 : nClasses
        % Conventional recognition using the complete feature space
        prob(:,jj) = prior(jj) * gmmprob(mix(jj),featSpace);
    end
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************