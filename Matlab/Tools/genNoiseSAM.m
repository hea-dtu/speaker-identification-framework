function noise = genNoiseSAM(noiseType,fsHz,nSamples,varargin)
%genNoiseSAM   Create sinusoidal amplitude-modulated Gaussian noise.
%   
%USAGE
%       noise = genNoiseSAM(noiseType,fsHz,nSamples)
%
%INPUT ARGUMENTS
%   noiseType : string defining noise type and amplitude modulation 
%               '<type>_<freq>_<depth>'
%                <type>  = sam, this cannot be changed
%                <freq>  = modulation frequency in Hertz
%                <depth> = modulation depth (default depth = 0.5)
% 
%                'sam_8_1' produces 8-Hz modulated noise with a depth of 1
% 
%        fsHz : sampling frequency in Hz
%    nSamples : length of noise signal in samples
% 
%OUTPUT ARGUMENTS
%       noise : noise signal [nSamples x 1]
% 
%   genNoiseSAM(...) plots the modulation transfer function (MTF) of the
%   noise in a new figure.   
% 
%   See also genNoisePSAM.
% 
%EXAMPLE
%   % Create 10 seconds of 4-Hz amplitude-modulated Gaussian noise
%   genNoiseSAM('sam_4',16E3,160E3);

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/11/07
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 3 
    help(mfilename);
    error('Wrong number of input arguments!')
end


%% EXTRACT AMPLITUDE MODULATION PARAMETERS
% 
% 
% Detect underscore 
idxUscore = strfind(noiseType,'_');

% Figure out modulation frequency and modulation depth values
switch(numel(idxUscore))
    case 1
        mDepth = 0.5;
        fMod   = str2double(noiseType(idxUscore(1)+1:end));
    case 2        
        fMod   = str2double(noiseType(idxUscore(1)+1:idxUscore(2)-1));
        mDepth = str2double(noiseType(idxUscore(2)+1:end));
    otherwise
        error('Wrong usage of noise type ''<type>_<freq>_<depth>''')
end

% Check for valid parameters
if ~isfinite(fMod) || ~isfinite(mDepth);
    error('Wrong usage of noise type ''<type>_<freq>_<depth>''')
end
     

%% CREATE WHITE GAUSSIAN NOISE
% 
% 
% Create white Gaussian noise
noise = randn(nSamples,1);


%% IMPRINT MODULATION
% 
% 
% Modulation signal
modSignal = 1 + mDepth * sin(2*pi*(0:nSamples-1)' * fMod/fsHz);

% Apply amplitude modulations
noise = bsxfun(@times,noise,modSignal);


%% SHOW MTF OF NOISE
% 
% 
% Visualize the modulation transfer function (MTF) of the noise
if nargout == 0
    % Create modulation center frequencies
    cfHz = createFreqAxisLog(0.5,2*pow2(nextpow2(fMod)));
    
    % Compute MTF
    calcMTF(noise,fsHz,cfHz);
    title(['SAM noise modulated at ',num2str(fMod),...
           ' Hz with a depth of ',num2str(mDepth)]);
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************