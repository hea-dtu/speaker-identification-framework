function R = SIDBatch(strEXP,bParfor,bSaveSimulation,bPlotSimulation)
%SIDBatch   Framework for large-scale speaker identification experiments


%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/20
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS 
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end
      
% Set default values
if nargin < 2 || isempty(bParfor);         bParfor         = true; end
if nargin < 3 || isempty(bSaveSimulation); bSaveSimulation = true; end
if nargin < 4 || isempty(bPlotSimulation); bPlotSimulation = true; end


%% INITIALIZE PARALLEL PROCESSING
% 
% 
% Detect current parallel pool
if exist('gcp','file')
    p = gcp('nocreate');
else
    p = [];
end

% Create parallel pool on default cluster
if bParfor 
    if isempty(p)    
        try
            hPP = parpool;
        catch
            % Couldn't initialize pool, deactivate parallel processing
            bParfor = false;
        end
    else
       hPP = p; 
    end
else
    % Close parallel pool
    if ~isempty(p)    
        delete(p);
    end
end


%% INSTALL SUBROUTINES
% 
% 
% Detect SID root directory
rootSID = [fileparts(which('SIDBatch.m')),filesep,'..',filesep];

% Add paths (Important, this needs to done after the parpool command)
addpath([rootSID,'SID'])
addpath([rootSID,'EXP'])
addpath([rootSID,'Features'])
addpath([rootSID,'MissingData'])
addpath([rootSID,'Netlab'])
addpath([rootSID,'Tools'])
addpath([rootSID,'Voicebox'])


%% SID MAIN LOOP
%
%
% Convert character array to cell
if ischar(strEXP)
    strEXP = {strEXP};
end

% Number of experiments
nExperiments = numel(strEXP);

% Loop over the number of experiments
for ii = 1 : nExperiments
    
    % Print warning if multiple simulations are neither saved nor plotted
    if ii == 1 && nExperiments > 1 && ~bSaveSimulation
        warning('Simulations will not be saved, press any key to continue!')
        pause;
    end
    
    % Create experiment and SID presets
    [EXP,P] = SIDSetup(strEXP{ii});
    
    % Control seed of random generator (to guarantee reproducibility)
    try %#ok
        if EXP.bShuffleRNG
            rng('shuffle');
        else
            rng('default');
        end
    end

    % Loop over number of experiment iterations
    for jj = 1 : EXP.nIterEXP

        % Report progress
        fprintf([repmat('=',[1 80]),'\n']);
        fprintf('     Experiment (%i/%i) ''%s'' (Iteration: %i/%i) \n',...
            ii,nExperiments,strEXP{ii},jj,EXP.nIterEXP);
        fprintf([repmat('=',[1 80]),'\n']);
        
        % Run one iteration of the SID experiment
        EXP = SIDMain(EXP,P);

        % Create result structure
        if jj == 1
            % Remove iteration-dependent fields 
            R = rmfield(EXP,{'speakerIDs' 'timeSec' 'memory' 'nWorkers' 'recRate' 'confMat'});
            
            % Add presets
            R.P = P;
            
            % Add fields 
            R.speakerIDs = cell(EXP.nIterEXP,1);
            R.timeSec    = [];
            R.memory     = [];
            R.nWorkers   = [];
            R.recRate    = [];
            R.recRateDim = 'nSNRs x nNoiseTypes x nPresets x nIterEXP';
            R.confMat    = [];
            R.confMatDim = 'nSpeakers x nSpeakers x nSNRs x nNoiseTypes x nPresets x nIterEXP';
            
            % Check if ROC statistics should be stored
            if isfield(R,'rocStats')
                R = rmfield(R,'rocStats');
                
                % Add fields
                R.rocStats = [];
                R.rocStatsDim = 'nSNRs x nNoiseTypes x nPresets x nIterEXP x [h-fa h cr fa m density acc bacc]';
                
                bStoreROC = true;
            else
                bStoreROC = false;
            end
        end
        
        % Copy results from current iteration
        R.speakerIDs{jj} = EXP.speakerIDs;
        R.timeSec  = cat(1,R.timeSec,EXP.timeSec);
        R.memory   = cat(1,R.memory,EXP.memory);
        R.nWorkers = cat(1,R.nWorkers,EXP.nWorkers);
        R.recRate  = cat(4,R.recRate,EXP.recRate);
        R.confMat  = cat(6,R.confMat,EXP.confMat);
        if bStoreROC
            R.rocStats = cat(4,R.rocStats,EXP.rocStats);
        end
    end
        
    % Save simulation
    if bSaveSimulation
        % Get unique date string
        strTime = datestr(now,'yyyymmdd_HHMM_SS');
        
        % Root directory
        fSpaceRoot = [getRoot('fspace'),filesep,'SID',filesep];
        
        % Create folder if it doesn't exist
        if ~isdir(fSpaceRoot)
            mkdir(fSpaceRoot);
        end
        
        % Save simulation struct
        save([fSpaceRoot,'SID_',strEXP{ii},'_',strTime,'.mat'],'R','-v7.3');
    end
    
    % Plot results
    if bPlotSimulation
        % Visualize SID performance
        SIDPlot(R);
        
        % Ensure that figures will appear on the screen
        pause(1);
    end
end


%% SHUT-DOWN PARALLEL PROCESSING
% 
% 
% Close parallel pool
if bParfor
    delete(hPP);
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************