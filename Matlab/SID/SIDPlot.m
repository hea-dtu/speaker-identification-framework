function h = SIDPlot(R,idxSubset,idxNoise,bShowErrorbar)
%SIDPlot   Plotting routine for speaker identification experiments


%% CHECK INPUT ARGUMENTS 
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 4
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(idxSubset); idxSubset = 1:numel(R.strPresets); end
if nargin < 3 || isempty(idxNoise); idxNoise = 1:numel(R.noiseTypes); end
if nargin < 4 || isempty(bShowErrorbar); bShowErrorbar = false; end


%% DETERMINE SID PERFORMANCE
% 
% 
% Average performance and standard error across noise types and iterations
dataM  = mean(mean(R.recRate(:,idxNoise,idxSubset,:),2),4);
dataSE = std(mean(R.recRate(:,idxNoise,idxSubset,:),4),[],2)/sqrt(R.nIterEXP);


%% PLOT SID RESULTS
% 
% 
nSNRs = size(R.recRate,1);

markerSize = 12;
lineWidth  = 1;

% Determine color and markers
[strMarker,strCol] = determineMarkers(R.strPresets(idxSubset));

% Plot identification performance as a function of the SNR
figure;
if bShowErrorbar
    h = errorbar(squeeze(dataM),squeeze(dataSE));
else
    h = plot(1:nSNRs,squeeze(dataM));
end

for hh=1:numel(h)
    set(h(hh),'marker',strMarker{hh},'markersize',markerSize,...
        'markerfacecolor',strCol(hh,:),'markeredgecolor','k',...
        'color',strCol(hh,:),'linewidth',lineWidth)
end

legend(regexprep(R.strPresets(idxSubset),'_',' '),'Location','SouthWest')
set(gca,'XTick',1:nSNRs,'XTickLabel',num2str(R.snrdB(:)))
xlabel('SNR (dB)')
ylabel('Speaker identification accuracy (%)')
grid on;
xlim([1-1E-2 nSNRs+1E-2])
ylim([0 100])

% Show recognition performance averaged across SNRs and noise types
figure;
h = bar(squeeze(mean(dataM,1)),1);
grid on;
title('Average performance')
xlabel('Number of SID systems')
ylabel('Speaker identification accuracy (%)')

try
    yb = cat(1, h.YData) * 0 + 5;
    xb = bsxfun(@plus, h(1).XData, [h.XOffset]');
    hold on;
    text(xb(:),yb(:),regexprep(R.strPresets(idxSubset),'_',' '),...
        'rotation',90,'color',[0.9 0.9 0.9]);
end

