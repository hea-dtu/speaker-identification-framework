function logLikelihood = SIDClassify(P,fSpaceFBE,ibm)
%SIDClassify   Classification routine for speaker identification systems

% Short-cut
C  = P.CLASSIFIER;
MD = P.MD;
F  = P.FEATURE;

% Determine dimensionality of experiment
[nSNRs,nNoiseTypes] = size(fSpaceFBE);

% Number of different speaker identities
nClasses = numel(C.GMM);

% Allocate memory
logLikelihood = zeros(nClasses,nSNRs,nNoiseTypes);

% Restrict range of center frequencies
if isempty(F.cfHzSelect)
    cfHzIdx = 1:F.nFilter;
else
    cfHzIdx = find(F.cfHz > min(F.cfHzSelect) & F.cfHz <= max(F.cfHzSelect));
end

% Remove features located at the edges, since they are not relative
% features due to the zero-padding     
if P.FEATURE.bDecorrelate && P.FEATURE.bRemoveEdges
    cfHzIdx(cfHzIdx==1 | cfHzIdx==F.nFilter) = [];
end

% Loop over number of noise types
for ii = 1 : nNoiseTypes
    
    % Reset normalization states for each noise type
    if isfield(P(1).POST,'STATS') && isfield(P.POST.STATS,'states');
        P.POST.STATS = rmfield(P.POST.STATS,'states');
    end
    
    % Loop over all SNRs (this should be the inner for loop, because the
    % adaptive normalization strategy benefits from seeing the same noise
    % type at various SNRs). Once a new noise type is used, the internal
    % states corresponding to the adaptive normalization statistics are
    % reset, and the adaptive normalization is initialized with the 
    % statistics observed during training.    
    for kk = 1 : nSNRs

        % Get mask if direct masking or missing data is activated
        if MD.bDirectMasking || MD.bMissingData
            
            % Mask
            mask = ibm.(MD.maskEstimation){kk,ii};
            
            % Perform spectro-temporal integration
            if numel(MD.contextTF) > 1
                mask = integrateContext(mask,MD.contextTF,...
                    MD.contextShape,@mean,'replicate');
            end
            
            % Apply local threshold to binarize mask
            if MD.bUseBinaryMask
                if strcmpi(MD.maskEstimation,'ebm_voicing')
                    mask = mask < MD.maskThreshold;
                else
                    mask = mask > MD.maskThreshold;
                end
            end
        end
        
        % Perform direct masking
        if MD.bDirectMasking
            % Floor mask
            maskDM = floorMask(mask,MD.floordB);
            
            % Direct masking
            fSpaceFBE{kk,ii} = fSpaceFBE{kk,ii} .* maskDM;
        end

        % Feature extraction (return P, since it possibly contains
        % updated normalization states)
        [fSpace,P] = SIDFeatures(P,fSpaceFBE{kk,ii});
        
        % Derive frame-based probabilities
        if MD.bMissingData
            
            % Restrict frequency range
            maskMD = mask(:,cfHzIdx);
            
            % Check if we are dealing with binary or soft masks
            if not(MD.bUseBinaryMask)
                % Pass soft mask through sigmoid function
                maskMD = sigmoid(maskMD,MD.alphaSoftMask,MD.betaSoftMask);
            end
            
            % Replicate mask pattern (instead of using delta masks)
            maskMD = cat(2,maskMD,repmat(maskMD,[1 numel(P.POST.deltaHalfWSize)]));
            
            % Select active frames
            if MD.bSelectActiveFrames
                nUnits  = sum(maskMD,2);
                thres   = min(round(0.5*F.nFilter),median(nUnits(nUnits>0)));
                bDelete = nUnits < thres;
                
                % Delete frames for which the number of reliable
                % T-F units is below the "activity threshold".
                fSpace(bDelete,:) = [];
                maskMD(bDelete,:) = [];
            end
            
            % Classify
            prob = classifyGMM_Netlab(C.GMM,fSpace,[],maskMD,MD.lowerBound);
            
            % File-based integration of log-likelihoods
            logLikelihood(:,kk,ii) = sum(log(prob),1);
        else
            % Classify
            prob = classifyGMM_Netlab(C.GMM,fSpace);
            
            % File-based integration of log-likelihoods
            logLikelihood(:,kk,ii) = sum(log(prob),1);
        end
    end
end

