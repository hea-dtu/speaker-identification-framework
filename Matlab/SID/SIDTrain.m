function P = SIDTrain(P,fSpaceFBE,labels,vad)
%SIDTrain   Training routine for speaker identification experiments

% Short-cut
C = P.CLASSIFIER;


%% CLEAN PRESET
% 
% 
% Delete existing models
if isfield(P.CLASSIFIER,'UBM');     
    P.CLASSIFIER = rmfield(P.CLASSIFIER,'UBM');
end
if isfield(P.CLASSIFIER,'GMM');     
    P.CLASSIFIER = rmfield(P.CLASSIFIER,'GMM');
end
if isfield(P.POST,'STATS');     
    P.POST = rmfield(P.POST,'STATS');
end


%% CREATE TRAINING LABELS
% 
% 
% Number of sentences
nSentences = size(labels,2);

% Number of feature observations
nFrames = size(fSpaceFBE,1);

% Allocate memory
labelsTrain = zeros(nFrames,1);

% Loop over the number of sentences
for ss = 1 : nSentences;     
    labelsTrain(labels(1,ss):labels(2,ss)) = labels(3,ss);
end

    
%% FEATURE EXTRACTION
% 
% 
% File-based processing
if P.POST.bFileBased

    % File-based integration
    for ss = 1 : nSentences
        % Transform filterbank energy features to final feature space
        if ss == 1
            % We do not know the feature space dimensionality 
            [fSpace,P] = SIDFeatures(P,fSpaceFBE(labels(1,ss):labels(2,ss),:));
            
            % Allocate memory
            fSpace(nFrames,:) = 0;
        else
            [fSpace(labels(1,ss):labels(2,ss),:),P] = SIDFeatures(P,fSpaceFBE(labels(1,ss):labels(2,ss),:));
        end
    end
    
else
    % Transform filterbank energy features to final feature space
    [fSpace,P] = SIDFeatures(P,fSpaceFBE);
end


%% VOICE ACTIVITY DETECTION
% 
% 
% Only consider speech-dominated frames during training
if C.bUseVAD
    fSpace = fSpace(vad==true,:);
    labelsTrain = labelsTrain(vad==true);
end


%% TRAIN GMM CLASSIFIER
% 
% 
% Train GMM classifier
if C.bUBM
    % Train a universal background model using all features
    P.CLASSIFIER.UBM = trainGMM(fSpace,ones(size(fSpace,1),1),C.method,...
        C.nGMMs,C.nIterEM,C.thresEM,C.cvType);
    
    % Adapt speaker-specific model from UBM
    P.CLASSIFIER.GMM = adaptGMM(fSpace,labelsTrain,P.CLASSIFIER.UBM,...
        C.nIterUBM);
else
    % Train speaker-specific GMM classifier
    P.CLASSIFIER.GMM = trainGMM(fSpace,labelsTrain,C.method,C.nGMMs,...
        C.nIterEM,C.thresEM,C.cvType);
end
