function EXP = SIDMain(EXP,P)
%SIDMain   Main routine for speaker identification experiments

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/20
%   ***********************************************************************


%% DETECT PARALLEL POOL
% 
% 
% Get current parallel pool without creating a new one
if exist('gcp','file')
    objPP = gcp('nocreate');
else
    objPP = [];
end

% Derive the number of workers available
if ~isempty(objPP)
    nWorkers = objPP.NumWorkers;
else
    nWorkers = 1;
end

% Report status
fprintf('     Number of workers: %.2i\n',nWorkers);
fprintf([repmat('-',[1 31]),'\n']);


%% DETECT REQUIRED SEGREGATION SYSTEMS
% 
%    
% Empty cell specifying required mask estimation methods
maskType = {};

% Loop over all SID systems
for ii = 1 : numel(P)
    % Detect if IBM estimation system is required
    if P(ii).MD.bMissingData || P(ii).MD.bDirectMasking
        maskType = cat(2,maskType,P(ii).MD.maskEstimation);
    end
end

% Delete multiple instances of IBM estimators
maskType = unique(maskType);


%% COPY FEATURE PARAMETERS
% 
% 
% These are constant across all SID systems
bRemoveDC      = P(1).PRE.bRemoveDC;
bPreEmphasis   = P(1).PRE.bPreEmphasis;
blockSec       = P(1).FEATURE.blockSec;
stepSec        = P(1).FEATURE.stepSec;
nFilter        = P(1).FEATURE.nFilter;
fRangeHz       = P(1).FEATURE.fRangeHz;
vadThresdB     = P(1).CLASSIFIER.vadThresdB;
vadHangoverSec = P(1).CLASSIFIER.vadHangoverSec;
fsHzRef        = EXP.fsHz;
    

%% DIMENSIONALITY OF SID EXPERIMENT
% 
%     
if isempty(EXP.nSpeakers);  EXP.nSpeakers  = inf; end
if isempty(EXP.nSentences); EXP.nSentences = inf; end

% Detect all folders 
strFolders = listDirs(EXP.audioDatabase,0);

% Detect all sub-folders
strSubFolders = listDirs(EXP.audioDatabase);

% Store sub-folder names
subFolderNames = {strSubFolders.name};

% Check if folder names are identical
if isempty(setdiff(subFolderNames,{strFolders.name}))
    % If they are, then wave files are located within these folders. Thus,
    % we don't change anything!
else
    % Remove folder names, because wave files are assumed to be located in
    % the sub-folders.
    for ll = 1 : numel(strFolders);
        subFolderNames(strcmp(subFolderNames,strFolders(ll).name)) = [];
    end
end

% Total number of audio folders
nClassesTotal = numel(subFolderNames);

% Restricted number of folders
nClasses = min(EXP.nSpeakers,nClassesTotal);

% Randomize folder indices
idxSpeakers = rnsubset(nClasses,nClassesTotal);
    
% Number of SNRs
nSNRs = numel(EXP.snrdB);

% Number of noise types
nNoiseTypes = numel(EXP.noiseTypes);


%% SPLIT SIGNALS INTO TRAINING AND TESTING SETS
% 
% 
% Allocate memory
audioTrain = [];
audioTest  = [];
audioLabelsTrain = [];
audioLabelsTest  = [];

% Loop over number of folders
for ii = 1 : nClasses
    
    % Get all files
    allfiles = listFiles(subFolderNames{idxSpeakers(ii)},...
        ['*.',upper(EXP.audioFileFormat)]);
    
    % listFiles is case-sensitive, look for lower case if allfiles is empty
    if isempty(allfiles)
        allfiles = listFiles(subFolderNames{idxSpeakers(ii)},...
            ['*.',lower(EXP.audioFileFormat)]);
    end
    
    % Create cell array of file names
    audioList = {allfiles.name}';
    
    % Detect number of available sentences
    nSentencesTotal = numel(audioList);
    
    % Restrict number of sentences used
    nSentences = min(EXP.nSentences,nSentencesTotal);
    
    % Randomize file indices
    idxSentences = rnsubset(nSentences,nSentencesTotal);
    
    % Loop over the number of audio file patterns which should be part of 
    % the training
    for jj = 1 : numel(EXP.audioFilePatternsForTraining)
        
        % Detect files which contain the jj-th pattern
        idxTrain = strfind(audioList,EXP.audioFilePatternsForTraining{jj});
        for ll = 1:numel(idxTrain)
            if ~isempty(idxTrain{ll})
                % Delete the sentence index and put it in front
                idxSentences(idxSentences==ll) = [];
                idxSentences = cat(2,ll,idxSentences);
            end
        end
    end
        
    % Number of sentences for training and testing per SNR condition
    nSentencesTrain = round(EXP.trainVStestPerc*nSentences);
    nSentencesTest  = nSentences - nSentencesTrain;
    
    % List of audio files used for training and testing
    audioTrain = cat(1,audioTrain,audioList(idxSentences(1:nSentencesTrain)));
    audioTest  = cat(1,audioTest,audioList(idxSentences(nSentencesTrain+1:end)));
    
    % Corresponding class labels for training and testing
    audioLabelsTrain = cat(1,audioLabelsTrain,ones(nSentencesTrain,1) * ii);
    audioLabelsTest  = cat(1,audioLabelsTest,ones(nSentencesTest,1) * ii);
end


%% SID FEATURE EXTRACTION (TRAINING)
% 
% 
% Measure processing time
tic;

% Number of sentence material
nSentencesTrain = numel(audioTrain);

% Allocate memory
fSpaceTrain = cell(nSentencesTrain,1);
labelsTrain = zeros(3,nSentencesTrain);
vadTrain    = cell(nSentencesTrain,1);

% Check if parallel processing is supported
if nWorkers > 1
    
    % Parfor loop over the number fo training sentences
    parfor ii = 1:nSentencesTrain
        
        % Load audio file
        [speech,fs] = readAudio(audioTrain{ii});
        
        % Resampling, if necessary
        speech = resample(speech,fsHzRef,fs);
        
        % Compute filterbank energy features
        feat = calcFBE(speech,fsHzRef,blockSec,stepSec,fRangeHz,nFilter,...
            bRemoveDC,bPreEmphasis);
        
        % Perform voice activity detection (VAD)
        bUseFrames = detectVoiceActivityKinnunen(speech,fsHzRef,...
            vadThresdB,'frames',vadHangoverSec,blockSec,stepSec);
        
        % Accumulate labels, feature space and VAD
        labelsTrain(:,ii) = [1; size(feat,2); audioLabelsTrain(ii)];
        fSpaceTrain{ii}   = transpose(feat);
        vadTrain{ii}      = bUseFrames;
        
        % Report progress
        fprintf('SID: Feature extraction %.2f %%\n',100*(ii/nSentencesTrain));
    end
else
    % Loop over the number fo training sentences
    for ii = 1:nSentencesTrain
        
        % Load audio file
        [speech,fs] = readAudio(audioTrain{ii});
        
        % Resampling, if necessary
        speech = resample(speech,fsHzRef,fs);
        
        % Compute filterbank energy features
        feat = calcFBE(speech,fsHzRef,blockSec,stepSec,fRangeHz,nFilter,...
            bRemoveDC,bPreEmphasis);
        
        % Perform voice activity detection (VAD)
        bUseFrames = detectVoiceActivityKinnunen(speech,fsHzRef,...
            vadThresdB,'frames',vadHangoverSec,blockSec,stepSec);
        
        % Accumulate labels, feature space and VAD
        labelsTrain(:,ii) = [1; size(feat,2); audioLabelsTrain(ii)];
        fSpaceTrain{ii}   = transpose(feat);
        vadTrain{ii}      = bUseFrames;
        
        % Report progress
        fprintf('SID: Feature extraction %.2f %%\n',100*(ii/nSentencesTrain));
    end
end

% Re-organize feature space, vad decisions and labels
fSpaceTrain = cell2mat(fSpaceTrain);
vadTrain    = cell2mat(vadTrain);
labelsTrain(1,2:end) = cumsum(labelsTrain(1,2:end) + labelsTrain(2,1:end-1)-1)+1;
labelsTrain(2,2:end) = labelsTrain(1,2:end) + labelsTrain(2,2:end) - 1;

% Stop time
timeSec.features = toc;
    

%% SID TRAINING
% 
% 
% Measure processing time
tic;

% Find unique SID presets
[idxUnique,idxPresets] = findUniqueSIDPresets(P);

% Number of unique SID presets
nUniquePresets = numel(idxUnique);

% Check if parallel processing is supported
if nWorkers > 1
    
    % Ensure that parfor loops of a list of increasing consecutive integers
    PP = P(idxUnique);
    
    % Parloop over number of SID presets
    parfor ii = 1:nUniquePresets
        
        % Train SID system
        PP(ii) = SIDTrain(PP(ii),fSpaceTrain,labelsTrain,vadTrain);
        
        % Report progress
        fprintf('SID: Training %.2f %%\n',100*(ii/nUniquePresets));
    end
    
    % Copy trained models in PP back to P
    [P(idxUnique)] = PP; clear PP;
else
    % Loop over number of SID presets
    for ii = 1:nUniquePresets
        
        % Train SID system
        P(idxUnique(ii)) = SIDTrain(P(idxUnique(ii)),fSpaceTrain,labelsTrain,vadTrain);
        
        % Report progress
        fprintf('SID: Training %.2f %%\n',100*(ii/nUniquePresets));
    end
end

% Duplicate trained SID models
for ii = 1:nUniquePresets
    if sum(idxPresets == ii) > 1
        % Duplicate classifier structure 
        [P(idxPresets == ii).CLASSIFIER] = deal([P(idxUnique(ii)).CLASSIFIER]);
        % Duplicate POST structure since it may contain normalization
        % constants derived from the training data
        [P(idxPresets == ii).POST] = deal([P(idxUnique(ii)).POST]);
    end
end

% Stop time for training 
timeSec.training = toc;

% Free some memory
clear fSpaceTrain labelsTrain vadTrain;


%% SID CLASSIFICATION AND EVALUATION
% 
% 
% Measure processing time
tic;

% Number of presets
nPresets = numel(P);

% Number of test sentences
nSentencesTest = numel(audioTest);
    
% Allocate memory
fuseIdx = EXP.fusion;
nFusion = numel(fuseIdx);
recRate = zeros(nSNRs,nNoiseTypes,nPresets+nFusion);
confMat = zeros(nClasses,nClasses,nSNRs,nNoiseTypes,nPresets+nFusion);
rocStat = zeros(nSNRs,nNoiseTypes,nPresets,1,8);

% Table containing all conditions to be tested
conditions = cartesianProduct({1:nSNRs,1:nNoiseTypes});
nCondTest  = size(conditions,1);

% Conditions
idxSNR   = conditions(:,1);
idxNoise = conditions(:,2);
snrdBVec = EXP.snrdB(idxSNR);
noiseVec = EXP.noiseTypes(idxNoise);

if nWorkers > 1
    
    % Loop over the number of sentences
    parfor hh = 1 : 1:nSentencesTest
        
        % Copy file name and the corresponding label
        fNameAudio = audioTest{hh};
        labelsTest = audioLabelsTest(hh);
        
        % Allocate memory for each sentence
        fSpaceTest = cell(nSNRs,nNoiseTypes);
        ibmTest    = [];
        
        for mm = 1 : numel(maskType)
            ibmTest.(maskType{mm}) = repmat({[]},[nSNRs,nNoiseTypes]);
        end
        
        % Parfor loop over the number of test conditions
        for ii = 1:nCondTest
            
            % Create noisy speech
            [mix,speech,noise] = genNoisySpeech(fNameAudio,fsHzRef,...
                noiseVec{ii},snrdBVec(ii),false);
            
            % Compute filterbank energy (FBE) features
            feat = calcFBE(mix,fsHzRef,blockSec,stepSec,fRangeHz,...
                nFilter,bRemoveDC,bPreEmphasis);
            
            % Store FBE features
            fSpaceTest{idxSNR(ii),idxNoise(ii)} = transpose(feat);
            
            % Segregation stage
            for mm = 1 : numel(maskType)
                switch(lower(maskType{mm}))
                    case 'ibm'
                        % Compute ideal binary mask
                        IBM = calcIBM_Triang(speech,noise,fsHzRef,...
                            blockSec,stepSec,fRangeHz,nFilter);
                        % Replace IBM with a priori SNR to allow different
                        % thresholds during recognition
                        mask = IBM.aPrioriSNR;
                    case 'irm'
                        % Compute ideal ratio mask
                        IBM = calcIRM_Triang(speech,noise,fsHzRef,...
                            blockSec,stepSec,fRangeHz,nFilter);
                        % Replace IBM with IRM
                        mask = IBM.irm;
                    case 'ebm_spp'
                        % Estimate binary mask via SPP
                        IBM = estIBM_SPP(mix,fsHzRef,blockSec,stepSec,...
                            fRangeHz,nFilter,[1 1]);
                        % Replace IBM with SPP to allow different
                        % thresholds during recognition
                        mask = IBM.audSPP;
                    case 'ebm_voicing'
                        % Estimate binary mask via voicing distance
                        IBM = estIBM_Voicing(mix,fsHzRef,blockSec,...
                            stepSec,fRangeHz,nFilter);
                        % Replace IBM with voiceDist to allow
                        % different thresholds during recognition.
                        mask = IBM.voiceDist;
                    otherwise
                        error('Grouping stage is not supported!')
                end
                
                % Accumulate mask pattern
                ibmTest.(maskType{mm}){idxSNR(ii),idxNoise(ii)} = mask.';
            end
        end
        
        % Allocate memory
        probSeg = zeros(nClasses,nSNRs,nNoiseTypes,nPresets);
        
        % Parloop over number of SID presets
        for pp = 1:nPresets
            probSeg(:,:,:,pp) = SIDClassify(P(pp),fSpaceTest,ibmTest);
        end
        
        % Evaluate SID performance
        [currRecRate,currConf] = SIDEvaluate(probSeg,labelsTest,fuseIdx);
        
        % If possible, evaluate ROC statistics
        currRStats = SIDROC(P,ibmTest);
        
        % Accumulate results
        recRate = recRate + currRecRate;
        confMat = confMat + currConf;
        if ~isempty(currRStats)
            rocStat = rocStat + currRStats;
        end
        
        % Report progress
        fprintf('SID: Classification %.2f %%\n',100*(hh/nSentencesTest));
    end
else
    
    % Loop over the number of sentences
    for hh = 1 : 1:nSentencesTest
        
        % Copy file name and the corresponding label
        fNameAudio = audioTest{hh};
        labelsTest = audioLabelsTest(hh);
        
        % Allocate memory for each sentence
        fSpaceTest = cell(nSNRs,nNoiseTypes);
        ibmTest    = [];
        
        for mm = 1 : numel(maskType)
            ibmTest.(maskType{mm}) = repmat({[]},[nSNRs,nNoiseTypes]);
        end
        
        % Parfor loop over the number of test conditions
        for ii = 1:nCondTest
            
            % Create noisy speech
            [mix,speech,noise] = genNoisySpeech(fNameAudio,fsHzRef,...
                noiseVec{ii},snrdBVec(ii),false);
            
            % Compute filterbank energy (FBE) features
            feat = calcFBE(mix,fsHzRef,blockSec,stepSec,fRangeHz,...
                nFilter,bRemoveDC,bPreEmphasis);
            
            % Store FBE features
            fSpaceTest{idxSNR(ii),idxNoise(ii)} = transpose(feat);
            
            % Segregation stage
            for mm = 1 : numel(maskType)
                switch(lower(maskType{mm}))
                    case 'ibm'
                        % Compute ideal binary mask
                        IBM = calcIBM_Triang(speech,noise,fsHzRef,...
                            blockSec,stepSec,fRangeHz,nFilter);
                        % Replace IBM with a priori SNR to allow different
                        % thresholds during recognition
                        mask = IBM.aPrioriSNR;
                    case 'irm'
                        % Compute ideal ratio mask
                        IBM = calcIRM_Triang(speech,noise,fsHzRef,...
                            blockSec,stepSec,fRangeHz,nFilter);
                        % Replace IBM with IRM
                        mask = IBM.irm;
                    case 'ebm_spp'
                        % Estimate binary mask via SPP
                        IBM = estIBM_SPP(mix,fsHzRef,blockSec,stepSec,...
                            fRangeHz,nFilter,[1 1]);
                        % Replace IBM with SPP to allow different
                        % thresholds during recognition
                        mask = IBM.audSPP;
                    case 'ebm_voicing'
                        % Estimate binary mask via voicing distance
                        IBM = estIBM_Voicing(mix,fsHzRef,blockSec,...
                            stepSec,fRangeHz,nFilter);
                        % Replace IBM with voiceDist to allow
                        % different thresholds during recognition.
                        mask = IBM.voiceDist;
                    otherwise
                        error('Grouping stage is not supported!')
                end
                
                % Accumulate mask pattern
                ibmTest.(maskType{mm}){idxSNR(ii),idxNoise(ii)} = mask.';
            end
        end
        
        % Allocate memory
        probSeg = zeros(nClasses,nSNRs,nNoiseTypes,nPresets);
        
        % Loop over number of SID presets
        for pp = 1:nPresets
            probSeg(:,:,:,pp) = SIDClassify(P(pp),fSpaceTest,ibmTest);
        end
        
        % Evaluate SID performance
        [currRecRate,currConf] = SIDEvaluate(probSeg,labelsTest,fuseIdx);
        
        % If possible, evaluate ROC statistics
        currRStats = SIDROC(P,ibmTest);
        
        % Accumulate results
        recRate = recRate + currRecRate;
        confMat = confMat + currConf;
        if ~isempty(currRStats)
            rocStat = rocStat + currRStats;
        end
        
        % Report progress
        fprintf('SID: Classification %.2f %%\n',100*(hh/nSentencesTest));
    end
end

% Normalize
recRate = recRate / nSentencesTest;
confMat = confMat / (nSentencesTest / nClasses);
rocStat = rocStat / nSentencesTest;

% Stop time for classification
timeSec.classification = toc;

% Copy preset names
strPresets = {P(:).preset};

% Create preset names for fusion-based SID systems
if ~isempty(EXP.fusion)
    for ii = 1:numel(EXP.fusion)
        % Extend preset names
        strPresets{nPresets+ii} = cell2mat(cellfun(@(a,b)[a,b],...
            strPresets(EXP.fusion{ii}),[repmat({' + '}        ,...
            [1 numel(EXP.fusion{ii})-1]) {''}],'uni',false));
    end
end

% Report memory usage
if ispc
    infoM = memory;
else
    [~,infoM] = unix('free');
end

% Complete experimental struct
EXP.strPresets      = regexprep(strPresets,'_',' ');
EXP.speakerIDs      = subFolderNames(idxSpeakers);
EXP.nSpeakers       = nClasses;
EXP.nSentences      = nSentences;
EXP.nSentencesTrain = nSentencesTrain;
EXP.nSentencesTest  = nSentencesTest;
EXP.timeSec         = timeSec;
EXP.memory          = infoM;
EXP.nWorkers        = nWorkers;
EXP.recRate         = recRate;
EXP.confMat         = confMat;

% Add ROC statistics
if ~isempty(rocStat)
    EXP.rocStats = rocStat;
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************