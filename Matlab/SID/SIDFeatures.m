function [fSpace,P] = SIDFeatures(P,fSpace)
%SIDFeatures   Feature extraction based on filterbank energy features

% Short-cut
F = P.FEATURE;


%% COMPRESS FILTERBANK ENERGY FEATURES
% 
% 
% Apply compression
fSpace = compress(fSpace,F.compress);
        

%% RASTA FILTERING
% 
% 
if F.bRASTA
    % Emphasize speech-specific temporal modulations
    fSpace = filterRASTA(fSpace,F.stepSec,160E-3,1);
end


%% DECORRELATE FILTERBANK ENERGY FEATURES
% 
% 
if F.bDecorrelate
    % Apply filter across frequency to decorrelate FBE features
    fSpace = filterFBE(fSpace,2,'central');
end
            

%% SELECT FREQUENCY RANGE
% 
% 
% Restrict range of center frequencies
if isempty(F.cfHzSelect)
    cfHzIdx = 1:F.nFilter;
else
    cfHzIdx = find(F.cfHz > min(F.cfHzSelect) & F.cfHz <= max(F.cfHzSelect));
end

% Remove features located at the edges, since they are not relative
% features due to the zero-padding 
if F.bDecorrelate && F.bRemoveEdges
    cfHzIdx(cfHzIdx == 1 | cfHzIdx == F.nFilter) = [];
end

% Select center frequencies
fSpace = fSpace(:,cfHzIdx);

    
%% TRANSFORM COMPRESSED FILTERBANK ENERGIES TO CEPSTRAL FEATURES
% 
%
% Apply DCT
if F.bDCT
    % Apply DCT
    fSpace = applyDCT(fSpace',F.nMFCCoeffs,F.typeDCT,F.bRemove1st)';
    
    % Perform liftering, which aims at equalizing the feature range of the
    % resulting MFC coefficients
    fSpace = applyLiftering(fSpace',22)';
end


%% EXTRACT DELTA FEATURES TO INCORPORATE TEMPORAL CONTEXT
% 
% 
% Calculate delta features
if ~isempty(P.POST.deltaHalfWSize)
    fSpace = cat(2,fSpace,calcDeltaHTK(fSpace,P.POST.deltaHalfWSize,1));
end


%% POST-PROCESSING
% 
% 
% Convert feature space into a "probability mass function"
if P.POST.bNormPMF
    % L1 norm
    normL1 = sum(fSpace,2);
    normL1(normL1==0) = 1; % Prevent division by zero;
    
    fSpace = bsxfun(@rdivide,fSpace,normL1);
end


% Normalize feature space
if P.POST.bNormFeatures
    if isfield(P.POST,'bFileBased') && P.POST.bFileBased;
        % File-based processing, do not create a STATS structure
        fSpace = normalizeData(fSpace,P.POST.normType,1);
    else
        if isfield(P.POST,'STATS');
            % Scale features based on statistics observed during training
            if P.POST.bAdaptive && P.POST.tauSec > 0
                % Adaptive normalization
                [fSpace,P.POST.STATS] = scaleDataAdaptive(fSpace,...
                    P.POST.STATS,P.FEATURE.stepSec,P.POST.tauSec);
            else
                % Use static statistics obtained during training
                fSpace = scaleData(fSpace,P.POST.STATS);
            end
        else
            % Normalize feature space and store corresponding statistics
            [fSpace,P.POST.STATS] = normalizeData(fSpace,P.POST.normType,1);
        end
    end
end

% Perform ARMA filtering
if P.POST.orderARMA > 0
    fSpace = filterARMA(fSpace,P.POST.orderARMA);
end
