function rocStats = SIDROC(P,ibm)
%SIDROC   Evaluate hit minus false alarm rate


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 2
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Check if IBM is available
if ~isfield(ibm,'ibm')
    rocStats = [];
    return;
end

% Determine dimensionality of experiment
[nSNRs,nNoiseTypes] = size(ibm.ibm);
nMethods            = numel(P);

% Allocate memory
rocStats = nan(nSNRs,nNoiseTypes,nMethods,1,8);


%% COMPUTE ROC STATISTICS
%
%
% Loop over number of noise types
for jj = 1 : nNoiseTypes
    
    % Loop over number of SNRs
    for kk = 1 : nSNRs
        
        % Compute the reference, the IBM, with an LC of 0 dB
        idealBinaryMask = ibm.ibm{kk,jj} > 0;
        
        % Loop over the number of methods
        for ii = 1 : nMethods
            
            % Check if missing data technique is used
            if P(ii).MD.bMissingData || P(ii).MD.bDirectMasking
                
                % Restrict range of center frequencies
                if isempty(P(ii).FEATURE.cfHzSelect)
                    cfHzIdx = 1:P(ii).FEATURE.nFilter;
                else
                    cfHzIdx = find(P(ii).FEATURE.cfHz > min(P(ii).FEATURE.cfHzSelect) ...
                        & P(ii).FEATURE.cfHz <= max(P(ii).FEATURE.cfHzSelect));
                end
                
                % Remove features located at the edges, since they are not relative
                % features due to the zero-padding
                if P(ii).FEATURE.bDecorrelate && P(ii).FEATURE.bRemoveEdges
                    cfHzIdx(cfHzIdx==1 | cfHzIdx==P(ii).FEATURE.nFilter) = [];
                end
                
                % Mask
                mask = ibm.(P(ii).MD.maskEstimation){kk,jj}(:,cfHzIdx);
                
                % Perform spectro-temporal integration
                if numel(P(ii).MD.contextTF) > 1
                    mask = integrateContext(mask,P(ii).MD.contextTF,...
                        P(ii).MD.contextShape,@mean,'replicate');
                end
                
                % Apply local threshold to binarize mask
                if P(ii).MD.bUseBinaryMask
                    if strcmpi(P(ii).MD.maskEstimation,'ebm_voicing')
                        mask = mask < P(ii).MD.maskThreshold;
                    else
                        mask = mask > P(ii).MD.maskThreshold;
                    end
                    
                    % Evaluate ROC statistics
                    [hitfa,hit,fa,miss,cr,acc,bacc] = calcROC(mask,...
                        idealBinaryMask(:,cfHzIdx));
                    
                    % Save hit rate minus false alarm rate
                    rocStats(kk,jj,ii,1) = hitfa;
                    rocStats(kk,jj,ii,2) = hit;
                    rocStats(kk,jj,ii,3) = cr;
                    rocStats(kk,jj,ii,4) = fa;
                    rocStats(kk,jj,ii,5) = miss;
                    rocStats(kk,jj,ii,6) = mean(mask(:)==true);
                    rocStats(kk,jj,ii,7) = acc;
                    rocStats(kk,jj,ii,8) = bacc;
                else
                    % Currently there is no evaluation for soft masks.
                    % Possibly, a MSE distance or a correlation metric
                    % could be used. 
                end
            end
        end
    end
end


