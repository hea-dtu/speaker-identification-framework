function OUT = SIDPresets(preset,F,varargin)
%SIDPresets   Presets for speaker identification systems


%% CHECK INPUT ARGUMENTS 
% 
% 
% Check for proper input arguments
if nargin < 2 
    help(mfilename);
    error('Wrong number of input arguments!')
end


%% CONFIGURE SID PRESETS
% 
% 
% Number of presets
nPresets = numel(preset);

% Loop over number of presets
for ii = 1 : nPresets

    % Initialize preset
    P = F; 
    P.preset = upper(preset{ii});
    
    % Select preset
    switch P.preset
        case 'MFCC'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;            
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';    
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_PMF'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;            
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';    
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_HTK'

            % Feature extraction parameters
            P.FEATURE.compress       = 'log';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [300 3700];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;            
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';    
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_GMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;            
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';    
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_PMF_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_0'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;           
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;               
        
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;    
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;    
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M12'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -12;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;    
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M15'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -15;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M18'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -18;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'MFCC_FHEQ_DM_F26_IBM_LC_M21'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -21;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
          
        case 'MFCC_FHEQ_DM_F26_SPP_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FHEQ_DM_F26_SPP_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
 		case 'MFCC_FHEQ_DM_F26_SPP_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
   
		case 'MFCC_FHEQ_DM_F26_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
			
   		case 'MFCC_FHEQ_DM_F26_SPP_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
			
		case 'MFCC_FHEQ_DM_F26_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
		case 'MFCC_FHEQ_DM_F26_SPP_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

		case 'MFCC_FHEQ_DM_F26_SPP_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'MFCC_FHEQ_DM_F26_SPP_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;              
			
        case 'MFCC_FHEQ_DM_F26_SPP_0P1_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FHEQ_DM_F26_SPP_0P2_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
 		case 'MFCC_FHEQ_DM_F26_SPP_0P3_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
   
		case 'MFCC_FHEQ_DM_F26_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
			
   		case 'MFCC_FHEQ_DM_F26_SPP_0P5_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
			
		case 'MFCC_FHEQ_DM_F26_SPP_0P6_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
		case 'MFCC_FHEQ_DM_F26_SPP_0P7_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

		case 'MFCC_FHEQ_DM_F26_SPP_0P8_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'MFCC_FHEQ_DM_F26_SPP_0P9_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;     
            
        case 'MFCC_FHEQ_DM_F26_VOICING_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_FHEQ_DM_F26_VOICING_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  

		case 'MFCC_FHEQ_DM_F26_VOICING_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

		case 'MFCC_FHEQ_DM_F26_VOICING_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
			
		case 'MFCC_FHEQ_DM_F26_VOICING_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
			
		case 'MFCC_FHEQ_DM_F26_VOICING_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
		case 'MFCC_FHEQ_DM_F26_VOICING_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  

		case 'MFCC_FHEQ_DM_F26_VOICING_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'MFCC_FHEQ_DM_F26_VOICING_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;              
            
        case 'MFCC_HTK_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'log';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [300 3700];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'MFCC_LOG_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'log';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;         
   
        case 'MFCC_AMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = true;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';    
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;

        case 'FBE'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_PMF'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_GMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_AMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_FMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_PMF_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M21'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -21;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M18'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -18;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M15'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -15;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M12'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -12;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M9'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDF_IBM_LC_M6'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_IBM_LC_M3'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDF_IBM_LC_0'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDB_IBM_LC_M21'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -21;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_M18'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -18;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_M15'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -15;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_M12'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -12;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_M9'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -9;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDB_IBM_LC_M6'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -6;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_M3'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -3;   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDB_IRM'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'irm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = false;
            P.MD.maskThreshold       = [];   
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDB_IBM_LC_0'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_IBM_LC_3'
            
            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
            
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
      case 'DFBE'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
      case 'DFBE_PMF'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
        case 'DFBE_GMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_AMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = true;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FMVN'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'meanvar';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_GHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_PMF_FHEQ'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;            
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_SPP_0P1_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_SPP_0P2_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;       
                        
        case 'DFBE_FHEQ_MDF_SPP_0P3_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
                        
        case 'DFBE_MDF_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_SPP_0P5_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;          
        
        case 'DFBE_FHEQ_MDF_SPP_0P6_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'DFBE_FHEQ_MDF_SPP_0P7_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;     
            
        case 'DFBE_FHEQ_MDF_SPP_0P8_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'DFBE_FHEQ_MDF_SPP_0P9_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;    
        
        case 'DFBE_FHEQ_MDF_SPP_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_SPP_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;       
                        
        case 'DFBE_FHEQ_MDF_SPP_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
                        
        case 'DFBE_MDF_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_SPP_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;          
        
        case 'DFBE_FHEQ_MDF_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'DFBE_FHEQ_MDF_SPP_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;     
            
        case 'DFBE_FHEQ_MDF_SPP_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'DFBE_FHEQ_MDF_SPP_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;               
                                   
        case 'DFBE_FHEQ_MDF_VOICING_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_VOICING_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_VOICING_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
        
        case 'DFBE_FHEQ_MDF_VOICING_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_VOICING_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_VOICING_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
                        
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;  
            
        case 'DFBE_FHEQ_MDF_VOICING_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
                        
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
        
        case 'DFBE_FHEQ_MDF_VOICING_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;            
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_VOICING_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [3 3];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            

        case 'DFBE_FHEQ_DM_F26_VOICING_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
        case 'DFBE_FHEQ_DM_F26_VOICING_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
        
        case 'DFBE_FHEQ_DM_F26_VOICING_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_VOICING_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_VOICING_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_VOICING_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'DFBE_FHEQ_DM_F26_VOICING_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             
        
        case 'DFBE_FHEQ_DM_F26_VOICING_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'DFBE_FHEQ_DM_F26_VOICING_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'rect';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P9_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P8_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P7_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P6_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P5_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
    
        case 'DFBE_FHEQ_DM_F26_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P3_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;              
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P2_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P1_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_SPP_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
    
        case 'DFBE_FHEQ_DM_F26_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;              
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_SPP_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
                        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M24'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -24;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M21'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
                        
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -21;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
                
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M18'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;            
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -18;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M15'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -15;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M12'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -12;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_DM_F26_IBM_LC_M3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;           
        
        case 'DFBE_FHEQ_DM_F26_IBM_LC_0'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = false;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = true;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_IBM_LC_M27'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -27;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_IBM_LC_M24'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -24;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_IBM_LC_M21'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -21;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_IBM_LC_M18'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -18;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_IBM_LC_M15'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -15;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_IBM_LC_M12'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -12;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'DFBE_FHEQ_MDF_IBM_LC_M9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'DFBE_FHEQ_MDF_IBM_LC_M6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
                                    
        case 'DFBE_FHEQ_MDF_IBM_LC_6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;          
            
        case 'DFBE_FHEQ_MDF_IBM_LC_M3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = -3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;     
            
        case 'DFBE_FHEQ_MDF_IBM_LC_3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
                        
        case 'DFBE_FHEQ_MDF_IBM_LC_0'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = true;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = true;
            P.POST.bFileBased        = true;
            P.POST.normType          = 'heq';       
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 20;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ibm';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
    
        case 'FBE_MDB_SPP_0P1_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_SPP_0P2_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             

        case 'FBE_MDB_SPP_0P3_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;         
            
        case 'FBE_PMF_MDB_SPP_0P4_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'FBE_MDB_SPP_0P5_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P6_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

        case 'FBE_PMF_MDB_SPP_0P6_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P7_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

        case 'FBE_MDB_SPP_0P8_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P9_NOINT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [1 1];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             
        
        case 'FBE_MDB_SPP_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_SPP_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             

        case 'FBE_MDB_SPP_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;         
            
        case 'FBE_PMF_MDB_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'FBE_MDB_SPP_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

        case 'FBE_PMF_MDB_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P6_NOCONTEXT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';     
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;   
            P.MD.maskThreshold       = 0.6;   
            P.MD.bSelectActiveFrames = false;  
            
        case 'FBE_MDB_SPP_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

        case 'FBE_MDB_SPP_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_SPP_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             
            
        case 'FBE_MDB_VOICING_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDB_VOICING_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;             

        case 'FBE_MDB_VOICING_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_VOICING_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
        case 'FBE_MDB_VOICING_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDB_VOICING_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 

        case 'FBE_MDB_VOICING_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;

        case 'FBE_MDB_VOICING_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;   
            
        case 'FBE_MDB_VOICING_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_voicing';
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;               
       
        case 'FBE_MDF_SPP_0P1'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.1;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;            
            
          case 'FBE_MDF_SPP_0P2'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.2;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
          case 'FBE_MDF_SPP_0P3'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.3;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
          case 'FBE_MDF_SPP_0P4'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.4;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
          case 'FBE_MDF_SPP_0P5'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.5;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
        
        case 'FBE_MDF_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_PMF_MDF_SPP_0P6'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = true;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.6;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false; 
            
        case 'FBE_MDF_SPP_0P7'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.7;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_SPP_0P8'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;            
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.8;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;
            
        case 'FBE_MDF_SPP_0P9'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = -inf;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';
            P.MD.contextTF           = [5 5];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = true;
            P.MD.maskThreshold       = 0.9;
            P.MD.alphaSoftMask       = [];
            P.MD.betaSoftMask        = [];
            P.MD.bSelectActiveFrames = false;              

        case 'FBE_MDB_SPP_SOFT'

            % Feature extraction parameters
            P.FEATURE.compress       = 'cuberoot';
            P.FEATURE.bDecorrelate   = false;
            P.FEATURE.bRemoveEdges   = false;
            P.FEATURE.cfHzSelect     = [];
            P.FEATURE.bRASTA         = false;
            
            % MFCC parameters
            P.FEATURE.bDCT           = false;
            P.FEATURE.typeDCT        = 3;
            P.FEATURE.nMFCCoeffs     = 13;
            P.FEATURE.bRemove1st     = true;
            
            % Feature post processing
            P.POST.deltaHalfWSize    = [];             
            P.POST.bNormPMF          = false;
            P.POST.bNormFeatures     = false;
            P.POST.bFileBased        = false;
            P.POST.normType          = 'meanvar';            
            P.POST.bAdaptive         = false;
            P.POST.tauSec            = 0;            
            P.POST.orderARMA         = 0;
            
            % GMM settings
            P.CLASSIFIER.bUseVAD     = true;
            P.CLASSIFIER.nGMMs       = 16;
            P.CLASSIFIER.nIterEM     = 5;
            P.CLASSIFIER.thresEM     = 1E-3;
            P.CLASSIFIER.cvType      = 'diag';
            P.CLASSIFIER.method      = 'netlab';
            
            % UBM settings
            P.CLASSIFIER.bUBM        = true;
            P.CLASSIFIER.nIterUBM    = 25;
     
            % Missing data settings
            P.MD.bMissingData        = true;
            P.MD.lowerBound          = 0;
            P.MD.bDirectMasking      = false;
            P.MD.floordB             = 26;
            P.MD.maskEstimation      = 'ebm_spp';       
            P.MD.contextTF           = [];
            P.MD.contextShape        = 'plus';
            P.MD.bUseBinaryMask      = false;
            P.MD.maskThreshold       = [];   
            P.MD.alphaSoftMask       = 10;
            P.MD.betaSoftMask        = 0.6;
            P.MD.bSelectActiveFrames = false;
            
        otherwise
            error('Preset ''%s'' is not supported!',upper(P.preset))
    end
    
    % Check for invalid settings
    if P.MD.bMissingData && P.MD.bDirectMasking    
        error(['Error in preset ''%s'': Select either '...
            '''bMissingData'' or ''bDirectMasking''.'],P.preset)
    end

    % Check for invalid delta feature settings
    if numel(P.POST.deltaHalfWSize) == 2 && P.POST.deltaHalfWSize(1) == 0
        error(['The delta window size may not be zero when requesting '...
            'accelecation features!']);
    end

    % Check for invalid delta feature settings
    if P.MD.bMissingData && P.MD.lowerBound == 0 && numel(P.POST.deltaHalfWSize) > 0
        error(['Bounded marginalization does not support the use of '...
            'delta features!'])
    end    
    
    % Overwrite settings specified by varargin
    if ~isempty(varargin)
        if 1
            P.CLASSIFIER = parseCell2Struct(P.CLASSIFIER,varargin);
        else
            namesP = fieldnames(P);
            % This is the general solution, but for now, we will restrict
            % it to the classifier settings 
            for pp = 1 : numel(namesP)
                if isstruct(P.(namesP{pp}))
                    % We should get rid of these warnings in case the
                    % fieldname is not found in a particular substruct.
                    P.(namesP{pp}) = parseCell2Struct(P.(namesP{pp}),varargin);
                end
            end
        end
    end
    
    % Compute center frequencies
    [~,P.FEATURE.cfHz] = calcFBE([1; zeros(1E3,1)],P.fsHz,...
        P.FEATURE.blockSec,P.FEATURE.stepSec,P.FEATURE.fRangeHz,...
        P.FEATURE.nFilter,false,false);
   
    % Accumulate settings
    OUT(ii) = P; %#ok
end

