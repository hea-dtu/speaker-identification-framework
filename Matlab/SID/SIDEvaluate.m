function [recRate,confMat] = SIDEvaluate(loglikelihood,label,fusion)
%SIDEvaluate   Evaluate speaker identification performance


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(fusion); fusion = {}; end

% Determine dimensionality of SID experiment
[nClasses,nSNRs,nNoiseTypes,nMethods] = size(loglikelihood);

% Number of score fusions
nFusions = numel(fusion);

% Allocate memory
recRate = zeros(nSNRs,nNoiseTypes,nMethods+nFusions);
confMat = zeros(nClasses,nClasses,nSNRs,nNoiseTypes,nMethods+nFusions);


%% FUSE SCORES
% 
% 
% Fuse scores of two SID systems
if ~isempty(fusion)
    
    % Range normalization
    minVal = min(loglikelihood,[],1);
    maxVal = max(loglikelihood,[],1);
    loglikelihood = bsxfun(@minus,loglikelihood,minVal);
    loglikelihood = bsxfun(@rdivide,loglikelihood,maxVal-minVal);

    % Loop over number of fusions
    for ii = 1 : nFusions
        % Combine likelihoods at equal weight
        loglikelihood = cat(4,loglikelihood,...
            sum(loglikelihood(:,:,:,fusion{ii}),4));
    end
end


%% SID DECISION
% 
% 
% Classification
classIdx = argmax(loglikelihood,1);
       

%% COMPUTE SID PERFORMANCE
% 
% 
% Loop over all SNRs
for ii = 1 : nSNRs
    
    % Loop over number of noise types
    for jj = 1 : nNoiseTypes
        
        % Loop over number of SID systems
        for kk = 1 : size(loglikelihood,4)
            
            % Compute confusion matrix and overall recognition performance
            [currConf,currRR] = confmat(classIdx2labelNetlab(classIdx(:,ii,jj,kk),nClasses),...
                classIdx2labelNetlab(label,nClasses));
            
            % Replace NaNs with zeros
            currConf(~isfinite(currConf)) = 0;
            
            % Store confusion matrix
            confMat(:,:,ii,jj,kk) = currConf;
            
            % Store performance
            recRate(ii,jj,kk) = currRR(1);
        end
    end
end
