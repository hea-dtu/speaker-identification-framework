%% EXPERIMENT PARAMETERS
% 
% 
% Select audio material
audioDatabase = [pwd,filesep,'..',filesep,'Audio',filesep,'EMIME',filesep];

% Audio file format
audioFileFormat = 'wav';

% Fore file names with this pattern to be part of the training 
% (SA***.wav files contain the same phonetic material for all speakers in
% the TIMIT database) 
audioFilePatternsForTraining = {};

% Number of simulations
nIterEXP = 3;

% Seed random generator by computer time (true), or use default seed (false)
bShuffleRNG = false;

% Restrict number of speaker to this maximum 
nSpeakersMax = 100;

% Restrict number of sentences per speaker to this maximum 
nSentencesMax = 10;

% Percentage of sentences used for training and testing 
trainVStestPerc = 0.7;

% SNR vector for testing
snrdB = -5 : 5 : 20;

% Select different types of interfering noises
noiseTypes = {'icra_01' 'icra_07' 'noisex_factory1'};


%% SID PRESETS
% 
% 
% Comparison of different features and normalization strategies
presets = {'FBE'                    ...
           'FBE_MDF_SPP_0P6'        ...
           'FBE_MDB_SPP_0P6'        ...
           'FBE_MDF_IBM_LC_0'       ...
           'FBE_MDB_IBM_LC_0'       ...
           'DFBE'                   ...
           'DFBE_FHEQ'              ...
           'DFBE_FHEQ_MDF_SPP_0P4'  ...
           'DFBE_FHEQ_MDF_IBM_LC_0'  };

% Control fusion of different approaches {[methodNr1 methodNr2] ...}
fusion = {[3 8]};


%% GENERAL FEATURE PARAMETERS
% 
% 
% ======================================================  
% The following general settings will affect all presets
% ======================================================

% Reference sampling frequency in Hz
fsHz = 16e3;

% Substract long-term mean 
bRemoveDC = false;

% Apply pre-emphasis filter
bPreEmphasis = false;

% Feature extraction parameters
blockSec = 20e-3;     % Block size in seconds
stepSec  = 10e-3;     % Frame step size in seconds
nFilter  = 32;        % Number of MEL-spaced subbands
fRangeHz = [80 8000]; % Frequency range

% Voice activity detection (VAD) threshold in dB
vadThresdB = 40;

% VAD hangover time constant in seconds
vadHangoverSec = 50E-3;
      
% Use Voice activity detection (VAD) during training to exclude silent
% frames (will overwrite SIDSetup.m)
bUseVAD = true;


%% GENERAL CLASSIFIER PARAMETERS
% 
% 
% Number of Gaussian mixture components (will overwrite SIDSetup.m)
% nGMMs = 128;
nGMMs = 64;
% nGMMs = 32;
% nGMMs = 16;
% nGMMs = 8;
% nGMMs = 4;

% Number of EM iterations
nIterEM = 5;

% Use universal background model adaptation (will overwrite SIDSetup.m)
bUseUBM = true;

% Iterations for UBM adaptation
nIterUBM = 5;
