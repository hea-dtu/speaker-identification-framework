function IBM = calcIBM_Triang(speech,noise,fsHz,blockSec,stepSec,fRangeHz,nFilter,LC)
%calcIBM_Triang   Compute IBM using a priori speech and noise information.
%   
%USAGE
%        IBM = calcIBM_Triang(speech,noise,fsHz)
%        IBM = calcIBM_Triang(speech,noise,fsHz,blockSec,stepSec,fRangeHz,nFilter,LC)
% 
%INPUT ARGUMENTS
%     speech : speech signal [nSamples x 1]
%      noise : noise signal  [nSamples x 1]
%       fsHz : sampling frequency in Hertz
%   blockSec : block size in seconds for which the SNR is computed
%              (default, blockSec = 32E-3)
%    stepSec : step size in seconds (default, blockSec = 8E-3)
%   fRangeHz : lower and upper frequency limit in Hertz 
%              (default, fRangeHz = [50 fsHz/2])
%    nFilter : number of triangular filters (default, nFilter = 20)
%         LC : local SNR criterion in dB to classify speech- and
%              noise-dominated T-F units (default, LC = 0)
% 
%OUTPUT ARGUMENTS
%        IBM : parameter structure containing the ideal binary mask

%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/04/15
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 8
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 4 || isempty(blockSec);  blockSec = 32e-3;       end
if nargin < 5 || isempty(stepSec);   stepSec  = 8e-3;        end
if nargin < 6 || isempty(fRangeHz);  fRangeHz = [50 fsHz/2]; end
if nargin < 7 || isempty(nFilter);   nFilter  = 20;          end
if nargin < 8 || isempty(LC);        LC       = 0;           end

% Check if signals have the same dimensions
if size(speech) ~= size(noise)
    error('Speech and noise signals must be of equal size!')
end

% Determine size of speech signal
[nSamples,nChannels] = size(speech);

% Check if input is monaural
if nChannels ~= 1
    error('Monaural input required.')
end


%% INITIALIZE PARAMETERS
% 
% 
% Framing parameters
blockSize = 2 * round(fsHz * blockSec / 2); 
stepSize  = round(fsHz * stepSec);          
nfft      = pow2(nextpow2(blockSize));
overlap   = blockSize - stepSize;
winType   = 'hamming'; 
win       = window(winType,blockSize);


%% Zero-padding
% 
% 
% Number of frames
nFrames = ceil((nSamples-overlap)/stepSize); 

% Compute number of required zeros
nZeros = ((nFrames * stepSize) + overlap) - nSamples;

% Pad input signal with zeros
speech = [speech; zeros(nZeros,1)];
noise  = [noise;  zeros(nZeros,1)];


%% AUDITORY SPECTROGRAM
% 
% 
% Compute spectrogram
specSpeech = spectrogram(speech,win,overlap,nfft,fsHz);
specNoise  = spectrogram(noise,win,overlap,nfft,fsHz);

% Implement triangular filter weights
[wts,mn,mx] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,false,false);

% Apply triangular filters
specAudSpeech = applyWeights_FFT(specSpeech(mn:mx,:),wts,'p_p');
specAudNoise  = applyWeights_FFT(specNoise(mn:mx,:),wts,'p_p');


%% COMPUTE IBM PATTERN
% 
% 
% Prevent division by zero
epsilon = realmin;

% Compute a-priori SNR
aPrioriSNR = 10 * log10(specAudSpeech ./ (specAudNoise + epsilon));
    
% Derive ideal binary mask
ibm = aPrioriSNR > LC;
    
% Summarize IBM parameter 
IBM = struct('label','IBM parameter structure','fsHz',fsHz,...
             'nSamples',nSamples,'blockSec',blockSec,'stepSec',stepSec,...
             'ibm',ibm,'aPrioriSNR',aPrioriSNR,'wts',wts,'mn',mn,...
             'mx',mx,'LC',LC);


%% SHOW IDEAL BINARY MASK
% 
% 
if nargout == 0
    figure;
    imagesc((1:nFrames)*stepSec,cfHz,ibm)
    axis xy;
    colorbar;
    
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    title('Estimated binary mask');
end         