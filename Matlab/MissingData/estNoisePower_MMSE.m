function [phiNNPow,SPP] = estNoisePower_MMSE(phiXXPow,hopSec,noiseOnlySec)
%estNoisePower_MMSE   Estimate MMSE noise power spectrum 
%   
%USAGE
%   [phiNNPow,SPP] = estNoisePower_MMSE(phiXXPow,hopSec)
%   [phiNNPow,SPP] = estNoisePower_MMSE(phiXXPow,hopSec,noiseOnlySec)
%
%INPUT ARGUMENTS
%       phiXXPow : noisy power spectrum [nFreq x nFrames]
%         hopSec : step size of frames in seconds
%   noiseOnlySec : assumed initial segment of noise only in seconds used to
%                  initialize the noise power estimate 
%                  (default, noiseOnlySec = 50E-3)
% 
%OUTPUT ARGUMENTS
%   phiNNPow : estimated noise power spectrum [nFreq x nFrames]
%        SPP : speech presence probability    [nFreq x nFrames]
% 
%   estNoisePower_MMSE(...) plots the estimated SPP in a new figure.
% 
%REFERENCES
%   [1] T. Gerkmann and R. C.Hendriks, "Unbiased MMSE-Based Noise Power
%       Estimation With Low Complexity and Low Tracking Delay", IEEE Trans.
%       Audio, Speech, Language Processing, 20, pp.1383-1393, 2012. 

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/04/16
%   v.0.2   2013/05/23 added SPP to output parameters
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(noiseOnlySec); noiseOnlySec = 50E-3; end

% Determine size of input signal
[nRealFreqs, nFrames] = size(phiXXPow);

% Allocate memory
phiNNPow = zeros(nRealFreqs, nFrames);
SPP      = zeros(nRealFreqs, nFrames);


%% INITIALIZE PARAMETERS
%
%
% Initial noise estimate (using the first noise-only frames)
nFramesInit  = fix(noiseOnlySec/hopSec);
noisePow     = mean(phiXXPow(:,1:nFramesInit),2);
PH1mean      = 0.5;
alphaPH1mean = exp(-hopSec/(-(256/16e3)/log(0.9)));
alphaPSD     = exp(-hopSec/(-(256/16e3)/log(0.8)));

% Constants for a posteriori SPP
q            = 0.5; % a priori probability of speech presence
priorFact    = q./(1-q);
xiOptDb      = 15;  % optimal fixed a priori SNR for SPP estimation
xiOpt        = 10.^(xiOptDb./10);
logGLRFact   = log(1./(1+xiOpt));
GLRexp       = xiOpt./(1+xiOpt);


%% NOISE POWER AND SPEECH PRESENCE PROBABILITY ESTIMATION
%
%
% Loop over number of frames
for ii = 1:nFrames
    
    % A posteriori SNR based on old noise power estimate
    snrPost1 = phiXXPow(:,ii)./(noisePow);
    
    % Noise power estimation
    GLR = priorFact .* exp(min(logGLRFact + GLRexp .* snrPost1,200));
    
    % A posteriori speech presence probability
    PH1 = GLR./(1+GLR);
    
    PH1mean        = alphaPH1mean * PH1mean + (1-alphaPH1mean) * PH1;
    stuckInd       = PH1mean > 0.99;
    PH1(stuckInd)  = min(PH1(stuckInd),0.99);
    estimate       = PH1 .* noisePow + (1-PH1) .* phiXXPow(:,ii) ;
    noisePow       = alphaPSD * noisePow+(1-alphaPSD) * estimate;
    
    % Store estimated noise power and speech presence probability
    phiNNPow(:,ii) = noisePow; 
    SPP(:,ii)      = PH1;
end


%% PLOT RESULTS
%
%
% Visualize SPP
if nargout == 0    
    figure;
    imagesc((1:nFrames)*hopSec,1:nRealFreqs,SPP)
    axis xy;
    colorbar;
    
    xlabel('Time (s)')
    ylabel('Number of FFT bins')
    title('Speech presence probability');
end


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************