function p = marginalize(fSpace,GMMs,mask,lowBound,bScale,erfSwitch) 
%marginalize   Calculate frame-based probabilities for GMM models.
%   The GMM models must be supplied in the NETLAB format. Besides the 
%   conventional Gaussian probability (baseline performance), two missing
%   data methods are supported:
%   1) Full marginalization utilizes a binary mask to ignore unreliable
%      time-frequency (T-F) units
%   2) Bounded marginalization exploits reliable T-F units, as identified 
%      by the binary mask, and additionally computes the counter-evidence  
%      of unreliable T-F units based on the concept of energetic masking.
% 
%USAGE  
%        prob = marginalize(fSpace,GMM) 
%        prob = marginalize(fSpace,GMM,mask,lowBound,bScale,erfSwitch) 
% 
%INPUT ARGUMENTS
%      fSpace : feature space matrix [nFrames x nFeatures]
%         GMM : trained GMM model(s) supplied in NETLAB format
%        mask : binary/soft mask identifying reliability of feature space 
%               of size [nFrames x nFeatures]
%    lowBound : lower integration bound of the marginal distribution, if
%               lowBound is omitted or negative, full marginalization is
%               performed (default, lowBound = -1)
%      bScale : scale marginal probability by the integration bounds, no
%               scaling is performed when bScale is false or set to zero
%               (default, bScale = true)
%   erfSwitch : select error function (default, erfSwitch = 2)
%               1 = Approximation by Sun Microsystems 
%               2 = Approximation by A & S
%               3 = Approximation used by Sheffield university
% 
%  OUTPUT ARGUMENTS
%        prob : frame-based probabilities for each GMM [nFrames x nGMMs]
% 
%NOTE
%   If only the feature space and the trained GMM models are supplied,
%   conventional Gaussian probabilities are calculcated. Given a binary
%   mask as third input argument, 'full marginalization' is performed by  
%   simply ignoring unreliable T-F units and utilize only relible T-F
%   units. The fourth input argument specifies the lower bound used by the
%   'bounded marginalization' approach, which is typically set to 0.
% 
%REFERENCES
%   [1]  M. Cooke, P. Green, L. Josifovski, and A. Vizinho, "Robust
%        automatic speech recognition with missing and unreliable acoustic
%        data," Speech Commun., vol. 34, pp. 267�285, 2001.  

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2007-2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/08/01 re-written
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 6
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(mask);      mask      = ones(size(fSpace)); end
if nargin < 4 || isempty(lowBound);  lowBound  = 0;                  end
if nargin < 5 || isempty(bScale);    bScale    = 1;                  end
if nargin < 6 || isempty(erfSwitch); erfSwitch = 2;                  end

% Check if all GMM models have the same GMM model complexity
if sum(diff([GMMs(:).ncentres])) > 0
    error('GMM complexity varies across class models.')
end

% Check if feature space is within bounds
if isfinite(lowBound) && any(fSpace(:) < lowBound)
    warning(['Feature space contains values below the lower bound of ',...
             num2str(lowBound)])
end

% Check if feature space is finite
if any(isinf(fSpace(:)) | isnan(fSpace(:)))
    error('Feature space is not finite.')
end

% Check if missing data mask is finite
if any(isinf(mask(:)) | isnan(mask(:)))
    error('Missing data mask is not finite.')
end

% Determine type of missing data mask
if isa(mask,'logical')
    % Binary missing data mask
    bBinaryMarginalization = true;
else
    % Check if mask is binary or soft
    if isBinary(mask)
        bBinaryMarginalization = true;
    else
        bBinaryMarginalization = false;
    end
end

% Check if full marginalization should be used
if ~isfinite(lowBound) 
    if ~bBinaryMarginalization
        error('Full marginalization is not supported for soft masks.')
    else
        % Set the lower bound to -1 ... for full marginalization
        lowBound = -1;
    end
end


%% PERFORM MARGINALIZATION
% 
% 
% Ensure that the mask is of type double
mask = double(mask);

% Select marginalization method
if bBinaryMarginalization
    % =====================================================================
    % BINARY MASK MARGINALIZATION (either 0 or 1)
    % =====================================================================
    %
    % MEX processing
    p = marginalizeBinaryMEX(fSpace,GMMs,mask,lowBound,bScale,erfSwitch);
else
    % =====================================================================
    % SOFT MASK MARGINALIZATION ([0, 1])
    % =====================================================================
    %
    % MEX processing
    p = marginalizeSoftMEX(fSpace,GMMs,mask,lowBound,bScale,erfSwitch);
end
                

%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************