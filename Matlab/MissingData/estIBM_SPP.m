function EBM = estIBM_SPP(mix,fsHz,blockSec,stepSec,fRangeHz,nFilter,intTF,thresSPP)
%estIBM_SPP   Estimate the ideal binary mask via speech presence probability.
%   
%USAGE
%   EBM = estIBM_SPP(mix,fsHz)
%   EBM = estIBM_SPP(mix,fsHz,blockSec,stepSec,fRangeHz,nFilter,intTF,thresSPP)
% 
%INPUT ARGUMENTS
%        mix : noisy speech signal [nSamples x 1]
%       fsHz : sampling frequency in Hertz
%   blockSec : block size in seconds (default, blockSec = 20E-3)
%    stepSec : step size in seconds  (default, stepSec = blockSec * 0.5)
%   fRangeHz : lower and upper frequency limit in Hertz 
%              (default, fRangeHz = [80 fsHz/2])
%    nFilter : number of triangular filters (default, nFilter = 32)
%      intTF : dimensions of a plus-shaped integration window spanning
%              across time and frequency, values must be odd 
%              (default, intTF = [5 5])
%   thresSPP : threshold to binarize the speech presence probability
%              (default, thresSPP = 0.4)
% 
%OUTPUT ARGUMENTS
%        EBM : EBM parameter structure containing the estimated binary mask
% 
%   estIBM_SPP(...) plots the EBM in a new figure.
% 
%REFERENCES
%   [1] T. May and T. Gerkmann, "Generalization of supervised learning for
%       binary mask estimation", International Workshop on Acoustic Signal
%       Enhancement (IWAENC), pp.154-158, 2014. 

%   Developed with Matlab 8.3.0.532 (R2014a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/12
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 8
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(blockSec);  blockSec = 20e-3;          end
if nargin < 4 || isempty(stepSec);   stepSec  = 0.5 * blockSec; end
if nargin < 5 || isempty(fRangeHz);  fRangeHz = [80 fsHz/2];    end
if nargin < 6 || isempty(nFilter);   nFilter  = 32;             end
if nargin < 7 || isempty(intTF);     intTF    = [5 5];          end
if nargin < 8 || isempty(thresSPP);  thresSPP = 0.4;            end

% Determine size of input signal
dim = size(mix);
    
% Check if input is mono
if min(dim) > 1
    error('Input must be mono.')
else
    % Number of samples
    dim = max(dim);
end

% Check if intTF are odd numbers
if any(~(intTF-fix(intTF./2)*2))
    error('All elements in intTF need to be odd-numbered')
end


%% INITIALIZE PARAMETERS
% 
% 
% Initial noise-only segment in seconds
noiseOnlySec = 50E-3;

% Framing parameters
blockSize = 2 * round(fsHz * blockSec / 2); 
stepSize  = round(fsHz * stepSec);          
nfft      = pow2(nextpow2(blockSize));
overlap   = blockSize - stepSize;
winType   = 'hamming';
win       = window(winType,blockSize);

% Implement triangular filter weights
[wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,false,false);


%% ZERO-PADDING
% 
% 
% Number of frames
nFrames = ceil((dim-overlap)/stepSize); 

% Compute number of required zeros
nZeros = ((nFrames * stepSize) + overlap) - dim;

% Pad input signal with zeros
mix = [mix; zeros(nZeros,1)];


%% NOISE POWER ESTIMATION
% 
% 
% Compute spectrogram
specMix = spectrogram(mix,win,overlap,nfft,fsHz);

% Power spectrum
specPow = specMix .* conj(specMix);

% Estimate SPP based on the estimated noise power
[~,specSSP] = estNoisePower_MMSE(specPow,stepSec,noiseOnlySec);

% Integrate energy-scaled SPP into auditory filters
audSPP = (wts * (specSSP(mn:mx,:) .* specPow(mn:mx,:)))./(wts * specPow(mn:mx,:));


%% SPECTRO-TEMPORAL INTEGRATION
% 
% 
% Average SPP across a plus-shaped neighborhood function
if any(intTF > 1)
    audSPP = integrateContext(audSPP,intTF,'plus',@mean,'replicate');
end


%% COMPUTE IBM PATTERN
% 
% 
% Derive ideal binary mask
ibm = audSPP > thresSPP;

% Summarize IBM parameter 
EBM = struct('label','IBM parameter structure','fsHz',fsHz,...
             'nSamples',dim,'bPadZeros',true,...
             'blockSec',blockSec,'stepSec',stepSec,...
             'nfft',nfft,'window',win,'fb2FFT',wts,...
             'ibm',ibm,'LC',thresSPP,'audSPP',audSPP);

         
%% SHOW ESTIMATED BINARY MASK
% 
% 
if nargout == 0
    figure;
    imagesc((1:nFrames)*stepSec,cfHz,ibm)
    axis xy;
    colorbar;
    
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    title('Estimated binary mask');
end