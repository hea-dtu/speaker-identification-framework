function dmask = calcDeltaMask(mask,order,dim)
%calcDeltaMask   Compute delta binary mask across a regression window.
%   The resulting delta mask will contain ones only if all units across the
%   delta window are reliable.
%   
%USAGE
%       ibmD = calcDeltaMask(mask,order)
%       ibmD = calcDeltaMask(mask,order,dim)
% 
%INPUT ARGUMENTS
%       mask : binary mask pattern [nFrames x nChannels]
%      order : order of delta processing
%        dim : dimension across which the delta mask should be computed
%              (default, dim = 1)
% 
%OUTPUT ARGUMENTS
%      dmask : delta mask pattern [nFrames x nChannels]
% 
%   calcDeltaMask(...) plots the mask and the delta mask in a new figure.
% 
%REFERENCES
%   [1] J. Barker, L. Josifovski, M. Cooke and P. Green, "Soft decisions in
%       missing data techniques for robust automatic speech recognition",
%       Proc. ICSLP, 2000.

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/05/18
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(dim); dim = 1; end

% Check if order is odd 
if isEven(order)
   error('The order must be odd') 
end


%% COMPUTE DELTA IBM PATTERN
% 
% 
% Select dimension across which the delta IBM should be performed
switch dim
    case 1
        h = ones(order,1);
        h((order+1)/2) = 0;
    case 2
        h = ones(1,order);
        h((order+1)/2) = 0;
    otherwise
        error('Filter dimension must be either 1 or 2')
end

% Apply filter
dmask = sffilt(@sum,double(mask),h,'replicate');
dmask = dmask == (order-1);
        
            
%% SHOW IBM BEFORE AND AFTER DELTA OPERATION
% 
% 
if nargout == 0
    figure;
    subplot(211)
    if dim == 1
        imagesc(mask)
        xlabel('Number of frequency channels')
        ylabel('Number of frames')
    else
        imagesc(mask)
        xlabel('Number of frames')
        ylabel('Number of frequency channels')
    end
    axis xy;
    colorbar;
    
    title('Mask');
    
    subplot(212)
    if dim == 1
        imagesc(dmask==1)
        xlabel('Number of frequency channels')
        ylabel('Number of frames')
        title('\Delta_f mask');
    else
        imagesc(dmask==1')
        xlabel('Number of frames')
        ylabel('Number of frequency channels')
        title('\Delta_t mask');
    end
    axis xy;
    colorbar;
end