function mask = applyRateThreshold(FBE,blockSec,stepSec,winMASec,thresdB,winMA)
%applyRateThreshold   Binary mask based on rate threshold selection.
%   
%USAGE
%   mask = applyRateThreshold(FBE,blockSec,stepSec)
%   mask = applyRateThreshold(FBE,blockSec,stepSec,winMASec,thresdB,winMA)
% 
%INPUT ARGUMENTS
%        FBE : filterbank energy (FBE) features [nChannels x nFrames]
%   blockSec : block size in seconds of FBE features 
%    stepSec : step size in seconds of FBE features 
%   winMASec : window size in seconds for moving average energy calculation 
%              (default, winAGCSec = 200E-3)
%    thresdB : rate threshold in dB, if the FBE feature value drops below
%              the long-term moving average by this fraction, the
%              corresponding time-frequency unit will be labeled unreliable
%              (default, thresdB = -11)
%      winMA : string or handle defining the moving average window shape 
%              (default, winMA = 'triang')
% 
%OUTPUT ARGUMENTS
%       mask : binary mask indicating reliable units [nChannels x nFrames]
% 
%   applyRateThreshold(...) plots the binary mask in a new figure.
% 
%REFERENCES
%   [1] K. J. Palom�ki, G. J. Brown and D. L. Wang, "A binaural processor
%       for missing data speech recognition in the presence of noise and
%       small-room reverberation", Speech Communication, vol. 43(4),
%       pp.361-378, 2004.    

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/08/05
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 3 || nargin > 6
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 4 || isempty(winMASec); winMASec = 200E-3;   end
if nargin < 5 || isempty(thresdB);  thresdB  = -11;      end
if nargin < 6 || isempty(winMA);    winMA    = 'triang'; end


%% MOVING AVERAGE ENERGY CALCULATION
% 
% 
% Calculate number of frames for moving average calculation
nFrMA = max(0,1+floor((winMASec - blockSec)/stepSec));

% Force 'nFrAGC' to be odd
if isEven(nFrMA);
    nFrMA = nFrMA + 1; 
end

% Create window
winFct = window(winMA,nFrMA);

% Normalize window to preserve mean
winFct = winFct./sum(winFct);

% Zero-padding
rateMod = [FBE; zeros((nFrMA-1)/2,size(FBE,2));];
rateMA  = fftfilt(winFct,rateMod);

% Shift backward
rateMA = rateMA((1+((nFrMA-1)/2)):end,:);


%% CALCULATE BINARY MASK
% 
% 
% Remove elements below threshold
mask = 10 * log10(FBE./rateMA) > thresdB;


%% PLOT RESULTS
% 
% 
% Plot
if nargout == 0

    figure;
    ax(1) = subplot(311);
    imagesc(FBE);axis xy;
    title('FBE features')
    
    ax(2) = subplot(312);
    imagesc(10 * log10(FBE./rateMA));axis xy
    title('FBE features after moving average normalization')
    ylabel('Number of channels')
    
    ax(3) = subplot(313);
    imagesc(mask);axis xy
    title('Binary mask')
    xlabel('Number of frames')
    
    linkaxes(ax,'xy');
end