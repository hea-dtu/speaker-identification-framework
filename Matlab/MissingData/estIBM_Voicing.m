function EBM = estIBM_Voicing(mix,fsHz,blockSec,stepSec,fRangeHz,nFilter,M,dimMed1,dimMed2,thres)
%estIBM_Voicing   Estimate the ideal binary mask using the voicing distance.
%   
%USAGE
%   EBM = estIBM_Voicing(mix,fsHz)
%   EBM = estIBM_Voicing(mix,fsHz,blockSec,stepSec,fRangeHz,nFilter,M,dimMed1,dimMed2,thres)
% 
%INPUT ARGUMENTS
%        mix : noisy speech signal [nSamples x 1]
%       fsHz : sampling frequency in Hertz
%   blockSec : block size in seconds (default, blockSec = 20E-3)
%    stepSec : step size in seconds  (default, blockSec = 10E-3)
%   fRangeHz : lower and upper frequency limit in Hertz 
%              (default, fRangeHz = [80 fsHz/2])
%    nFilter : number of triangular filters (default, nFilter = 32)
%          M : FFT bin context (-M:1:M) across which the voicing distance 
%              is computed (default, M = 3)
%    dimMed1 : dimension of 2D median filter [dimTime dimFrequency] which
%              is applied to the interpolated voicing distance. If dimMed
%              is one-dimensional, the same value will be used for both
%              dimensions. (default, dimMed = [5 9]) 
%    dimMed2 : dimension of 2D median filter [dimTime dimFrequency] which
%              is applied to the auditory filter-based voicing distance. If
%              dimMed is one-dimensional, the same value will be used for
%              both dimensions. (default, dimMed = [3 3]) 
%      thres : threshold to binarize the voicing-distance
%              (default, thres = 0.21 according to [2])
% 
%OUTPUT ARGUMENTS
%        EBM : EBM parameter structure containing the estimated binary mask
% 
%   estIBM_Voicing(...) plots the EBM in a new figure.
% 
%REFERENCES
%   [1] P. Jancovic and M. K�k�er, "Voicing-character estimation of speech
%       spectra: Application to noise robust speech recognition", IEEE
%       ICASSP, pp.257-260, 2006.
% 
%   [2] P. Jancovic and M. K�k�er, "Estimation of Voicing-Character of
%       Speech Spectra Based on Spectral Shape ", IEEE Signal Processing
%       Letters, vol. 14(1), pp.66-69, 2007. 

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/06/24
%   v.0.2   2014/11/21 optimized code and added documentation
%   v.0.3   2015/05/31 added second median filter
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 10
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3  || isempty(blockSec);  blockSec = 20e-3;       end
if nargin < 4  || isempty(stepSec);   stepSec  = 10e-3;       end
if nargin < 5  || isempty(fRangeHz);  fRangeHz = [80 fsHz/2]; end
if nargin < 6  || isempty(nFilter);   nFilter  = 32;          end
if nargin < 7  || isempty(M);         M        = 3;           end
if nargin < 8  || isempty(dimMed1);   dimMed1  = [5 9];       end
if nargin < 9  || isempty(dimMed2);   dimMed2  = [3 3];       end
if nargin < 10 || isempty(thres);     thres    = 0.21;        end

% Determine size of input signal
dim = size(mix);
    
% Check if input is mono
if min(dim) > 1
    error('Input must be mono.')
else
    % Number of samples
    dim = max(dim);
end

% Check if dimMed1 are odd numbers
if any(~(dimMed1-fix(dimMed1./2)*2))
   error('All elements in ''dimMed1'' need to be odd-numbered.') 
end

% Check if dimMed2 are odd numbers
if any(~(dimMed2-fix(dimMed2./2)*2))
   error('All elements in ''dimMed2'' need to be odd-numbered.') 
end

% Check median filter dimensionality ''dimMed1''
switch numel(dimMed1)
    case 1
        % Replicate dimension to both time and frequency
        dimMed1 = [dimMed1 dimMed1];
    case 2
        % Do nothing
    otherwise
        error('1 or 2-dimensional input expected for median filter.')
end

% Check median filter dimensionality ''dimMed2''
switch numel(dimMed2)
    case 1
        % Replicate dimension to both time and frequency
        dimMed2 = [dimMed2 dimMed2];
    case 2
        % Do nothing
    otherwise
        error('1 or 2-dimensional input expected for median filter.')
end


%% INITIALIZE PARAMETERS
% 
% 
% Framing parameters
blockSize = 2 * round(fsHz * blockSec / 2); 
stepSize  = round(fsHz * stepSec);          
nfft      = pow2(nextpow2(blockSize));
overlap   = blockSize - stepSize;
winType   = 'hamming';
win       = window(winType,blockSize);

% Implement triangular filter weights
[wts,mn,mx,cfHz] = createFB_Triang(fsHz,nfft,nFilter,fRangeHz,false,false);


%% ZERO-PADDING
% 
% 
% Number of frames
nFrames = ceil((dim-overlap)/stepSize); 

% Compute number of required zeros
nZeros = ((nFrames * stepSize) + overlap) - dim;

% Pad input signal with zeros
mix = [mix; zeros(nZeros,1)];


%% COMPUTE SPECTRA
% 
% 
% Compute spectrogram
spec = spectrogram(mix,win,overlap,nfft,fsHz);

% Spectrum of window
specWin = spectrogram(ones(blockSize,1),win,overlap,nfft,fsHz);

% Magnitude spectra
specMag = abs(spec);
winMag  = abs(specWin);

% Determine dimensionality
[nRealFreqs,nFrames] = size(specMag);

% Allocate memory
vd = zeros(nRealFreqs,nFrames);

% Frame analysis window
mIdx = -M:1:M;

% Spectrum of window within the specified range (-M:M)
winSpec = [winMag((M+1):-1:2); winMag(1:M+1)];

% Normalize magnitude to one at peak position
winSpec = winSpec ./ winSpec(M+1);


%% DETERMINE VOICING DISTANCE
%
%
% Loop over number of frames
for ii = 1:nFrames
    
    % Detect all local peaks
    peakIdx = v_findpeaks(specMag(:,ii));
    
    % Number of local peaks
    nPeaks = numel(peakIdx);
    
    % Initialize variables
    pastIdx  = 0;
    pastAmpl = inf;
    
    % Loop over number of peaks
    for jj = 1 : nPeaks
        % Indices of local peak within the range mIdx
        voiceIdx = mIdx+peakIdx(jj);
        voiceIdx = voiceIdx(voiceIdx>=1 & voiceIdx<=nRealFreqs);
        
        % Normalize magnitude to one at peak position
        specIn  = specMag(voiceIdx,ii);
        specIn  = specIn ./ specMag(peakIdx(jj),ii);
        specWin = winSpec(voiceIdx-peakIdx(jj)+M+1);
        
        % Voicing RMS distance
        voiceAmpl = sqrt(mean(power(specIn - specWin,2)));
        
        % Detect overlapping peaks
        N        = numel(voiceIdx);
        bOverlap = false(N,1);
        for kk = 1 : N
            bOverlap(kk) = any(voiceIdx(kk) == pastIdx);
        end
        
        % Store voicing distance, use minimum in case of overlapping peaks
        vd(voiceIdx(~bOverlap),ii) = voiceAmpl;
        vd(voiceIdx( bOverlap),ii) = min([pastAmpl voiceAmpl]);
        
        % Keep track of past peak information
        pastAmpl = voiceAmpl;
        pastIdx  = voiceIdx;
    end
    
    % Interpolation
    ok = vd(:,ii)~=0;
    
    % Find endpoints for extrapolation
    if ok(1) == 0
        idxStart = 1:find(diff(ok)==1);
        vd(idxStart,ii) = interp1(find(ok),vd(ok,ii),idxStart,'nearest','extrap');
    else
        idxStart = 0;
    end
    if ok(end) == 0
        idxEnd = nRealFreqs-find(diff(ok(end:-1:1))==1)+1:nRealFreqs;
        vd(idxEnd,ii) = interp1(find(ok),vd(ok,ii),idxEnd,'nearest','extrap');
    else
        idxEnd = nRealFreqs + 1;
    end
    
    % Extrapolate indices
    extPolIdx = (idxStart(end)+1):idxEnd(1)-1;

    % Extrapolate missing channels
    vd(extPolIdx,ii) = interp1(find(ok),vd(ok,ii),extPolIdx,'pchip','extrap');
end


%% POST-PROCESSING
% 
% 
if any(dimMed1 > 1)
    % 2D median filter
    vd = medfilt2(vd,[dimMed1(2) dimMed1(1)],'symmetric');
end

% Integrate voicing-distance into auditory channels 
voiceRaw = (wts * (vd(mn:mx,:) .* specMag(mn:mx,:).^2)) ./ (wts * specMag(mn:mx,:).^2);

if any(dimMed2 > 1)
    % 2D median filter
    voiceDist = medfilt2(voiceRaw,[dimMed2(2) dimMed2(1)],'symmetric');
else
    % Use raw voicing distance directly
    voiceDist = voiceRaw;
end


%% COMPUTE IBM PATTERN
% 
% 
% Derive ideal binary mask
ibm = voiceDist < thres;

% Summarize IBM parameter 
EBM = struct('label','IBM parameter structure','fsHz',fsHz,...
             'nSamples',dim,'bPadZeros',true,...
             'blockSec',blockSec,'stepSec',stepSec,...
             'nfft',nfft,'window',win,'fb2FFT',wts,...
             'ibm',ibm,'voicingThres',thres,'voiceDist',voiceDist);

         
%% SHOW ESTIMATED BINARY MASK
% 
% 
if nargout == 0
    figure;
    imagesc((1:nFrames)*stepSec,cfHz,ibm)
    axis xy;
    colorbar;
    
    xlabel('Time (s)')
    ylabel('Frequency (Hz)')
    title('Estimated binary mask');
end