function out = sigmoid(in,alpha,beta)
%sigmoid   Pass input through a sigmoid function.
%
%                        1
%     out = -------------------------------
%           (1 + exp(-alpha * (in - beta)))
%
%USAGE
%     out = sigmoid(in,alpha)
%     out = sigmoid(in,alpha,beta)
%
%INPUT ARGUMENTS
%      in : input signal [nSamples x nChannels]
%   alpha : sigmoid slope
%    beta : sigmoid center (default, beta = 0)
% 
%OUTPUT ARGUMENTS
%     out : transformed signal [nSamples x nChannels]
%
%REFERENCES
%   [1] Barker, J., Josifovski, L., Cooke, M., and Green, P. (2000),"Soft 
%       decisions in missing data techniques for robust automatic speech 
%       recognition," International Conference on Spoken Language 
%       Processing (ICSLP).

%   Developed with Matlab 8.5.0.197613 (R2015a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2015
%              Technical University of Denmark (DTU)
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2015/08/01
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS  
% 
% 
% Check for proper input arguments
if nargin < 2 || nargin > 3
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 3 || isempty(beta); beta = 0; end


%% PERFORM SIGMOID MAPPING
% 
% 
% Sigmoid function
out = 1./(1 + exp(-alpha * (in - beta)));
