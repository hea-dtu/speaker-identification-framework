#include <math.h>
#include <cmath>
#include "mex.h"

using namespace std;

/* Helper functions */
#define max(x, y)   ((x) > (y) ? (x) : (y))
#define	min(A, B)	((A) < (B) ? (A) : (B))
#define swap(A,B)   temp = (A); (A)=(B); (B) = temp;

/* Input Arguments */
#define   FSPACE         prhs[0]  // feature space matrix
#define   GMM            prhs[1]  // Trained GMM models stored in a struct (NETLAB format)
#define   MASK           prhs[2]  // mask indicating reliability of feature space
#define   LOWERBOUND     prhs[3]  // lower integration bound for bounded marginalization
#define   SCALE          prhs[4]  // scale marginals according to integration bounds
#define   ERFSWITCH      prhs[5]  // switch for erf approximation

/* Output Arguments */
#define   PROBMATRIX     plhs[0]  // frame-based probabilities

/*  Set resolution of floating point numbers */
const double EPS = pow(2.00,-52.00);

/* Used for gaussian probability calculation */
const double M_sqrt2Pi = sqrt(2.0 * (4.0 * atan(1.0)));
const double M_sqrtOf2 = sqrt(2.0);

void usage()
{
mexPrintf(" marginalize   Calculate frame-based probabilities for GMM models.\n");
mexPrintf("   The GMM models must be supplied in the NETLAB format. Besides the \n");
mexPrintf("   conventional Gaussian probability (baseline performance), two missing\n");
mexPrintf("   data methods are supported:\n");
mexPrintf("   1) Full marginalization utilizes a binary mask to ignore unreliable\n");
mexPrintf("      time-frequency (T-F) units\n");
mexPrintf("   2) Bounded marginalization exploits reliable T-F units, as identified \n");
mexPrintf("      by the binary mask, and additionally computes the counter-evidence  \n");
mexPrintf("      of unreliable T-F units based on the concept of energetic masking.\n");
mexPrintf("\n"); 
mexPrintf(" USAGE  \n");
mexPrintf("        prob = marginalize(fSpace,GMM) \n");
mexPrintf("        prob = marginalize(fSpace,GMM,mask,lowBound,bScale,erfSwitch) \n");
mexPrintf("\n"); 
mexPrintf(" INPUT ARGUMENTS\n");
mexPrintf("      fSpace : feature space matrix [nFrames x nFeatures]\n");
mexPrintf("         GMM : trained GMM model(s) supplied in NETLAB format\n");
mexPrintf("        mask : binary mask identifying reliable feature components of size\n");
mexPrintf("               [nFrames x nFeatures]\n");
mexPrintf("    lowBound : lower integration bound of the marginal distribution, if\n");
mexPrintf("               lowBound is omitted or negative, full marginalization is\n");
mexPrintf("               performed (default, lowBound = -1)\n");
mexPrintf("      bScale : scale marginal probability by the integration bounds, no\n");
mexPrintf("               scaling is performed when bScale is false or set to zero\n");
mexPrintf("               (default, bScale = true)\n");
mexPrintf("   erfSwitch : select error function (default, erfSwitch = 2)\n");
mexPrintf("               1 = Approximation by Sun Microsystems \n");
mexPrintf("               2 = Approximation by A & S\n");
mexPrintf("               3 = Approximation used by Sheffield university\n");
mexPrintf("\n"); 
mexPrintf("  OUTPUT ARGUMENTS\n");
mexPrintf("        prob : frame-based probabilities for each GMM [nFrames x nGMMs]\n");
mexPrintf("\n"); 
mexPrintf(" NOTE\n");
mexPrintf("   If only the feature space and the trained GMM models are supplied,\n");
mexPrintf("   conventional Gaussian probabilities are calculcated. Given a binary\n");
mexPrintf("   mask as third input argument, \"full marginalization\" is performed by  \n");
mexPrintf("   simply ignoring unreliable T-F units and utilize only relible T-F\n");
mexPrintf("   units. The fourth input argument specifies the lower bound used by the\n");
mexPrintf("   \"bounded marginalization\" approach, which is typically set to 0.\n");
mexPrintf("\n"); 
mexPrintf(" REFERENCES\n");
mexPrintf("   [1]  M. Cooke, P. Green, L. Josifovski, and A. Vizinho, \"Robust\n");
mexPrintf("        automatic speech recognition with missing and unreliable acoustic\n");
mexPrintf("        data,\" Speech Commun., vol. 34, pp. 267�285, 2001.  \n");
mexPrintf("\n"); 
mexPrintf(" Implementation by Tobias May, 2007-2015, DTU \n");
mexPrintf("\n"); 
}    

// Calculate Gaussian probability 
double calcGaussProb(double fSpace, double mu, double var)
{
	return exp(-0.5 * (pow(fSpace - mu,2) / var)) / (M_sqrt2Pi * sqrt(var));
}

// Error function calculation
double erf_Sheffield(double in)
{
   return 2.0/(1.0+exp(-2.3236*in))-1.0;
}

double erf_Sun(double x);
double erfc_Sun(double x);
static const double tiny = 1e-300,
        half= 5.00000000000000000000e-01, /* 0x3FE00000, 0x00000000 */
        one = 1.00000000000000000000e+00, /* 0x3FF00000, 0x00000000 */
        two = 2.00000000000000000000e+00, /* 0x40000000, 0x00000000 */
        /* c = (float)0.84506291151 */
        erx = 8.45062911510467529297e-01, /* 0x3FEB0AC1, 0x60000000 */
        /*
         * Coefficients for approximation to erf on [0,0.84375]
         */
        efx = 1.28379167095512586316e-01, /* 0x3FC06EBA, 0x8214DB69 */
        efx8= 1.02703333676410069053e+00, /* 0x3FF06EBA, 0x8214DB69 */
        pp0 = 1.28379167095512558561e-01, /* 0x3FC06EBA, 0x8214DB68 */
        pp1 = -3.25042107247001499370e-01, /* 0xBFD4CD7D, 0x691CB913 */
        pp2 = -2.84817495755985104766e-02, /* 0xBF9D2A51, 0xDBD7194F */
        pp3 = -5.77027029648944159157e-03, /* 0xBF77A291, 0x236668E4 */
        pp4 = -2.37630166566501626084e-05, /* 0xBEF8EAD6, 0x120016AC */
        qq1 = 3.97917223959155352819e-01, /* 0x3FD97779, 0xCDDADC09 */
        qq2 = 6.50222499887672944485e-02, /* 0x3FB0A54C, 0x5536CEBA */
        qq3 = 5.08130628187576562776e-03, /* 0x3F74D022, 0xC4D36B0F */
        qq4 = 1.32494738004321644526e-04, /* 0x3F215DC9, 0x221C1A10 */
        qq5 = -3.96022827877536812320e-06, /* 0xBED09C43, 0x42A26120 */
        /*
         * Coefficients for approximation to erf in [0.84375,1.25]
         */
        pa0 = -2.36211856075265944077e-03, /* 0xBF6359B8, 0xBEF77538 */
        pa1 = 4.14856118683748331666e-01, /* 0x3FDA8D00, 0xAD92B34D */
        pa2 = -3.72207876035701323847e-01, /* 0xBFD7D240, 0xFBB8C3F1 */
        pa3 = 3.18346619901161753674e-01, /* 0x3FD45FCA, 0x805120E4 */
        pa4 = -1.10894694282396677476e-01, /* 0xBFBC6398, 0x3D3E28EC */
        pa5 = 3.54783043256182359371e-02, /* 0x3FA22A36, 0x599795EB */
        pa6 = -2.16637559486879084300e-03, /* 0xBF61BF38, 0x0A96073F */
        qa1 = 1.06420880400844228286e-01, /* 0x3FBB3E66, 0x18EEE323 */
        qa2 = 5.40397917702171048937e-01, /* 0x3FE14AF0, 0x92EB6F33 */
        qa3 = 7.18286544141962662868e-02, /* 0x3FB2635C, 0xD99FE9A7 */
        qa4 = 1.26171219808761642112e-01, /* 0x3FC02660, 0xE763351F */
        qa5 = 1.36370839120290507362e-02, /* 0x3F8BEDC2, 0x6B51DD1C */
        qa6 = 1.19844998467991074170e-02, /* 0x3F888B54, 0x5735151D */
        /*
         * Coefficients for approximation to erfc in [1.25,1/0.35]
         */
        ra0 = -9.86494403484714822705e-03, /* 0xBF843412, 0x600D6435 */
        ra1 = -6.93858572707181764372e-01, /* 0xBFE63416, 0xE4BA7360 */
        ra2 = -1.05586262253232909814e+01, /* 0xC0251E04, 0x41B0E726 */
        ra3 = -6.23753324503260060396e+01, /* 0xC04F300A, 0xE4CBA38D */
        ra4 = -1.62396669462573470355e+02, /* 0xC0644CB1, 0x84282266 */
        ra5 = -1.84605092906711035994e+02, /* 0xC067135C, 0xEBCCABB2 */
        ra6 = -8.12874355063065934246e+01, /* 0xC0545265, 0x57E4D2F2 */
        ra7 = -9.81432934416914548592e+00, /* 0xC023A0EF, 0xC69AC25C */
        sa1 = 1.96512716674392571292e+01, /* 0x4033A6B9, 0xBD707687 */
        sa2 = 1.37657754143519042600e+02, /* 0x4061350C, 0x526AE721 */
        sa3 = 4.34565877475229228821e+02, /* 0x407B290D, 0xD58A1A71 */
        sa4 = 6.45387271733267880336e+02, /* 0x40842B19, 0x21EC2868 */
        sa5 = 4.29008140027567833386e+02, /* 0x407AD021, 0x57700314 */
        sa6 = 1.08635005541779435134e+02, /* 0x405B28A3, 0xEE48AE2C */
        sa7 = 6.57024977031928170135e+00, /* 0x401A47EF, 0x8E484A93 */
        sa8 = -6.04244152148580987438e-02, /* 0xBFAEEFF2, 0xEE749A62 */
        /*
         * Coefficients for approximation to erfc in [1/.35,28]
         */
        rb0 = -9.86494292470009928597e-03, /* 0xBF843412, 0x39E86F4A */
        rb1 = -7.99283237680523006574e-01, /* 0xBFE993BA, 0x70C285DE */
        rb2 = -1.77579549177547519889e+01, /* 0xC031C209, 0x555F995A */
        rb3 = -1.60636384855821916062e+02, /* 0xC064145D, 0x43C5ED98 */
        rb4 = -6.37566443368389627722e+02, /* 0xC083EC88, 0x1375F228 */
        rb5 = -1.02509513161107724954e+03, /* 0xC0900461, 0x6A2E5992 */
        rb6 = -4.83519191608651397019e+02, /* 0xC07E384E, 0x9BDC383F */
        sb1 = 3.03380607434824582924e+01, /* 0x403E568B, 0x261D5190 */
        sb2 = 3.25792512996573918826e+02, /* 0x40745CAE, 0x221B9F0A */
        sb3 = 1.53672958608443695994e+03, /* 0x409802EB, 0x189D5118 */
        sb4 = 3.19985821950859553908e+03, /* 0x40A8FFB7, 0x688C246A */
        sb5 = 2.55305040643316442583e+03, /* 0x40A3F219, 0xCEDF3BE6 */
        sb6 = 4.74528541206955367215e+02, /* 0x407DA874, 0xE79FE763 */
        sb7 = -2.24409524465858183362e+01; /* 0xC03670E2, 0x42712D62 */

extern double exp(double);
extern double fabs(double);

double erf_Sun(double x)
{
    int n0,hx,ix,i;
    double R,S,P,Q,s,y,z,r;
    n0 = ((*(int*)&one)>>29)^1;
    hx = *(n0+(int*)&x);
    ix = hx&0x7fffffff;
    if(ix>=0x7ff00000) { /* erf(nan)=nan */
        i = ((unsigned)hx>>31)<<1;
        return (double)(1-i)+one/x; /* erf(+-inf)=+-1 */
    }
    
    if(ix < 0x3feb0000) { /* |x|<0.84375 */
        if(ix < 0x3e300000) { /* |x|<2**-28 */
            if (ix < 0x00800000)
                return 0.125*(8.0*x+efx8*x); /*avoid underflow */
            return x + efx*x;
        }
        z = x*x;
        r = pp0+z*(pp1+z*(pp2+z*(pp3+z*pp4)));
        s = one+z*(qq1+z*(qq2+z*(qq3+z*(qq4+z*qq5))));
        y = r/s;
        return x + x*y;
    }
    if(ix < 0x3ff40000) { /* 0.84375 <= |x| < 1.25 */
        s = fabs(x)-one;
        P = pa0+s*(pa1+s*(pa2+s*(pa3+s*(pa4+s*(pa5+s*pa6)))));
        Q = one+s*(qa1+s*(qa2+s*(qa3+s*(qa4+s*(qa5+s*qa6)))));
        if(hx>=0) return erx + P/Q; else return -erx - P/Q;
    }
    if (ix >= 0x40180000) { /* inf>|x|>=6 */
        if(hx>=0) return one-tiny; else return tiny-one;
    }
    x = fabs(x);
    s = one/(x*x);
    if(ix< 0x4006DB6E) { /* |x| < 1/0.35 */
        R=ra0+s*(ra1+s*(ra2+s*(ra3+s*(ra4+s*(
                ra5+s*(ra6+s*ra7))))));
        S=one+s*(sa1+s*(sa2+s*(sa3+s*(sa4+s*(
                sa5+s*(sa6+s*(sa7+s*sa8)))))));
    } else { /* |x| >= 1/0.35 */
        R=rb0+s*(rb1+s*(rb2+s*(rb3+s*(rb4+s*(
                rb5+s*rb6)))));
        S=one+s*(sb1+s*(sb2+s*(sb3+s*(sb4+s*(
                sb5+s*(sb6+s*sb7))))));
    }
    z = x;
    *(1-n0+(int*)&z) = 0;
    r = exp(-z*z-0.5625)*exp((z-x)*(z+x)+R/S);
    if(hx>=0) return one-r/x; else return r/x-one;
}

double erfc_Sun(double x)
{
    int n0,hx,ix;
    double R,S,P,Q,s,y,z,r;
    n0 = ((*(int*)&one)>>29)^1;
    hx = *(n0+(int*)&x);
    ix = hx&0x7fffffff;
    if(ix>=0x7ff00000) { /* erfc(nan)=nan */
        /* erfc(+-inf)=0,2 */
        return (double)(((unsigned)hx>>31)<<1)+one/x;
    }
    
    if(ix < 0x3feb0000) { /* |x|<0.84375 */
        if(ix < 0x3c700000) /* |x|<2**-56 */
            return one-x;
        z = x*x;
        r = pp0+z*(pp1+z*(pp2+z*(pp3+z*pp4)));
        s = one+z*(qq1+z*(qq2+z*(qq3+z*(qq4+z*qq5))));
        y = r/s;
        if(hx < 0x3fd00000) { /* x<1/4 */
            return one-(x+x*y);
        } else {
            r = x*y;
            r += (x-half);
            return half - r ;
        }
    }
    if(ix < 0x3ff40000) { /* 0.84375 <= |x| < 1.25 */
        s = fabs(x)-one;
        P = pa0+s*(pa1+s*(pa2+s*(pa3+s*(pa4+s*(pa5+s*pa6)))));
        Q = one+s*(qa1+s*(qa2+s*(qa3+s*(qa4+s*(qa5+s*qa6)))));
        if(hx>=0) {
            z = one-erx; return z - P/Q;
        } else {
            z = erx+P/Q; return one+z;
        }
    }
    if (ix < 0x403c0000) { /* |x|<28 */
        x = fabs(x);
        s = one/(x*x);
        if(ix< 0x4006DB6D) { /* |x| < 1/.35 ~ 2.857143*/
            R=ra0+s*(ra1+s*(ra2+s*(ra3+s*(ra4+s*(
                    ra5+s*(ra6+s*ra7))))));
            S=one+s*(sa1+s*(sa2+s*(sa3+s*(sa4+s*(
                    sa5+s*(sa6+s*(sa7+s*sa8)))))));
        } else { /* |x| >= 1/.35 ~ 2.857143 */
            if(hx<0&&ix>=0x40180000) return two-tiny;/* x < -6 */
            R=rb0+s*(rb1+s*(rb2+s*(rb3+s*(rb4+s*(
                    rb5+s*rb6)))));
            S=one+s*(sb1+s*(sb2+s*(sb3+s*(sb4+s*(
                    sb5+s*(sb6+s*sb7))))));
        }
        z = x;
        *(1-n0+(int*)&z) = 0;
        r = exp(-z*z-0.5625)*
                exp((z-x)*(z+x)+R/S);
        if(hx>0) return r/x; else return two-r/x;
    } else {
        if(hx>0) return tiny*tiny; else return two-tiny;
    }
}


double erf_Aprox(double x)
{
    // constants
    double a1 =  0.254829592;
    double a2 = -0.284496736;
    double a3 =  1.421413741;
    double a4 = -1.453152027;
    double a5 =  1.061405429;
    double p  =  0.3275911;
 
    // Save the sign of x
    int sign = 1;
    if (x < 0)
        sign = -1;
    x = fabs(x);
 
    // A&S formula 7.1.26
    double t = 1.0/(1.0 + p*x);
    double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);
 
    return sign*y;
}

// Error function calculation
double calcERF(double in, int erfSwitch)
{
 	if (erfSwitch == 1){
        // Sun Microsystems
 		return erf_Sun(in);  
    }
 	else if (erfSwitch == 2){
        // Approximation by A & S
 		return erf_Aprox(in); 
    }
    else{
        // Approximation used by Sheffield University
        return erf_Sheffield(in); 
    }
}

// MEX wrapper
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	double *fSpace, *probMatrix, *mask, *priors, *erfTableLow;
	double frameBasedProb, *covar, *centres;
	double lowerBound, erfHigh, scaleErf, alpha, pBinaryP, pBinaryM;
	int    ii, jj, kk, ll, nFrames, nFeatures;
 	int    nCentres, nGMMs, exit = false, erfSwitch;
	int    bScale = true, bUseMask = true, idxCounter, bBounded = false;


	// Check for proper arguments
  	if ((nrhs < 2) || (nlhs > 1)){
    	exit = true;   
  	} 
	
	// Figure out if a mask is supplied
	if(nrhs >= 3)
		bUseMask = true;
	else 
		bUseMask = false;

	// Set lower bound
	if(nrhs >= 4){
		lowerBound = mxGetScalar(LOWERBOUND);
        
        // De-activate bounded marginalization
        if(lowerBound < 0.0){
            bBounded = false;
        }
        else{
            bBounded = true;
        }
	}
	else{
		lowerBound = 0.0;
		bBounded   = false;
	}
        
	// Scaling
	if(nrhs >= 5){
		alpha = (double)mxGetScalar(SCALE);
		if (alpha!=0.0){
			bScale = true;
        }
		else{
			bScale = false;
        }
	}
	else{
		alpha  = 1.0;
		bScale = false;
	}

	// Erf function implementation
	if(nrhs >= 6){
		erfSwitch = (int)mxGetScalar(ERFSWITCH);
		if (erfSwitch>3 || erfSwitch<1)
			mexErrMsgTxt ("Erf switch must be either 1, 2 or 3!");
	}
	else{
		erfSwitch = 2;
	}
	
	if(!exit){
  		// Check the dimensions of the feature space
	  	nFrames   = (int)mxGetM(FSPACE);
  		nFeatures = (int)mxGetN(FSPACE);

		// Assign pointer to the feature space
  		fSpace = mxGetPr(FSPACE);

		// Check for proper mask dimension
		if(bUseMask){
			if (nFrames != (int)mxGetM(MASK)  || nFeatures != (int)mxGetN(MASK))
				mexErrMsgTxt ("Dimension of feature space and mask musst be identical!");
			
			// Assign pointer to the mask
			mask = mxGetPr(MASK);
		}

		// Check if GMM is a structure
    	if (! mxIsStruct (GMM))
			mexErrMsgTxt ("GMM must be a structure containing the trained GMM models in NETLAB format!");
     
		// Determine the number of GMMs
		nGMMs = (int) mxGetNumberOfElements(GMM);

		// Create a matrix for the return argument 
		PROBMATRIX = mxCreateDoubleMatrix(nFrames, nGMMs ,mxREAL);
  		probMatrix = mxGetPr(PROBMATRIX);

	    // Allocate memory (by assuming that the number of centres will be CONSTANT over all spealer models)
		erfTableLow = (double *) mxCalloc(nFeatures * (int) mxGetScalar(mxGetField(GMM, 0, "ncentres")), sizeof(double));


		if(bUseMask && bBounded && bScale){

			// ==================================================================
			// ***********************
			// BOUNDED MARGINALIZATION
			// ***********************
			// ==================================================================
			// Scale marginals with respect to integration bounds

			// Loop over the number of GMMs
			for (ii = 0; ii < nGMMs; ii++){	
				
				// ===========================
				// GET GMM-DEPENDENT PARAMERTS
				// ===========================
	
				// Get covariance
				covar = mxGetPr(mxGetField(GMM, ii,   "covars"));
			
				// Get mean values
				centres = mxGetPr(mxGetField(GMM, ii, "centres"));

				// Get priors
				priors = mxGetPr(mxGetField(GMM, ii,  "priors"));
				
				// Get the number of centres 
				nCentres = (int) mxGetScalar(mxGetField(GMM, ii, "ncentres"));

				// ****************************************************************
				// Compute error function table for lower bound for current GMM
				// ****************************************************************

				// Loop over number of centres (clusters)
				for (jj = 0; jj < nCentres; jj++){
					// Loop over the number of features in the current frame
					for (ll = 0; ll < nFeatures; ll++){
						
						// Index pointer (depending on current feature and current cluster)
						idxCounter = ll*nCentres+jj;

						// Scaling factor
						scaleErf = 1.0 / (M_sqrtOf2 * sqrt(covar[idxCounter]));

						// Build-up error function table for lower integration bound
						erfTableLow[idxCounter] = calcERF((lowerBound - centres[idxCounter]) * scaleErf, erfSwitch);
					}
				}

				// Loop over the number of frames
				for (kk = 0; kk < nFrames; kk++){
					
					// Reset probability
					frameBasedProb = 0.0;

					// Loop over number of centres (clusters)
					for (jj = 0; jj < nCentres; jj++){

						// Reset probabilities for Present (P) and Missing (M) features
						pBinaryP = 1.0;
						pBinaryM = 1.0;

						// Loop over the number of features in the current frame
						for (ll = 0; ll < nFeatures; ll++){
							
							// Index pointer (depending on current feature and current cluster)
							idxCounter = ll*nCentres+jj;

							// Reliable feature 
							if(mask[kk+(ll*nFrames)] == 1){
								// Eq. 9.13 - present features
								pBinaryP *= calcGaussProb(fSpace[kk+(ll*nFrames)],centres[idxCounter],covar[idxCounter]);							}
							// Unreliable feature 
							else{
								// Eq. 9.13 - missing features
									
								// Recover unreliable parts by calculating the bounded marginal
								// expressed as the difference of two error functions

								// Just do this calculation if multiplying pBinaryM with another values is non-zero
								if (pBinaryM > 0){
									
									// Scaling factor
									scaleErf = 1.0 / (M_sqrtOf2 * sqrt(covar[idxCounter]));

	                                // Calculate upper bound using error function
									erfHigh = calcERF((fSpace[kk+(ll*nFrames)] - centres[idxCounter]) * scaleErf, erfSwitch);
										
									// Accumulate marginal probability
									pBinaryM *= (0.5 * (erfHigh - erfTableLow[idxCounter]));
								
									// Scale the marginal probability by the integration range
									pBinaryM *= (alpha / ( fSpace[kk+(ll*nFrames)] - lowerBound) );
								}
							}
						}
						// Weight probability with mixture weight (dot product with prior)
						frameBasedProb += ((pBinaryP * pBinaryM) * priors[jj]);
					}
					// Store GMM-dependent output probability
					probMatrix[kk+(ii*nFrames)] = frameBasedProb;
				}
			}
		}

		else if(bUseMask && bBounded && !bScale){
			
			// ==================================================================
			// ***********************
			// BOUNDED MARGINALIZATION
			// ***********************
			// ==================================================================
			// Do not scale marginals with respect to integration bounds

			// Loop over the number of GMMs
			for (ii = 0; ii < nGMMs; ii++){	
				
				// ===========================
				// GET GMM-DEPENDENT PARAMERTS
				// ===========================
	
				// Get covariance
				covar = mxGetPr(mxGetField(GMM, ii,   "covars"));
			
				// Get mean values
				centres = mxGetPr(mxGetField(GMM, ii, "centres"));

				// Get priors
				priors = mxGetPr(mxGetField(GMM, ii,  "priors"));
				
				// Get the number of centres 
				nCentres = (int) mxGetScalar(mxGetField(GMM, ii, "ncentres"));

				// ****************************************************************
				// Compute error function table for lower bound for current GMM
				// ****************************************************************

				// Loop over number of centres (clusters)
				for (jj = 0; jj < nCentres; jj++){
					// Loop over the number of features in the current frame
					for (ll = 0; ll < nFeatures; ll++){
						
						// Index pointer (depending on current feature and current cluster)
						idxCounter = ll*nCentres+jj;

						// Scaling factor
						scaleErf = 1.00 / (M_sqrtOf2 * sqrt(covar[idxCounter]));

						// Build-up error function table for lower integration bound
						erfTableLow[idxCounter] = calcERF((lowerBound - centres[idxCounter]) * scaleErf, erfSwitch);
					}
				}

				// Loop over the number of frames
				for (kk = 0; kk < nFrames; kk++){
					
					// Reset probability
					frameBasedProb = 0.0;

					// Loop over number of centres (clusters)
					for (jj = 0; jj < nCentres; jj++){

						// Reset probabilities for Present (P) and Missing (M) features
						pBinaryP = 1.0;
						pBinaryM = 1.0;

						// Loop over the number of features in the current frame
						for (ll = 0; ll < nFeatures; ll++){
							
							// Index pointer (depending on current feature and current cluster)
							idxCounter = ll*nCentres+jj;

							// Reliable feature 
							if(mask[kk+(ll*nFrames)] == 1){
								// Eq. 9.13 - present features
								pBinaryP *= calcGaussProb(fSpace[kk+(ll*nFrames)],centres[idxCounter],covar[idxCounter]);							}
							// Unreliable feature 
							else{
								// Eq. 9.13 - missing features
								
								// Just do this calculation if multiplying pBinaryM with another values is non-zero
								if (pBinaryM > 0){
									// Recover unreliable parts by calculating the bounded marginal
									// expressed as the difference of two error functions

									// Scaling factor
									scaleErf = 1.00 / (M_sqrtOf2 * sqrt(covar[idxCounter]));

									// Calculate upper bound using error function
									erfHigh = calcERF((fSpace[kk+(ll*nFrames)] - centres[idxCounter]) * scaleErf, erfSwitch);
										
									// Accumulate marginal probability
									pBinaryM *= (0.5 * (erfHigh - erfTableLow[idxCounter]));
								}
							}
						}
						// Weight probability with mixture weight (dot product with prior)
						frameBasedProb += pBinaryP * pBinaryM * priors[jj];
					}
					// Store GMM-dependent output probability
					probMatrix[kk+(ii*nFrames)] = frameBasedProb;
				}
			}
		}

		else if(bUseMask){

			// ==================================================================
			// ***********************
			// FULL MARGINALIZATION
			// ***********************
			// ==================================================================

			// Loop over the number of GMMs
			for (ii = 0; ii < nGMMs; ii++){	
				
				// ===============================
				// GET GMM-DEPENDENT PARAMERTS
				// ===============================
	
				// Get covariance
				covar = mxGetPr(mxGetField(GMM, ii,   "covars"));
			
				// Get mean values
				centres = mxGetPr(mxGetField(GMM, ii, "centres"));

				// Get priors
				priors = mxGetPr(mxGetField(GMM, ii,  "priors"));
				
				// Get the number of centres 
				nCentres = (int) mxGetScalar(mxGetField(GMM, ii, "ncentres"));

				// Loop over the number of frames
				for (kk = 0; kk < nFrames; kk++){
					
					// Reset probability
					frameBasedProb = 0.0;

					// Loop over number of centres (clusters)
					for (jj = 0; jj < nCentres; jj++){

						// Reset probabilities for Present (P) and Missing (M) features
						pBinaryP = 1.0;

						// Loop over the number of features in the current frame
						for (ll = 0; ll < nFeatures; ll++){
							
							// Index pointer (depending on current feature and current cluster)
							idxCounter = ll * nCentres + jj;

							// Reliable feature 
							if(mask[kk+(ll*nFrames)] == 1){
								// Eq. 9.13 - present features
								pBinaryP *= calcGaussProb(fSpace[kk+(ll*nFrames)],centres[idxCounter],covar[idxCounter]);							
							}
						}
						// Weight probability with mixture weight (dot product with prior)
						frameBasedProb += pBinaryP * priors[jj];
					}
					// Store GMM-dependent output probability
					probMatrix[kk+(ii*nFrames)] = frameBasedProb;
				}
			}
		}

		else{
			// ==================================================================
			// ***********************
			// BASELINE RECOGNITION
			// ***********************
			// ==================================================================

			// Loop over the number of GMMs
			for (ii = 0; ii < nGMMs; ii++){	
				
				// ===============================
				// GET GMM-DEPENDENT PARAMERTS
				// ===============================
	
				// Get covariance
				covar = mxGetPr(mxGetField(GMM, ii,   "covars"));
			
				// Get mean values
				centres = mxGetPr(mxGetField(GMM, ii, "centres"));

				// Get priors
				priors = mxGetPr(mxGetField(GMM, ii,  "priors"));
				
				// Get the number of centres 
				nCentres = (int) mxGetScalar(mxGetField(GMM, ii, "ncentres"));

				// Loop over the number of frames
				for (kk = 0; kk < nFrames; kk++){
					
					// Reset probability
					frameBasedProb = 0.0;

					// Loop over number of centres (clusters)
					for (jj = 0; jj < nCentres; jj++){

						// Reset probability
						pBinaryP = 1.0;
						
						// Loop over the number of features 
						for (ll = 0; ll < nFeatures; ll++){
							// Index pointer (depending on current feature and current cluster)
							idxCounter = ll * nCentres + jj;

							// Calculate Gaussian probability
							pBinaryP *= calcGaussProb(fSpace[kk+(ll*nFrames)],centres[idxCounter],covar[idxCounter]);							
						}
						// Weight probability with mixture weight (dot product with prior)
						frameBasedProb += pBinaryP * priors[jj];
					}
					// Store GMM-dependent probability of kk-th frame
					probMatrix[kk+(ii*nFrames)] = frameBasedProb;
				}
			}
		}
	}
	else
		usage();
}
