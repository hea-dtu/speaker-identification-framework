function [ibm,noiseFloor] = floorMask(ibm,noiseFloordB)
%floorMask   Incorporate a floor value to the ideal binary mask pattern.
%   The idea is to restrict the attenuation of the interfering signal in
%   order to reduce the musical-noise type of artifacts. 
%   
%USAGE
%   [ibm,noiseFloor] = floorMask(ibm,noiseFloordB)
% 
%INPUT ARGUMENTS
%            ibm : ideal binary mask pattern
%   noiseFloordB : noise floor in dB (default, noiseFloordB = 20) according
%                  to [2]. In [1], a floor corresponding to 26 dB was used.
% 
%OUTPUT ARGUMENTS
%            ibm : floored ideal binary mask pattern
%     noiseFloor : minimum IBM value
% 
%REFERENCES
%   [1] A. Narayanan and D. Wang, "The role of binary mask patterns in
%       automatic speech recognition in background noise", The Journal of
%       the Acoustical Society of America, pp.3083-3093, 2013. 
% 
%   [2] W. Hartmann, "ASR-driven binary mask estimation for robust
%       automatic speech recognition", PhD thesis, 2012.

%   Developed with Matlab 8.1.0.604 (R2013a). Please send bug reports to:
%   
%   Author  :  Tobias May, � 2013
%              Technical University of Denmark
%              tobmay@elektro.dtu.dk
%
%   History :
%   v.0.1   2013/09/10
%   ***********************************************************************


%% CHECK INPUT ARGUMENTS
% 
% 
% Check for proper input arguments
if nargin < 1 || nargin > 2
    help(mfilename);
    error('Wrong number of input arguments!')
end

% Set default values
if nargin < 2 || isempty(noiseFloordB); noiseFloordB = 20; end


%% FLOOR BINARY MASK PATTERN
% 
% 
% Compute mask floor 
noiseFloor = 10.^(-abs(noiseFloordB)/20);

% Impose lower limit to binary mask pattern
ibm = max(cast(ibm,'double'),noiseFloor);


%   ***********************************************************************
%   This program is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
% 
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
% 
%   You should have received a copy of the GNU General Public License
%   along with this program.  If not, see <http://www.gnu.org/licenses/>.
%   ***********************************************************************