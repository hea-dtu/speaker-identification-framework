# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## [v1.0.0] - 2016-05-15
### Added
- CHANGELOG and VERSION
- System used for speech communication paper submission
