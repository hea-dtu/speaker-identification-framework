# Speaker identification (SID) framework #

This repository contains basic signal processing functions to perform automatic speaker identification (SID) experiments. The identification is performed using Gaussian mixture model (GMM) classifiers. For improved robustness, all speaker models can be adapted from a universal background model (UBM), which is trained with the complete speech material from all speakers.
The speaker-specific characteristics are captured by either mel frequency cepstral coefficients (MFCCs) or filter-bank energy (FBE) features. The FBE features are correlated across frequency and thus, produce lower identification performance than MFCC features. This disadvantage of the FBE features can be resolved by applying a short differentiation filter across frequency channels, which effectively decorrelates the energy features.
In addition, a missing data-based classifier is available, which employs a grouping stage based in the ideal binary mask (IBM) to focus on the speech-relevant information. The noise-dominated information is used to compute the counter evidence that a particular speaker model has been masked by the interfering noise.
Moreover, two methods for estimating the IBM are available. The first is using noise power estimation to derive the speech presence probability in individual time-frequency units. The second one computes a voicing-distance, which is related to speech activity. 

![SID_System.png](https://bitbucket.org/repo/pkBejq/images/2061611989-SID_System.png)

Feature extraction:

* Mel-frequency cepstral coefficient (MFCC) features [^HTK2006]
* Filterbank energy (FBE) features [^HTK2006],[^NadeuMachoHernando2001]
* Decorrelated filterbank energy (DFBE) features [^NadeuMachoHernando2001]
* Delta-feature extraction [^HTK2006]
* Relative spectral (RASTA) filtering [^HermanskyMorgan1994]

Normalization techniques:

* Global mean and variance normalization (GMVN) [^ViikkiLaurila1998]
* File-based mean and variance normalization (FMVN) [^ViikkiLaurila1998]
* Adaptive mean and variance normalization (AMVN) [^ViikkiLaurila1998]
* Histogram equalization (HEQ) [^delaTorrePeinadoSeguraPerezCordobaBenitezRubio2005]
* Auto-regression moving average (ARMA) filtering [^ChenBilmes2007]

Classifiers:

* Speaker identification based on Gaussian mixture models (GMMs) [^ReynoldsRose1995]
* Adaptation of speaker models using a universal background model (UBM) [^ReynoldsQuatieriDunn2000]
* Missing-data recognition (Full marginalization & Bounded marginalization) combined with UBMs [^MayvandeParKohlrausch2012]
* Missing-data recognition via direct masking (DM) [^HartmannNarayananFoslerLussierWang13]

Segregation stages:

* Binary mask estimation for missing-data recognition based on speech presence probability [^MayGerkmann2014]
* Binary mask estimation for missing-data recognition based on the voicing distance [^JančovičKöküer2007]

### How do I get set up? ###

* Clone this repository.
* Browse to the Matlab folder an run the script `SIDDemo.m`

### Contribution guidelines ###

* Create an issue on [BitBucket](http://bitbucket.org/hea-dtu/speakeridentification-framework)
* Contact Tobias (tobmay@elektro.dtu.dk)

### How to cite ###

If you use the Matlab software in a scientific publication or proposal, please cite the corresponding paper:

* T. May, “Influence of binary mask estimation errors on robust speaker identification, “ Speech Commun., in press, [**DOI**](http://dx.doi.org/10.1016/j.specom.2016.12.002).

[^HTK2006]: S. Young, G. Evermann, M. Gales, T. Hain, D. Kershaw, X. Liu, G. Moore, J. Odell, D. Ollason, D. Povey, V. Valtchev and P. Woodland, “The HTK Book (for HTK Version 3.4.1),“ Engineering Department, Cambridge University, 2006, [**Link**](http://htk.eng.cam.ac.uk/).

[^NadeuMachoHernando2001]: C. Nadeu, D. Macho, and J. Hernando, “Time and frequency filtering of filter-bank energies for robust HMM speech recognition,“ Speech Commun., vol. 34, no. 1-2, pp. 93-114, 2001, [**DOI**](http://dx.doi.org/10.1016/S0167-6393(00)00048-0).

[^HermanskyMorgan1994]: H. Hermansky and N. Morgan, “RASTA processing of speech,“ IEEE Trans. Speech Audio Process., vol. 2, no. 4, pp. 578-589, 1994, [**DOI**](http://dx.doi.org/10.1109/89.326616).

[^ViikkiLaurila1998]: O. Viikki and K. Laurila, “Cepstral domain segmental feature vector normalization for noise robust speech recognition,“ Speech Commun., vol. 25, no. 1-3, pp. 133-147, 1998, [**DOI**](http://dx.doi.org/10.1016/S0167-6393(98)00033-8).

[^delaTorrePeinadoSeguraPerezCordobaBenitezRubio2005]: A. de la Torre, A. M. Peinado, J.C. Segura, J. L. Perez-Cordoba, M. C. Benitez and A. J. Rubio, “Histogram equalization of speech representation for robust speech recognition,“ IEEE Trans. Speech Audio Process., vol. 13, no. 3, pp. 355–366, 2005, [**DOI**](http://dx.doi.org/10.1109/TSA.2005.845805).

[^ChenBilmes2007]: C.-P. Chen and J. Bilmes, “MVA Processing of Speech Features,“ IEEE Trans. Speech Audio Process., vol. 15, no. 1, pp. 257-270, 2007, [**DOI**](http://dx.doi.org/10.1109/TASL.2006.876717).

[^ReynoldsRose1995]: D. Reynolds and R. Rose, “Robust text-independent speaker identification using Gaussian mixture speaker models,“ IEEE Trans. Speech Audio Process., vol. 3, no. 1, pp. 72–83, 1995, [**DOI**](http://dx.doi.org/10.1109/89.365379).

[^ReynoldsQuatieriDunn2000]: D. A. Reynolds, T. F. Quatieri, and R. B. Dunn, “Speaker verification using adapted Gaussian mixture models,“ Digital Signal Process., vol. 10, pp. 19–41, 2000, [**DOI**](http://dx.doi.org/10.1006/dspr.1999.0361).

[^MayvandeParKohlrausch2012]: T. May, S. van de Par, and A. Kohlrausch, “Noise-Robust Speaker Recognition Combining Missing Data Techniques and Universal Background Modeling,“ IEEE Trans. Speech Audio Process., vol. 20, no. 1, pp. 108–121, 2012, [**DOI**](http://dx.doi.org/10.1109/TASL.2011.2158309).

[^HartmannNarayananFoslerLussierWang13]: W. Hartmann, A. Narayanan, E. Fosler-Lussier and Deliang Wang, “A direct masking approach to robust ASR,“ IEEE Trans. Speech Audio Process., vol. 21, no. 10, pp. 1993–2005, 2013, [**DOI**](http://dx.doi.org/10.1109/TASL.2013.2263802).

[^MayGerkmann2014]: T. May and T. Gerkmann, “Generalization of supervised learning for binary mask estimation,“ International Workshop on Acoustic Signal Enhancement (IWAENC), pp.154-158, 2014, [**DOI**](http://dx.doi.org/10.1109/IWAENC.2014.6953357).

[^JančovičKöküer2007]: P. Jančovič, and M. Köküer, “Estimation of Voicing-Character of Speech Spectra Based on Spectral Shape,“ IEEE Signal Processing Letters., vol. 14, no. 1, pp. 66-69, 2007, [**DOI**](http://dx.doi.org/10.1109/LSP.2006.881517).